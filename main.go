/*

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package main

import (
	"flag"
	"os"

	"k8s.io/klog"

	"alauda.io/amp-controller/gateway"
	"alauda.io/amp-controller/gateway/kong"

	"alauda.io/amp-controller/metering"

	"k8s.io/apimachinery/pkg/runtime"
	clientgoscheme "k8s.io/client-go/kubernetes/scheme"
	_ "k8s.io/client-go/plugin/pkg/client/auth/gcp"
	ctrl "sigs.k8s.io/controller-runtime"
	"sigs.k8s.io/controller-runtime/pkg/log/zap"

	ampv1 "alauda.io/amp-controller/api/v1"
	"alauda.io/amp-controller/common"
	"alauda.io/amp-controller/controllers"
	// +kubebuilder:scaffold:imports
)

var (
	scheme   = runtime.NewScheme()
	setupLog = ctrl.Log.WithName("setup")
)

func init() {
	_ = clientgoscheme.AddToScheme(scheme)

	_ = ampv1.AddToScheme(scheme)
	// +kubebuilder:scaffold:scheme
}

func main() {
	klog.InitFlags(nil)
	defer klog.Flush()

	var metricsAddr string
	var enableLeaderElection bool
	flag.StringVar(&metricsAddr, "metrics-addr", ":8080", "The address the metric endpoint binds to.")
	flag.BoolVar(&enableLeaderElection, "enable-leader-election", false,
		"Enable leader election for controller manager. Enabling this will ensure there is only one active controller manager.")

	ctrl.SetLogger(zap.Logger(true))

	var debugMode bool
	flag.BoolVar(&debugMode, "debug", false, "Debug mode.")

	gatewayFlags := &gateway.GatewayFlags{}
	gateway.AddGatewayFlags(gatewayFlags)
	metering.AddMeteringFlags(&metering.MeteringConfig)

	flag.Parse()

	if err := gatewayFlags.ParseGatewayConfig(); nil != err {
		setupLog.Error(err, "problem setup gateway config")
		os.Exit(1)
	}

	if err := metering.MeteringConfig.Validate(); nil != err {
		setupLog.Error(err, "problem setup metering config")
		os.Exit(1)
	}

	if debugMode {
		metering.MeteringConfig.Debug = debugMode
		common.ReconcileDefaultDelay = common.ReconcileDebugDelay
	}

	setupLog.Info("metrics addr", "metric-addr", metricsAddr)

	mgr, err := ctrl.NewManager(ctrl.GetConfigOrDie(), ctrl.Options{
		Scheme:             scheme,
		MetricsBindAddress: metricsAddr,
		LeaderElection:     enableLeaderElection,
		Port:               9443,
	})
	if err != nil {
		setupLog.Error(err, "unable to start manager")
		os.Exit(1)
	}

	if err = (&controllers.ApiProductReconciler{
		Client: mgr.GetClient(),
		Log:    ctrl.Log.WithName("controllers").WithName("ApiProduct"),
	}).SetupWithManager(mgr); err != nil {
		setupLog.Error(err, "unable to create controller", "controller", "ApiProduct")
		os.Exit(1)
	}
	if err = (&controllers.ApiConsumeApplicationReconciler{
		Client: mgr.GetClient(),
		Log:    ctrl.Log.WithName("controllers").WithName("ApiConsumeApplication"),
	}).SetupWithManager(mgr); err != nil {
		setupLog.Error(err, "unable to create controller", "controller", "ApiConsumeApplication")
		os.Exit(1)
	}
	if err = (&controllers.ApiProductServiceReconciler{
		Client: mgr.GetClient(),
		Log:    ctrl.Log.WithName("controllers").WithName("ApiProductService"),
	}).SetupWithManager(mgr); err != nil {
		setupLog.Error(err, "unable to create controller", "controller", "ApiProductService")
		os.Exit(1)
	}
	if err = (&controllers.OrderReconciler{
		Client: mgr.GetClient(),
		Log:    ctrl.Log.WithName("controllers").WithName("Order"),
	}).SetupWithManager(mgr); err != nil {
		setupLog.Error(err, "unable to create controller", "controller", "Order")
		os.Exit(1)
	}
	if !metering.MeteringConfig.StandBy {
		if err = (&controllers.LeaseReconciler{
			Client: mgr.GetClient(),
			Log:    ctrl.Log.WithName("controllers").WithName("Lease"),
			Scheme: mgr.GetScheme(),
		}).SetupWithManager(mgr); err != nil {
			setupLog.Error(err, "unable to create controller", "controller", "Lease")
			os.Exit(1)
		}
	}
	if err = (&controllers.BatchOrderReconciler{
		Client: mgr.GetClient(),
		Log:    ctrl.Log.WithName("controllers").WithName("BatchOrder"),
	}).SetupWithManager(mgr); err != nil {
		setupLog.Error(err, "unable to create controller", "controller", "BatchOrder")
		os.Exit(1)
	}
	// +kubebuilder:scaffold:builder

	setupLog.Info("starting manager", "gateway controller", !kong.GatewayConfig.StandBy, "metering controller", !metering.MeteringConfig.StandBy)
	if err := mgr.Start(ctrl.SetupSignalHandler()); err != nil {
		setupLog.Error(err, "problem running manager")
		os.Exit(1)
	}
}
