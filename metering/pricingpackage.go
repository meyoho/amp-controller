package metering

import (
	"context"
	"fmt"
	"net/http"
	"sort"
	"time"

	"github.com/go-logr/logr"

	"github.com/juju/errors"
	"github.com/olivere/elastic"
	"k8s.io/apimachinery/pkg/api/resource"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"

	ampv1 "alauda.io/amp-controller/api/v1"
)

const (
	ESIndex                 = "amp-kong-*"
	ServiceNameKeyword      = "service.name.keyword"
	ConsumerUsernameKeyword = "consumer.username.keyword"
	Timestamp               = "@timestamp"
	RequestSize             = "request.size"
	ResponseSize            = "response.size"
	ResponseStatus          = "response.status"
)

type PricingPackageOperator interface {
	Operator
	NextPlan() *pricingpackage
}

type pricingpackage struct {
	ampv1.SubscriptionPlan
	startTime metav1.MicroTime
}

type state struct {
	openingTime metav1.MicroTime
	apiCalls    int64
	dataPlan    resource.Quantity
}

type pricingPackageOperator struct {
	targetPlans  []pricingpackage
	currentPlan  *pricingpackage
	currentState state
	currentTime  metav1.MicroTime
	Log          logr.Logger
}

func (op *pricingPackageOperator) GetNextPlan() (*pricingpackage, []pricingpackage) {
	if nil == op || len(op.targetPlans) == 0 {
		return nil, nil
	}
	sort.Slice(op.targetPlans, func(i, j int) bool {
		return op.targetPlans[i].CreateTime.Before(&op.targetPlans[j].CreateTime)
	})
	if nil == op.currentPlan {
		return &op.targetPlans[0], op.targetPlans[1:]
	}
	var plan *pricingpackage
	plans := []pricingpackage{}
	for _, tp := range op.targetPlans {
		if nil == plan && tp.CreateTime.Before(&op.currentPlan.CreateTime) {
			plan = &tp
		} else {
			plans = append(plans, tp)
		}
	}
	if nil == plan {
		return &op.targetPlans[0], op.targetPlans[1:]
	}
	return plan, plans
}

func (op *pricingPackageOperator) SetCurrentPlan(plan pricingpackage) {
	if nil == op.currentPlan {
		if plan.startTime.IsZero() {
			plan.startTime = op.currentTime
		}
		if op.currentState.openingTime.IsZero() || op.currentState.openingTime.After(op.currentTime.Time) {
			op.currentState.openingTime = op.currentTime
		}
		op.currentPlan = &plan
		return
	}
	if op.currentPlan.Id == plan.Id {
		op.Log.Info("can't set current plan with current plan, ignore")
		return
	}

	if !op.currentPlanExpired() {
		op.Log.Info("current plan not expired, ignore")
		return
	}
	// expired, change plan
	expirationTime := op.calculateExpirationTime()
	if plan.CreateTime.Before(&expirationTime) {
		if op.currentTime.Equal(&expirationTime) || op.currentTime.After(expirationTime.Time) {
			plan.startTime = expirationTime
		} else {
			plan.startTime = op.currentTime
		}
		apiCalls, _ := op.currentPlan.Config.GetAPICalls()
		if apiCalls >= op.currentState.apiCalls {
			op.currentState.apiCalls = 0
			plan.Config = plan.Config.AddAPICalls(apiCalls - op.currentState.apiCalls)
		} else {
			op.currentState.apiCalls = op.currentState.apiCalls - apiCalls
		}
		dataPlanStr, _ := op.currentPlan.Config.GetDataPlan()
		dataPlan, _ := resource.ParseQuantity(dataPlanStr)
		dataPlan.Sub(op.currentState.dataPlan)
		if dataPlan.Value() >= 0 {
			dataPlanStr, _ := plan.Config.GetDataPlan()
			newDataPlan, _ := resource.ParseQuantity(dataPlanStr)
			newDataPlan.Add(dataPlan)
			plan.Config = plan.Config.SetDataPlan(newDataPlan.String())
			op.currentState.dataPlan = *resource.NewQuantity(0, resource.DecimalSI)
		} else {
			dataPlan.Neg()
			op.currentState.dataPlan = dataPlan
		}
	} else {
		// restart
		plan.startTime = op.currentTime
		// gap: ---[expire]<- [gap] ->[current]----[createTime]-->
		// and it's impossible for createTime greater than currentTime, for we are in the same time dimension
		// but the clock is not always synchronized, so just in case
		if plan.CreateTime.After(op.currentTime.Time) {
			plan.CreateTime = op.currentTime
		}
		op.currentState.openingTime = op.currentTime
		op.currentState.dataPlan = *resource.NewQuantity(0, resource.DecimalSI)
		op.currentState.apiCalls = 0
	}
	op.currentPlan = &plan
	op.Log.Info("set current plan finish", "current plan id", op.currentPlan.Id, "start time", op.currentPlan.startTime, "api calls", op.currentState.apiCalls, "data plan", op.currentState.dataPlan.String())
	return
}

func (op *pricingPackageOperator) Calculate() {
	op.Log.Info("calculate start", "current plan", op.currentPlan, "start time", op.currentPlan.startTime)
	if nil == op.currentPlan || op.currentPlanExpired() {
		var nextPlan *pricingpackage
		nextPlan, op.targetPlans = op.GetNextPlan()
		if nil != nextPlan {
			op.SetCurrentPlan(*nextPlan)
			op.Log.Info("get next plan", "next plan", nextPlan, "target plans", len(op.targetPlans))
			op.Calculate()
		} else {
			// last plan
			op.currentPlan = nil
			op.Log.Info("last plan, calculate finish")
		}
	}
	op.Log.Info("calculate finish")
	return
}

// Expired calculate whether current plan is expired or not
func (op *pricingPackageOperator) currentPlanExpired() bool {
	if nil == op.currentPlan {
		return true
	}
	expirationTime := op.calculateExpirationTime()
	if op.currentTime.Equal(&expirationTime) || op.currentTime.After(expirationTime.Time) {
		op.Log.Info("current plan expired", "id", op.currentPlan.Id, "expiration time", expirationTime, "current time", op.currentTime)
		return true
	}
	apiCalls, _ := op.currentPlan.Config.GetAPICalls()
	leftAPICalls := apiCalls - op.currentState.apiCalls
	if leftAPICalls <= 0 {
		op.Log.Info("current plan expired", "id", op.currentPlan.Id, "left apicalls", leftAPICalls)
		return true
	}
	dataPlanStr, _ := op.currentPlan.Config.GetDataPlan()
	dataPlan, _ := resource.ParseQuantity(dataPlanStr)
	dataPlan.Sub(op.currentState.dataPlan)
	if dataPlan.IsZero() || dataPlan.Value() < 0 {
		op.Log.Info("current plan expired", "id", op.currentPlan.Id, "data plan", dataPlan.String())
		return true
	}
	return false
}

// Expired calculate whether current plan is expired or not
func (op *pricingPackageOperator) Expired() bool {
	if len(op.targetPlans) == 0 && op.currentPlanExpired() {
		return true
	}
	return false
}

// calculateExpirationTime calculate expiration time for current plan
func (op *pricingPackageOperator) calculateExpirationTime() metav1.MicroTime {
	if MeteringConfig.Debug {
		return metav1.MicroTime{
			Time: op.currentPlan.startTime.Add(time.Duration(MeteringConfig.Ratio) * time.Second),
		}
	} else {
		validity, _ := op.currentPlan.Config.GetValidity()
		expirationTime := op.currentPlan.startTime.AddDate(0, 0, int(validity))
		return metav1.MicroTime{Time: expirationTime}
	}
}

func (op *pricingPackageOperator) ExpirationTime() metav1.MicroTime {
	if nil == op.currentPlan {
		return op.currentTime
	}
	return op.calculateExpirationTime()
}

func (op *pricingPackageOperator) SetResult(service *ampv1.ApiProductService) bool {
	if nil == service {
		return false
	}
	var updateSpec bool
	if len(service.Spec.SubscriptionPlans) != len(op.targetPlans) {
		subscriptionPlan := []ampv1.SubscriptionPlan{}
		for _, plan := range op.targetPlans {
			subscriptionPlan = append(subscriptionPlan, plan.SubscriptionPlan)
		}
		service.Spec.SubscriptionPlans = subscriptionPlan
		updateSpec = true
	}
	if op.Expired() {
		service.Status.Condition = nil
		expirationTime := op.ExpirationTime()
		service.Status.ExpirationTime = &expirationTime
		service.Status.Phase = ampv1.ServiceExpired
		service.Status.OpeningTime = nil
		service.Status.TerminationTime = nil
		return updateSpec
	}

	service.Status.Condition = &ampv1.APIProductServiceCondition{
		CurrentPlan:      op.currentPlan.SubscriptionPlan,
		LastScheduleTime: op.currentTime,
		APICalls:         op.currentState.apiCalls,
		DataPlan:         op.currentState.dataPlan,
		StartTime:        op.currentPlan.startTime,
	}
	service.Status.OpeningTime = &op.currentState.openingTime
	service.Status.Phase = ampv1.ServiceOpened
	service.Status.ExpirationTime = nil
	service.Status.TerminationTime = nil
	return updateSpec
}

func (op *pricingPackageOperator) NextScheduleTime() time.Duration {
	interval := time.Duration(MeteringConfig.PricingPackageInterval)
	if op.Expired() {
		return 0
	}
	expirationTime := op.ExpirationTime()
	if delta := expirationTime.Sub(op.currentTime.Time); delta < interval {
		return delta
	}
	return interval
}

func getESData(startTime, currentTime time.Time, product, consumer string) (int64, resource.Quantity, error) {
	client, err := elastic.NewClient(
		elastic.SetURL(MeteringConfig.URL),
		elastic.SetBasicAuth(MeteringConfig.Username, MeteringConfig.Passwd),
		elastic.SetSniff(false),
		elastic.SetHealthcheck(false),
	)
	if nil != err {
		return 0, resource.Quantity{}, nil
	}
	defer client.Stop()

	m, err := client.IndexGet(ESIndex).Do(context.Background())
	if err != nil {
		// Handle error
		return 0, resource.Quantity{}, errors.Annotate(err, "error get index from elasticsearch")
	}
	if 0 == len(m) {
		return 0, resource.Quantity{}, nil
	}

	query := elastic.NewBoolQuery().
		Must(
			elastic.NewTermQuery(ServiceNameKeyword, product),
			elastic.NewTermQuery(ConsumerUsernameKeyword, consumer),
			elastic.NewRangeQuery(Timestamp).Gte(startTime).Lte(currentTime)).
		MustNot(
			elastic.NewTermQuery(ResponseStatus, http.StatusForbidden))
	//i, _ := query.Source()
	//println(fmt.Sprintf("elastcisearch query: %s", i))
	requestSize := elastic.NewSumAggregation().Field(RequestSize)
	responseSize := elastic.NewSumAggregation().Field(ResponseSize)
	searchResult, err := client.Search().
		Index(ESIndex).
		Query(query).
		Aggregation(RequestSize, requestSize).
		Aggregation(ResponseSize, responseSize).
		Size(0).
		RestTotalHitsAsInt(true).
		Do(context.Background())
	if nil != err {
		return 0, resource.Quantity{}, errors.Annotate(err, "error get dataPlan and APICalls from elasticsearch")
	}
	apiCalls := searchResult.Hits.TotalHits
	reqSize, found := searchResult.Aggregations.Sum(RequestSize)
	if !found || nil == reqSize || nil == reqSize.Value {
		return 0, resource.Quantity{}, fmt.Errorf("es query: field not found, %s", RequestSize)
	}
	respSize, found := searchResult.Aggregations.Sum(ResponseSize)
	if !found || nil == respSize || nil == respSize.Value {
		return 0, resource.Quantity{}, fmt.Errorf("es query: field not found, %s", ResponseSize)
	}
	return apiCalls, *resource.NewQuantity(int64(*reqSize.Value+*respSize.Value), resource.DecimalSI), nil
}

func NewPricingPackageOperator(service *ampv1.ApiProductService, lease *ampv1.Lease, eventTime metav1.MicroTime, logger logr.Logger) (Operator, error) {
	if nil == service || nil == lease {
		return nil, nil
	}
	var plans []pricingpackage
	for _, plan := range service.Spec.SubscriptionPlans {
		plans = append(plans, pricingpackage{SubscriptionPlan: plan})
	}
	operator := &pricingPackageOperator{
		currentTime: eventTime,
		targetPlans: plans,
		currentPlan: nil,
		Log:         logger,
	}
	var currentPlan *pricingpackage
	openingTime := service.Status.OpeningTime
	if nil != service.Status.Condition {
		if nil == openingTime || openingTime.After(service.Status.Condition.StartTime.Time) {
			operator.Log.Info("get opening time from start time", "start time", service.Status.Condition.StartTime)
			openingTime = &service.Status.Condition.StartTime
		}
		currentPlan = &pricingpackage{
			SubscriptionPlan: service.Status.Condition.CurrentPlan,
			startTime:        service.Status.Condition.StartTime,
		}
		// just in case
		if currentPlan.startTime.After(operator.currentTime.Time) {
			operator.Log.Info("get opening time from current time")
			openingTime = &operator.currentTime
		}
	}
	if nil == openingTime {
		openingTime = &operator.currentTime
	}
	apiCalls, dataPlan, err := getESData(openingTime.Time, operator.currentTime.Time, service.Spec.ProductName, service.Spec.ConsumeApplicationName)
	if nil != err {
		operator.Log.Info("get es error", "error", err.Error())
		return nil, nil
	}
	operator.Log.Info("get es", "apiCalls", apiCalls, "dataPlan", dataPlan.String())
	operator.currentState = state{
		apiCalls:    apiCalls,
		dataPlan:    dataPlan,
		openingTime: *openingTime,
	}
	if nil == currentPlan {
		currentPlan, operator.targetPlans = operator.GetNextPlan()
	}
	if nil == currentPlan {
		return nil, errors.New("No subscription plan available")
	}
	operator.SetCurrentPlan(*currentPlan)
	operator.Log.Info("get operator", "current time", operator.currentTime, "start time", operator.currentPlan.startTime, "opening time", operator.currentState.openingTime, "expiration time", operator.ExpirationTime(), "expired", operator.Expired())
	return operator, nil
}
