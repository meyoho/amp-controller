package metering

import (
	"errors"
	"fmt"
	"time"

	"github.com/go-logr/logr"

	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"

	ampv1 "alauda.io/amp-controller/api/v1"
)

type Operator interface {
	Calculate()
	Expired() bool
	SetResult(service *ampv1.ApiProductService) bool
	NextScheduleTime() time.Duration
}

func NewMeteringOperator(service *ampv1.ApiProductService, lease *ampv1.Lease, eventTime metav1.MicroTime, logger logr.Logger) (Operator, error) {
	var leaseType ampv1.SubscriptionType
	if nil == service {
		return nil, errors.New("nil api product service")
	}
	leaseType = service.SubscriptionPlanType()

	validator := NewValidator()

	for _, plan := range service.Spec.SubscriptionPlans {
		if err := validator.ValidateSubScriptionPlan(plan); nil != err {
			return nil, err
		}
	}
	switch leaseType {
	case ampv1.MonlthlyPlan:
		return NewMonthlyPlanOperator(service, lease, eventTime, logger)
	case ampv1.PricingPackage:
		return NewPricingPackageOperator(service, lease, eventTime, logger)
	}
	return nil, fmt.Errorf("unsupport lease type: type=%s, service=%s", leaseType, service.GetName())
}
