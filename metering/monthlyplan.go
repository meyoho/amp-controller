package metering

import (
	"errors"
	"sort"
	"time"

	"github.com/go-logr/logr"

	"k8s.io/apimachinery/pkg/api/resource"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"

	ampv1 "alauda.io/amp-controller/api/v1"
)

type MonthlyPlanOperator interface {
	Operator
	NextPlan() (*monthlyplan, []monthlyplan)
}

var _ MonthlyPlanOperator = &monthlyPlanOperator{}

type monthlyplan struct {
	ampv1.SubscriptionPlan
	startTime metav1.MicroTime
}

type monthlyPlanOperator struct {
	targetPlans  []monthlyplan
	currentPlan  *monthlyplan
	currentState state
	currentTime  metav1.MicroTime
	Log          logr.Logger
}

func (op *monthlyPlanOperator) NextPlan() (*monthlyplan, []monthlyplan) {
	if nil == op || len(op.targetPlans) == 0 {
		return nil, nil
	}
	sort.Slice(op.targetPlans, func(i, j int) bool {
		return op.targetPlans[i].CreateTime.Before(&op.targetPlans[j].CreateTime)
	})
	if nil == op.currentPlan {
		return &op.targetPlans[0], op.targetPlans[1:]
	}
	var nextPlan *monthlyplan
	var plans []monthlyplan
	for _, plan := range op.targetPlans {
		if nil == nextPlan && plan.CreateTime.Before(&op.currentPlan.CreateTime) {
			nextPlan = &plan
		} else {
			plans = append(plans, plan)
		}
	}
	if nil == nextPlan {
		return &op.targetPlans[0], op.targetPlans[1:]
	}
	return nextPlan, plans
}

func (op *monthlyPlanOperator) SetCurrentPlan(plan monthlyplan) {
	if nil == op.currentPlan {
		if plan.startTime.IsZero() {
			plan.startTime = op.currentTime
		}
		op.currentPlan = &plan
		if op.currentState.openingTime.IsZero() || op.currentState.openingTime.After(op.currentTime.Time) {
			op.currentState.openingTime = op.currentTime
		}
		return
	}
	if op.currentPlan.Id == plan.Id {
		op.Log.Info("can't set current plan with current plan, ignore")
		return
	}
	if !op.currentPlanExpired() {
		op.Log.Info("current plan not expired, ignore")
		return
	}
	expirationTime := op.calculateExpirationTime()
	if plan.CreateTime.Before(&expirationTime) {
		//expirationTime := op.calculateExpirationTime()
		plan.startTime = expirationTime
	} else {
		plan.startTime = op.currentTime
		// gap
		// -- [expirationTime] -- [currentTime] -- [createTime] -->
		if plan.CreateTime.After(op.currentTime.Time) {
			plan.CreateTime = op.currentTime
		}
		op.currentState.openingTime = op.currentTime
	}
	op.currentPlan = &plan
	op.Log.Info("set current plan finish", "current plan id", op.currentPlan.Id, "start time", op.currentPlan.startTime)
}

// Calculate `*Time`, for given active plan, Calculate is idempotent
func (op *monthlyPlanOperator) Calculate() {
	op.Log.Info("calculate start", "current plan", op.currentPlan, "start time", op.currentPlan.startTime)
	if nil == op.currentPlan || op.currentPlanExpired() {
		var nextPlan *monthlyplan
		nextPlan, op.targetPlans = op.NextPlan()
		op.Log.Info("get next plan", "next plan", nextPlan, "target plans", len(op.targetPlans))
		if nil != nextPlan {
			op.SetCurrentPlan(*nextPlan)
			op.Calculate()
		} else {
			// last plan
			op.currentPlan = nil
			op.Log.Info("last plan, calculate finish")
			return
		}
	}
	op.Log.Info("calculate finish", "current plan", op.currentPlan, "start time", op.currentPlan.startTime)
	return
}

func (op *monthlyPlanOperator) calculateExpirationTime() metav1.MicroTime {
	if MeteringConfig.Debug {
		return metav1.MicroTime{
			Time: op.currentPlan.startTime.Add(time.Duration(MeteringConfig.Ratio) * time.Second),
		}
	} else {
		monthNum, _ := op.currentPlan.Config.GetMonthNum()
		return metav1.MicroTime{
			Time: op.currentPlan.startTime.AddDate(0, int(monthNum), 0),
		}
	}
}

func (op *monthlyPlanOperator) ExpirationTime() metav1.MicroTime {
	if nil == op.currentPlan {
		//op.Log.Info("get expiration time from current time", "current time", op.currentTime)
		return op.currentTime
	}
	return op.calculateExpirationTime()
}

// currentPlanExpired calulate if current plan is expired
func (op *monthlyPlanOperator) currentPlanExpired() bool {
	if nil == op.currentPlan {
		return true
	}
	expirationTime := op.calculateExpirationTime()
	//op.Log.Info("currentPlanExpired: get expiration time", "expiration time", expirationTime, "start time", op.currentPlan.startTime)
	if op.currentTime.Equal(&expirationTime) || op.currentTime.After(expirationTime.Time) {
		op.Log.Info("current plan expired", "id", op.currentPlan.Id, "expiration time", expirationTime, "current time", op.currentTime)
		return true
	}
	return false
}

// Expired calulate if all plan is expired
func (op *monthlyPlanOperator) Expired() bool {
	if op.currentPlanExpired() && len(op.targetPlans) == 0 {
		return true
	}
	return false
}

func (op *monthlyPlanOperator) SetResult(service *ampv1.ApiProductService) bool {
	if nil == service {
		return false
	}
	var updateSpec bool
	if len(service.Spec.SubscriptionPlans) != len(op.targetPlans) {
		var plans []ampv1.SubscriptionPlan
		for _, plan := range op.targetPlans {
			plans = append(plans, plan.SubscriptionPlan)
		}
		service.Spec.SubscriptionPlans = plans
		updateSpec = true
	}
	if op.Expired() {
		expirationTime := op.ExpirationTime()
		service.Status.ExpirationTime = &expirationTime
		service.Status.OpeningTime = nil
		service.Status.TerminationTime = nil
		service.Status.Condition = nil
		service.Status.Phase = ampv1.ServiceExpired
		op.Log.Info("set result finish", "phase", service.Status.Phase, "nextSchedileTime", op.NextScheduleTime(), "condition", service.Status.Condition)
		return updateSpec
	}
	condition := &ampv1.APIProductServiceCondition{
		CurrentPlan:      op.currentPlan.SubscriptionPlan,
		LastScheduleTime: op.currentTime,
		APICalls:         0,
		DataPlan:         *resource.NewQuantity(0, resource.DecimalSI),
		StartTime:        op.currentPlan.startTime,
	}
	service.Status.Condition = condition
	service.Status.Phase = ampv1.ServiceOpened
	service.Status.TerminationTime = nil
	service.Status.OpeningTime = &op.currentState.openingTime
	service.Status.ExpirationTime = nil
	op.Log.Info("set result finish", "phase", service.Status.Phase, "nextSchedileTime", op.NextScheduleTime(), "condition", service.Status.Condition)
	return updateSpec
}

func (op *monthlyPlanOperator) NextScheduleTime() time.Duration {
	interval := time.Duration(MeteringConfig.MonthlyInterval)
	if op.Expired() {
		return 0
	}
	expirationTime := op.ExpirationTime()
	delta := expirationTime.Sub(op.currentTime.Time)
	if delta < interval {
		return delta
	}
	return interval
}

func NewMonthlyPlanOperator(service *ampv1.ApiProductService, lease *ampv1.Lease, eventTime metav1.MicroTime, logger logr.Logger) (Operator, error) {
	if nil == service || nil == lease {
		return nil, nil
	}
	var targetPlans []monthlyplan
	for _, plan := range service.Spec.SubscriptionPlans {
		targetPlans = append(targetPlans, monthlyplan{SubscriptionPlan: plan})
	}
	operator := &monthlyPlanOperator{
		currentTime: eventTime,
		currentPlan: nil,
		targetPlans: targetPlans,
		Log:         logger,
	}
	openingTime := service.Status.OpeningTime
	var currentPlan *monthlyplan
	if nil != service.Status.Condition {
		if nil == openingTime || openingTime.After(service.Status.Condition.StartTime.Time) {
			operator.Log.Info("get opening time from start time", "start time", service.Status.Condition.StartTime)
			openingTime = &service.Status.Condition.StartTime
		}
		currentPlan = &monthlyplan{
			SubscriptionPlan: service.Status.Condition.CurrentPlan,
			startTime:        service.Status.Condition.StartTime,
		}
		// just in case
		if currentPlan.startTime.After(operator.currentTime.Time) {
			operator.Log.Info("get opening time from current time")
			openingTime = &operator.currentTime
		}
	}
	if nil == currentPlan {
		currentPlan, operator.targetPlans = operator.NextPlan()
	}
	if nil == currentPlan {
		return nil, errors.New("No subscription plan available")
	}
	if nil == openingTime {
		openingTime = &eventTime
	}
	operator.currentState = state{
		openingTime: *openingTime,
	}
	operator.SetCurrentPlan(*currentPlan)
	operator.Log.Info("get operator", "current plan", operator.currentPlan.Id, "current time", operator.currentTime, "start time", operator.currentPlan.startTime, "opening time", operator.currentState.openingTime, "expiration time", operator.ExpirationTime())
	return operator, nil
}
