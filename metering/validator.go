package metering

import (
	"errors"
	"fmt"
	"regexp"

	"k8s.io/apimachinery/pkg/api/resource"

	ampv1 "alauda.io/amp-controller/api/v1"
)

type Validator interface {
	SubScriptionPlanValidator
}

type SubScriptionPlanValidator interface {
	ValidateSubScriptionPlan(plan ampv1.SubscriptionPlan) error
}

type validator struct{}

func (v *validator) ValidateSubScriptionPlan(plan ampv1.SubscriptionPlan) error {
	switch plan.Type {
	case ampv1.LongTime:
	case ampv1.MonlthlyPlan:
		monthNum, err := plan.Config.GetMonthNum()
		if nil != err {
			return fmt.Errorf("error get monthly config, err=%s", err.Error())
		}
		if monthNum <= 0 {
			return errors.New("monthly subscription plan should greater than zero")
		}
	case ampv1.PricingPackage:
		name := plan.Name
		re := regexp.MustCompile(`(?m)^[a-z][a-z0-9-]{0,61}\d$`)
		if !re.MatchString(name) {
			return fmt.Errorf("pricing package name %q does not valid, %q prefered", name, re.String())
		}
		validity, err := plan.Config.GetValidity()
		if nil != err {
			return err
		}
		if validity <= 0 {
			return fmt.Errorf("the validity period of the pricing package %q should be greater than zero", name)
		}
		d, err := plan.Config.GetDataPlan()
		if nil != err {
			return err
		}
		dataPlan, err := resource.ParseQuantity(d)
		if nil != err {
			return err
		}
		if dataPlan.Value() <= 0 {
			return errors.New("data plan should be greater than zero")
		}
	default:
		return fmt.Errorf("unsuport suscription plan type: %s", plan.Type)
	}
	return nil
}

func NewValidator() Validator {
	return &validator{}
}
