package metering

import (
	"context"
	"errors"
	"flag"
	"fmt"
	"net/url"
	"time"

	"github.com/olivere/elastic"
	logf "sigs.k8s.io/controller-runtime/pkg/log"
)

var log = logf.Log.WithName("metering")

var (
	MeteringConfig = Config{}
)

type Config struct {
	AllowCorrupt           bool
	URL                    string
	Username               string
	Passwd                 string
	Ratio                  int64
	StandBy                bool
	MonthlyInterval        int64
	PricingPackageInterval int64
	Debug                  bool
	//Client                 *elastic.Client
}

func (c *Config) Validate() error {
	MeteringConfig.StandBy = c.StandBy
	if MeteringConfig.StandBy {
		return nil
	}
	var err error
	log.Info("elasticsearch", "esURL", c.URL)
	if "" == c.URL {
		return errors.New("elasticsearch-url is empty")
	}
	if _, err = url.Parse(c.URL); nil != err {
		return errors.New("elasticsearch-url is empty")
	}
	client, err := elastic.NewClient(
		elastic.SetURL(c.URL),
		elastic.SetBasicAuth(c.Username, c.Passwd),
		elastic.SetSniff(false),
		elastic.SetHealthcheck(false),
	)
	if nil != err {
		return err
	}
	defer client.Stop()
	if info, code, err := client.Ping(c.URL).Do(context.Background()); nil != err {
		return err
	} else {
		log.Info(fmt.Sprintf("elasticsearch returned with code %d and version %s\n", code, info.Version.Number))
	}
	return nil
}

// AddMeteringFlags parse the flags
func AddMeteringFlags(c *Config) {
	//var esURL, esUser, esPasswd string
	//var ratio, monthlyInterval, pricingPackageInterval int64
	//var standBy, allowCorrupt bool
	//var err error
	flag.StringVar(&c.URL, "elasticsearch-url", "", "elasticsearch url")
	flag.StringVar(&c.Username, "elasticsearch-user", "", "elasticsearch username")
	flag.StringVar(&c.Passwd, "elasticsearch-password", "", "elasticsearch password")
	flag.Int64Var(&c.Ratio, "metering-ratio", 1, "ratio for metering")
	flag.Int64Var(&c.MonthlyInterval, "metering-monthly-interval", int64(1*24*time.Hour), "interval for monthly metering in nanosecond")
	flag.Int64Var(&c.PricingPackageInterval, "metering-pricing-package-interval", int64(15*time.Second), "interval for pricing package metering in nanosecond")
	flag.BoolVar(&c.StandBy, "metering-standby", true, "metering standby mode")
	flag.BoolVar(&c.AllowCorrupt, "metering-allow-corrupt", true, "continue to serve subscription plans when last schedule time is missing")
	//flag.Parse()
}
