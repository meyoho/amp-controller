# Build the manager binary
FROM index.alauda.cn/alaudaorg/alaudabase-alpine-go:1.13-alpine3.10 as builder

# Copy in the go src
WORKDIR /go/src/alauda.io/amp-controller
COPY ./ ./

# Build
RUN CGO_ENABLED=0 GOOS=linux GOARCH=amd64 GO111MODULE=off go build -a -o bin/manager

# Copy the controller-manager into a thin image
FROM alpine:3.10
WORKDIR /
COPY --from=builder /go/src/alauda.io/amp-controller/bin/manager .
ENTRYPOINT ["/manager"]
