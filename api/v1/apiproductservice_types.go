/*

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package v1

import (
	"k8s.io/apimachinery/pkg/api/resource"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

// EDIT THIS FILE!  THIS IS SCAFFOLDING FOR YOU TO OWN!
// NOTE: json tags are required.  Any new fields you add must have json tags for the fields to be serialized.

type ApiProductServicePhase string
type APInfo map[string]string

const (
	ServiceOpened           ApiProductServicePhase = "opened"
	ServiceExpired          ApiProductServicePhase = "expired"
	ServiceTerminated       ApiProductServicePhase = "terminated"
	ServiceOverdue          ApiProductServicePhase = "overdue"
	ServicePhaseEmpty       ApiProductServicePhase = ""
	OAuth2ClientId                                 = "client_id"
	OAuth2ClientSecret                             = "client_secret"
	OAuth2TokenEndpoint                            = "token_endpoint"
	APIEndpoint                                    = "endpoint"
	APISecureEndpoint                              = "secure_endpoint"
	APISecurityType                                = "security_type"
	APIPluginID                                    = "id"
	JWTAlgorithm                                   = "algorithm"
	JWTRSAPublicKey                                = "rsa_public_key"
	Beareroken                                     = "bearer_token"
	JWTKey                                         = "key"
	JWTSecret                                      = "secret"
	BasicUserName                                  = "username"
	BasicPassword                                  = "password"
	BasicToken                                     = "basic_token"
	PendingConfigAnnotation                        = "amp.alauda.io/pending-configuration"
)

// ApiProductServiceSpec defines the desired state of ApiProductService
type ApiProductServiceSpec struct {
	// INSERT ADDITIONAL SPEC FIELDS - desired state of cluster
	// Important: Run "make" to regenerate code after modifying this file
	ID                     string                 `json:"id"`
	ProductName            string                 `json:"product_name"`
	ProductDisplayName     string                 `json:"product_display_name,omitempty"`
	OwnerUserName          string                 `json:"owner_user_name,omitempty"`
	DeveloperName          string                 `json:"developer_name,omitempty"`
	ConsumeApplicationName string                 `json:"consume_application_name"`
	Info                   APInfo                 `json:"api_info"`
	SubscriptionPlans      SubscriptionPlans      `json:"subscription_plans,omitempty"`
	SubscriptionStatus     ApiProductServicePhase `json:"subscription_status,omitempty"`
	ExpirationTime         *metav1.Time           `json:"expiration_time,omitempty"`

	//SubscriptionPlan       *SubscriptionType `json:"subscription_plan,omitempty"`
	// SubscriptionPlan is deprecrated for legancy, need data migration
	//OrderName              string              `json:"order_name,omitempty"`
	// app_key: ''
	// app_secret: ''
	// endpoint: '' # string, http(s)://<gateway-address>/<api-product-name>
}

func (service *ApiProductService) SubscriptionPlanLongTime() bool {
	if nil != service.Status.Condition {
		if LongTime == service.Status.Condition.CurrentPlan.Type {
			return true
		}
	}
	return service.Spec.SubscriptionPlans.HasType(LongTime)
}

func (service *ApiProductService) SubscriptionPlanType() SubscriptionType {
	if nil != service.Status.Condition {
		return service.Status.Condition.CurrentPlan.Type
	}
	return service.Spec.SubscriptionPlans.Type()
}

type APIProductServiceCondition struct {
	// Active is the subscription id of which this lease serves
	//ActivePlan       *string           `json:"active_plan,omitempty"`

	// CurrentPlan is the subscription plan that was last observed in processing
	// +optional
	CurrentPlan SubscriptionPlan `json:"current_plan,omitempty"`

	LastScheduleTime metav1.MicroTime `json:"last_schedule_time,omitempty"`
	//NextScheduleTime metav1.MicroTime `json:"next_schedule_time,omitempty"`

	// +optional
	//SubscriptionStatus ApiProductServicePhase `json:"subscription_status,omitempty"`

	// APICalls is the number of times the api is called
	// +optional
	APICalls int64 `json:"api_calls,omitempty"`

	// DataPlan refers to data quotas for egress data
	// +optional
	DataPlan resource.Quantity `json:"data_plan,omitempty"`

	// OpeningTime is when current plan is started
	// +optional
	StartTime metav1.MicroTime `json:"start_time,omitempty"`
}

// ApiProductServiceStatus defines the observed state of ApiProductService
type ApiProductServiceStatus struct {
	// INSERT ADDITIONAL STATUS FIELD - define observed state of cluster
	// Important: Run "make" to regenerate code after modifying this file

	// DeployStatus describes the status of configuration synchronization to Kong
	// +optional
	DeployStatus ProductDeployPhase `json:"deploy_status,omitempty"`

	Message string `json:"message,omitempty"`

	Phase ApiProductServicePhase `json:"phase,omitempty"`

	// Condition for metering
	Condition *APIProductServiceCondition `json:"condition,omitempty"`

	// OpeningTime is when api product service is ready
	// +optional
	OpeningTime *metav1.MicroTime `json:"opening_time,omitempty"`

	// ExpirationTime is when api product service is expired
	// +optional
	ExpirationTime *metav1.MicroTime `json:"expiration_time,omitempty"`

	// when api product service is terminated
	// +optional
	TerminationTime *metav1.Time `json:"termination_time,omitempty"`
}

// +kubebuilder:object:root=true

// ApiProductService is the Schema for the apiproductservices API
type ApiProductService struct {
	metav1.TypeMeta   `json:",inline"`
	metav1.ObjectMeta `json:"metadata,omitempty"`

	Spec   ApiProductServiceSpec   `json:"spec,omitempty"`
	Status ApiProductServiceStatus `json:"status,omitempty"`
}

// +kubebuilder:object:root=true

// ApiProductServiceList contains a list of ApiProductService
type ApiProductServiceList struct {
	metav1.TypeMeta `json:",inline"`
	metav1.ListMeta `json:"metadata,omitempty"`
	Items           []ApiProductService `json:"items"`
}

func init() {
	SchemeBuilder.Register(&ApiProductService{}, &ApiProductServiceList{})
}
