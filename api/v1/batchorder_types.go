package v1

import (
	metaV1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

const (
	BatchOrderFlag         = "amp.alauda.io/batch-order"
	ReviewOrderCreated     = "amp.alauda.io/review-order"
	ReviewOrderCreatedTrue = "true"
)

type BatchOrderDeployPhase string

const (
	// ProductDeployed means all of the object product has been deployed to Kong
	BatchOrderDeployed BatchOrderDeployPhase = "deployed"
	// ProductError means some server error happened, and the task will be retried
	BatchOrderDeployError BatchOrderDeployPhase = "error" // will retry
	// ProductError means some client error happened, and the task will not be retried
	BatchOrderDeployFailed BatchOrderDeployPhase = "failed"
)

type Product struct {
	ProductName        string              `json:"product_name"`
	ProductProjectName string              `json:"product_project_name"`
	Developer          string              `json:"product_developer_name"`
	Status             OrderApprovalStatus `json:"approval_status"`
	ApprovalOpinion    string              `json:"approval_opinion"`
	ApprovalTime       *metaV1.Time        `json:"approval_time"`
}

// BatchOrderSpec defines the desired state of BatchOrder
type BatchorderSpec struct {
	ID                            string           `json:"id"`
	OrderName                     string           `json:"order_name"`
	OwnerUserName                 string           `json:"owner_user_name"`
	ProductList                   []Product        `json:"products"`
	SubscriptionPlan              SubscriptionPlan `json:"subscription_plan,omitempty"`
	Security                      Security         `json:"security,omitempty"`
	ConsumeApplicationName        string           `json:"consume_application_name"`
	ConsumeApplicationID          string           `json:"consume_application_id"`
	ConsumeApplicationProjectName string           `json:"consume_application_project_name"`
	CreateTime                    *metaV1.Time     `json:"create_time"`
	CreateUserName                string           `json:"create_user_name"`
	UpdateTime                    *metaV1.Time     `json:"update_time"`
	UpdateUserName                string           `json:"update_user_name"`
}

// BatchOrderStatus defines the observed state of BatchOrder
type BatchorderStatus struct {
	// INSERT ADDITIONAL STATUS FIELD - define observed state of cluster
	// Important: Run "make" to regenerate code after modifying this file
	DeployStatus BatchOrderDeployPhase `json:"deploy_status,omitempty"`
	Message      string                `json:"message,omitempty"`
}

// +kubebuilder:object:root=true

// BatchOrder is the Schema for the BatchOrder API
type Batchorder struct {
	metaV1.TypeMeta   `json:",inline"`
	metaV1.ObjectMeta `json:"metadata,omitempty"`

	Spec   BatchorderSpec   `json:"spec,omitempty"`
	Status BatchorderStatus `json:"status,omitempty"`
}

// +kubebuilder:object:root=true

// BatchOrderList contains a list of BatchOrder
type BatchorderList struct {
	metaV1.TypeMeta `json:",inline"`
	metaV1.ListMeta `json:"metadata,omitempty"`
	Items           []Batchorder `json:"items"`
}

func init() {
	SchemeBuilder.Register(&Batchorder{}, &BatchorderList{})
}
