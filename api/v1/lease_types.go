/*

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package v1

import (
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

// EDIT THIS FILE!  THIS IS SCAFFOLDING FOR YOU TO OWN!
// NOTE: json tags are required.  Any new fields you add must have json tags for the fields to be serialized.

const (
	LeaseExpired    LeasePhase = "expired"
	LeaseOpened     LeasePhase = "opened"
	LeaseTerminated LeasePhase = "terminated"
)

type LeaseType string
type LeasePhase string

// LeaseSpec defines the desired state of Lease
type LeaseSpec struct {
	// INSERT ADDITIONAL SPEC FIELDS - desired state of cluster
	// Important: Run "make" to regenerate code after modifying this file

	// Type represents the type of lease
	//Type LeaseType `json:"type"`

	APIProductSericeName string `json:"api_product_serice_name"`

	// AcquireTime is the time when the current lease was acquired, it will bring a event
	AcquireTime *metav1.MicroTime `json:"acquire_time,omitempty"`
	//SubscriptionPlans []*SubscriptionPlan `json:"subscription_plans,omitempty"`

	// Validity is the state of being legally. In days.
	// +optional
	//Validaty int64 `json:"validaty,omitempty"`
	// APICalls is the number of times the api is called
	// +optional
	//APICalls int64 `json:"api_calls,omitempty"`
	// DataPlan refers to data quotas for egress data
	// +optional
	//DataPlan resource.Quantity `json:"data_plan,omitempty"`
}

// LeaseStatus defines the observed state of Lease
type LeaseStatus struct {
	// INSERT ADDITIONAL STATUS FIELD - define observed state of cluster
	// Important: Run "make" to regenerate code after modifying this file

	//Phase LeasePhase `json:"phase,omitempty"`
	// acquireTime is a time when the current lease was acquired.
	//AcquireTime *metav1.MicroTime `json:"acquire_time"`
	// TTL defines the time to live for this lease in Days.
	//TTL int64 `json:"ttl,omitempty"`
	// APICalls is the number of times the api is called
	// +optional
	//APICalls int64 `json:"api_calls,omitempty"`
	// DataPlan refers to data quotas for egress data
	// +optional
	//DataPlan resource.Quantity `json:"data_plan,omitempty"`
}

// +kubebuilder:object:root=true

// Lease is the Schema for the leases API
type Lease struct {
	metav1.TypeMeta   `json:",inline"`
	metav1.ObjectMeta `json:"metadata,omitempty"`

	Spec   LeaseSpec   `json:"spec,omitempty"`
	Status LeaseStatus `json:"status,omitempty"`
}

func (l *Lease) Acquire() {
	now := metav1.NowMicro()
	l.Spec.AcquireTime = &now
}

// +kubebuilder:object:root=true

// LeaseList contains a list of Lease
type LeaseList struct {
	metav1.TypeMeta `json:",inline"`
	metav1.ListMeta `json:"metadata,omitempty"`
	Items           []Lease `json:"items"`
}

func init() {
	SchemeBuilder.Register(&Lease{}, &LeaseList{})
}
