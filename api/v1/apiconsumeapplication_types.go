/*

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package v1

import (
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

const (
	ConsumerSelfProductName          = "amp.alauda.io/consumer-self-product"
	ConsumerCertificationSynced      = "amp.alauda.io/consumer-synced"
	ConsumerCertificationSyncedFalse = "false"
	ConsumerCertificationSyncedTrue  = "true"
)

// EDIT THIS FILE!  THIS IS SCAFFOLDING FOR YOU TO OWN!
// NOTE: json tags are required.  Any new fields you add must have json tags for the fields to be serialized.

// ApiConsumeApplicationSpec defines the desired state of ApiConsumeApplication
type ApiConsumeApplicationSpec struct {
	// INSERT ADDITIONAL SPEC FIELDS - desired state of cluster
	// Important: Run "make" to regenerate code after modifying this file
	Id             string       `json:"id"`
	Name           string       `json:"name"`
	ProjectName    string       `json:"project_name"`
	ClientId       string       `json:"client_id"`
	DisplanName    string       `json:"display_name"`
	Description    string       `json:"description"`
	OwnerUserName  string       `json:"owner_user_name"`
	CreateTime     *metav1.Time `json:"create_time"`
	CreateUserName string       `json:"create_user_name"`
	UpdateTime     *metav1.Time `json:"update_time"`
	UpdateUserName string       `json:"update_user_name"`
	Info           []APInfo     `json:"api_info"`
}

// ApiConsumeApplicationStatus defines the observed state of ApiConsumeApplication
type ApiConsumeApplicationStatus struct {
	// INSERT ADDITIONAL STATUS FIELD - define observed state of cluster
	// Important: Run "make" to regenerate code after modifying this file
	Message string `json:"message,omitempty"`
}

// +kubebuilder:object:root=true

// ApiConsumeApplication is the Schema for the apiconsumeapplications API
type ApiConsumeApplication struct {
	metav1.TypeMeta   `json:",inline"`
	metav1.ObjectMeta `json:"metadata,omitempty"`

	Spec   ApiConsumeApplicationSpec   `json:"spec,omitempty"`
	Status ApiConsumeApplicationStatus `json:"status,omitempty"`
}

// +kubebuilder:object:root=true

// ApiConsumeApplicationList contains a list of ApiConsumeApplication
type ApiConsumeApplicationList struct {
	metav1.TypeMeta `json:",inline"`
	metav1.ListMeta `json:"metadata,omitempty"`
	Items           []ApiConsumeApplication `json:"items"`
}

func init() {
	SchemeBuilder.Register(&ApiConsumeApplication{}, &ApiConsumeApplicationList{})
}
