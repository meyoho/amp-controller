// +build !ignore_autogenerated

/*

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

// Code generated by controller-gen. DO NOT EDIT.

package v1

import (
	"k8s.io/apimachinery/pkg/runtime"
)

// DeepCopyInto is an autogenerated deepcopy function, copying the receiver, writing into out. in must be non-nil.
func (in *APIProductServiceCondition) DeepCopyInto(out *APIProductServiceCondition) {
	*out = *in
	in.CurrentPlan.DeepCopyInto(&out.CurrentPlan)
	in.LastScheduleTime.DeepCopyInto(&out.LastScheduleTime)
	out.DataPlan = in.DataPlan.DeepCopy()
	in.StartTime.DeepCopyInto(&out.StartTime)
}

// DeepCopy is an autogenerated deepcopy function, copying the receiver, creating a new APIProductServiceCondition.
func (in *APIProductServiceCondition) DeepCopy() *APIProductServiceCondition {
	if in == nil {
		return nil
	}
	out := new(APIProductServiceCondition)
	in.DeepCopyInto(out)
	return out
}

// DeepCopyInto is an autogenerated deepcopy function, copying the receiver, writing into out. in must be non-nil.
func (in APInfo) DeepCopyInto(out *APInfo) {
	{
		in := &in
		*out = make(APInfo, len(*in))
		for key, val := range *in {
			(*out)[key] = val
		}
	}
}

// DeepCopy is an autogenerated deepcopy function, copying the receiver, creating a new APInfo.
func (in APInfo) DeepCopy() APInfo {
	if in == nil {
		return nil
	}
	out := new(APInfo)
	in.DeepCopyInto(out)
	return *out
}

// DeepCopyInto is an autogenerated deepcopy function, copying the receiver, writing into out. in must be non-nil.
func (in *ApiConsumeApplication) DeepCopyInto(out *ApiConsumeApplication) {
	*out = *in
	out.TypeMeta = in.TypeMeta
	in.ObjectMeta.DeepCopyInto(&out.ObjectMeta)
	in.Spec.DeepCopyInto(&out.Spec)
	out.Status = in.Status
}

// DeepCopy is an autogenerated deepcopy function, copying the receiver, creating a new ApiConsumeApplication.
func (in *ApiConsumeApplication) DeepCopy() *ApiConsumeApplication {
	if in == nil {
		return nil
	}
	out := new(ApiConsumeApplication)
	in.DeepCopyInto(out)
	return out
}

// DeepCopyObject is an autogenerated deepcopy function, copying the receiver, creating a new runtime.Object.
func (in *ApiConsumeApplication) DeepCopyObject() runtime.Object {
	if c := in.DeepCopy(); c != nil {
		return c
	}
	return nil
}

// DeepCopyInto is an autogenerated deepcopy function, copying the receiver, writing into out. in must be non-nil.
func (in *ApiConsumeApplicationList) DeepCopyInto(out *ApiConsumeApplicationList) {
	*out = *in
	out.TypeMeta = in.TypeMeta
	in.ListMeta.DeepCopyInto(&out.ListMeta)
	if in.Items != nil {
		in, out := &in.Items, &out.Items
		*out = make([]ApiConsumeApplication, len(*in))
		for i := range *in {
			(*in)[i].DeepCopyInto(&(*out)[i])
		}
	}
}

// DeepCopy is an autogenerated deepcopy function, copying the receiver, creating a new ApiConsumeApplicationList.
func (in *ApiConsumeApplicationList) DeepCopy() *ApiConsumeApplicationList {
	if in == nil {
		return nil
	}
	out := new(ApiConsumeApplicationList)
	in.DeepCopyInto(out)
	return out
}

// DeepCopyObject is an autogenerated deepcopy function, copying the receiver, creating a new runtime.Object.
func (in *ApiConsumeApplicationList) DeepCopyObject() runtime.Object {
	if c := in.DeepCopy(); c != nil {
		return c
	}
	return nil
}

// DeepCopyInto is an autogenerated deepcopy function, copying the receiver, writing into out. in must be non-nil.
func (in *ApiConsumeApplicationSpec) DeepCopyInto(out *ApiConsumeApplicationSpec) {
	*out = *in
	if in.CreateTime != nil {
		in, out := &in.CreateTime, &out.CreateTime
		*out = (*in).DeepCopy()
	}
	if in.UpdateTime != nil {
		in, out := &in.UpdateTime, &out.UpdateTime
		*out = (*in).DeepCopy()
	}
	if in.Info != nil {
		in, out := &in.Info, &out.Info
		*out = make([]APInfo, len(*in))
		for i := range *in {
			if (*in)[i] != nil {
				in, out := &(*in)[i], &(*out)[i]
				*out = make(APInfo, len(*in))
				for key, val := range *in {
					(*out)[key] = val
				}
			}
		}
	}
}

// DeepCopy is an autogenerated deepcopy function, copying the receiver, creating a new ApiConsumeApplicationSpec.
func (in *ApiConsumeApplicationSpec) DeepCopy() *ApiConsumeApplicationSpec {
	if in == nil {
		return nil
	}
	out := new(ApiConsumeApplicationSpec)
	in.DeepCopyInto(out)
	return out
}

// DeepCopyInto is an autogenerated deepcopy function, copying the receiver, writing into out. in must be non-nil.
func (in *ApiConsumeApplicationStatus) DeepCopyInto(out *ApiConsumeApplicationStatus) {
	*out = *in
}

// DeepCopy is an autogenerated deepcopy function, copying the receiver, creating a new ApiConsumeApplicationStatus.
func (in *ApiConsumeApplicationStatus) DeepCopy() *ApiConsumeApplicationStatus {
	if in == nil {
		return nil
	}
	out := new(ApiConsumeApplicationStatus)
	in.DeepCopyInto(out)
	return out
}

// DeepCopyInto is an autogenerated deepcopy function, copying the receiver, writing into out. in must be non-nil.
func (in *ApiDefinition) DeepCopyInto(out *ApiDefinition) {
	*out = *in
	if in.HttpProtocol != nil {
		in, out := &in.HttpProtocol, &out.HttpProtocol
		*out = make([]string, len(*in))
		copy(*out, *in)
	}
	if in.ApiPaths != nil {
		in, out := &in.ApiPaths, &out.ApiPaths
		*out = make([]*ApiPath, len(*in))
		for i := range *in {
			if (*in)[i] != nil {
				in, out := &(*in)[i], &(*out)[i]
				*out = new(ApiPath)
				(*in).DeepCopyInto(*out)
			}
		}
	}
	if in.ApiSecurityMode != nil {
		in, out := &in.ApiSecurityMode, &out.ApiSecurityMode
		*out = new(ApiStrategy)
		(*in).DeepCopyInto(*out)
	}
	in.ApiService.DeepCopyInto(&out.ApiService)
}

// DeepCopy is an autogenerated deepcopy function, copying the receiver, creating a new ApiDefinition.
func (in *ApiDefinition) DeepCopy() *ApiDefinition {
	if in == nil {
		return nil
	}
	out := new(ApiDefinition)
	in.DeepCopyInto(out)
	return out
}

// DeepCopyInto is an autogenerated deepcopy function, copying the receiver, writing into out. in must be non-nil.
func (in *ApiPath) DeepCopyInto(out *ApiPath) {
	*out = *in
	if in.HttpMethod != nil {
		in, out := &in.HttpMethod, &out.HttpMethod
		*out = make([]string, len(*in))
		copy(*out, *in)
	}
	if in.Strategies != nil {
		in, out := &in.Strategies, &out.Strategies
		*out = make([]*ApiStrategy, len(*in))
		for i := range *in {
			if (*in)[i] != nil {
				in, out := &(*in)[i], &(*out)[i]
				*out = new(ApiStrategy)
				(*in).DeepCopyInto(*out)
			}
		}
	}
}

// DeepCopy is an autogenerated deepcopy function, copying the receiver, creating a new ApiPath.
func (in *ApiPath) DeepCopy() *ApiPath {
	if in == nil {
		return nil
	}
	out := new(ApiPath)
	in.DeepCopyInto(out)
	return out
}

// DeepCopyInto is an autogenerated deepcopy function, copying the receiver, writing into out. in must be non-nil.
func (in *ApiProduct) DeepCopyInto(out *ApiProduct) {
	*out = *in
	out.TypeMeta = in.TypeMeta
	in.ObjectMeta.DeepCopyInto(&out.ObjectMeta)
	in.Spec.DeepCopyInto(&out.Spec)
	in.Status.DeepCopyInto(&out.Status)
}

// DeepCopy is an autogenerated deepcopy function, copying the receiver, creating a new ApiProduct.
func (in *ApiProduct) DeepCopy() *ApiProduct {
	if in == nil {
		return nil
	}
	out := new(ApiProduct)
	in.DeepCopyInto(out)
	return out
}

// DeepCopyObject is an autogenerated deepcopy function, copying the receiver, creating a new runtime.Object.
func (in *ApiProduct) DeepCopyObject() runtime.Object {
	if c := in.DeepCopy(); c != nil {
		return c
	}
	return nil
}

// DeepCopyInto is an autogenerated deepcopy function, copying the receiver, writing into out. in must be non-nil.
func (in *ApiProductList) DeepCopyInto(out *ApiProductList) {
	*out = *in
	out.TypeMeta = in.TypeMeta
	in.ListMeta.DeepCopyInto(&out.ListMeta)
	if in.Items != nil {
		in, out := &in.Items, &out.Items
		*out = make([]ApiProduct, len(*in))
		for i := range *in {
			(*in)[i].DeepCopyInto(&(*out)[i])
		}
	}
}

// DeepCopy is an autogenerated deepcopy function, copying the receiver, creating a new ApiProductList.
func (in *ApiProductList) DeepCopy() *ApiProductList {
	if in == nil {
		return nil
	}
	out := new(ApiProductList)
	in.DeepCopyInto(out)
	return out
}

// DeepCopyObject is an autogenerated deepcopy function, copying the receiver, creating a new runtime.Object.
func (in *ApiProductList) DeepCopyObject() runtime.Object {
	if c := in.DeepCopy(); c != nil {
		return c
	}
	return nil
}

// DeepCopyInto is an autogenerated deepcopy function, copying the receiver, writing into out. in must be non-nil.
func (in *ApiProductService) DeepCopyInto(out *ApiProductService) {
	*out = *in
	out.TypeMeta = in.TypeMeta
	in.ObjectMeta.DeepCopyInto(&out.ObjectMeta)
	in.Spec.DeepCopyInto(&out.Spec)
	in.Status.DeepCopyInto(&out.Status)
}

// DeepCopy is an autogenerated deepcopy function, copying the receiver, creating a new ApiProductService.
func (in *ApiProductService) DeepCopy() *ApiProductService {
	if in == nil {
		return nil
	}
	out := new(ApiProductService)
	in.DeepCopyInto(out)
	return out
}

// DeepCopyObject is an autogenerated deepcopy function, copying the receiver, creating a new runtime.Object.
func (in *ApiProductService) DeepCopyObject() runtime.Object {
	if c := in.DeepCopy(); c != nil {
		return c
	}
	return nil
}

// DeepCopyInto is an autogenerated deepcopy function, copying the receiver, writing into out. in must be non-nil.
func (in *ApiProductServiceList) DeepCopyInto(out *ApiProductServiceList) {
	*out = *in
	out.TypeMeta = in.TypeMeta
	in.ListMeta.DeepCopyInto(&out.ListMeta)
	if in.Items != nil {
		in, out := &in.Items, &out.Items
		*out = make([]ApiProductService, len(*in))
		for i := range *in {
			(*in)[i].DeepCopyInto(&(*out)[i])
		}
	}
}

// DeepCopy is an autogenerated deepcopy function, copying the receiver, creating a new ApiProductServiceList.
func (in *ApiProductServiceList) DeepCopy() *ApiProductServiceList {
	if in == nil {
		return nil
	}
	out := new(ApiProductServiceList)
	in.DeepCopyInto(out)
	return out
}

// DeepCopyObject is an autogenerated deepcopy function, copying the receiver, creating a new runtime.Object.
func (in *ApiProductServiceList) DeepCopyObject() runtime.Object {
	if c := in.DeepCopy(); c != nil {
		return c
	}
	return nil
}

// DeepCopyInto is an autogenerated deepcopy function, copying the receiver, writing into out. in must be non-nil.
func (in *ApiProductServiceSpec) DeepCopyInto(out *ApiProductServiceSpec) {
	*out = *in
	if in.Info != nil {
		in, out := &in.Info, &out.Info
		*out = make(APInfo, len(*in))
		for key, val := range *in {
			(*out)[key] = val
		}
	}
	if in.SubscriptionPlans != nil {
		in, out := &in.SubscriptionPlans, &out.SubscriptionPlans
		*out = make(SubscriptionPlans, len(*in))
		for i := range *in {
			(*in)[i].DeepCopyInto(&(*out)[i])
		}
	}
	if in.ExpirationTime != nil {
		in, out := &in.ExpirationTime, &out.ExpirationTime
		*out = (*in).DeepCopy()
	}
}

// DeepCopy is an autogenerated deepcopy function, copying the receiver, creating a new ApiProductServiceSpec.
func (in *ApiProductServiceSpec) DeepCopy() *ApiProductServiceSpec {
	if in == nil {
		return nil
	}
	out := new(ApiProductServiceSpec)
	in.DeepCopyInto(out)
	return out
}

// DeepCopyInto is an autogenerated deepcopy function, copying the receiver, writing into out. in must be non-nil.
func (in *ApiProductServiceStatus) DeepCopyInto(out *ApiProductServiceStatus) {
	*out = *in
	if in.Condition != nil {
		in, out := &in.Condition, &out.Condition
		*out = new(APIProductServiceCondition)
		(*in).DeepCopyInto(*out)
	}
	if in.OpeningTime != nil {
		in, out := &in.OpeningTime, &out.OpeningTime
		*out = (*in).DeepCopy()
	}
	if in.ExpirationTime != nil {
		in, out := &in.ExpirationTime, &out.ExpirationTime
		*out = (*in).DeepCopy()
	}
	if in.TerminationTime != nil {
		in, out := &in.TerminationTime, &out.TerminationTime
		*out = (*in).DeepCopy()
	}
}

// DeepCopy is an autogenerated deepcopy function, copying the receiver, creating a new ApiProductServiceStatus.
func (in *ApiProductServiceStatus) DeepCopy() *ApiProductServiceStatus {
	if in == nil {
		return nil
	}
	out := new(ApiProductServiceStatus)
	in.DeepCopyInto(out)
	return out
}

// DeepCopyInto is an autogenerated deepcopy function, copying the receiver, writing into out. in must be non-nil.
func (in *ApiProductSpec) DeepCopyInto(out *ApiProductSpec) {
	*out = *in
	if in.Label != nil {
		in, out := &in.Label, &out.Label
		*out = make([]string, len(*in))
		copy(*out, *in)
	}
	in.ApiDefinition.DeepCopyInto(&out.ApiDefinition)
	if in.SubscriptionPlans != nil {
		in, out := &in.SubscriptionPlans, &out.SubscriptionPlans
		*out = make(SubscriptionPlans, len(*in))
		for i := range *in {
			(*in)[i].DeepCopyInto(&(*out)[i])
		}
	}
	if in.DocIds != nil {
		in, out := &in.DocIds, &out.DocIds
		*out = make([]string, len(*in))
		copy(*out, *in)
	}
	if in.CreateTime != nil {
		in, out := &in.CreateTime, &out.CreateTime
		*out = (*in).DeepCopy()
	}
	if in.UpdateTime != nil {
		in, out := &in.UpdateTime, &out.UpdateTime
		*out = (*in).DeepCopy()
	}
}

// DeepCopy is an autogenerated deepcopy function, copying the receiver, creating a new ApiProductSpec.
func (in *ApiProductSpec) DeepCopy() *ApiProductSpec {
	if in == nil {
		return nil
	}
	out := new(ApiProductSpec)
	in.DeepCopyInto(out)
	return out
}

// DeepCopyInto is an autogenerated deepcopy function, copying the receiver, writing into out. in must be non-nil.
func (in *ApiProductStatus) DeepCopyInto(out *ApiProductStatus) {
	*out = *in
	if in.DeployConditions != nil {
		in, out := &in.DeployConditions, &out.DeployConditions
		*out = make([]ProductDeployCondition, len(*in))
		for i := range *in {
			(*in)[i].DeepCopyInto(&(*out)[i])
		}
	}
}

// DeepCopy is an autogenerated deepcopy function, copying the receiver, creating a new ApiProductStatus.
func (in *ApiProductStatus) DeepCopy() *ApiProductStatus {
	if in == nil {
		return nil
	}
	out := new(ApiProductStatus)
	in.DeepCopyInto(out)
	return out
}

// DeepCopyInto is an autogenerated deepcopy function, copying the receiver, writing into out. in must be non-nil.
func (in *ApiService) DeepCopyInto(out *ApiService) {
	*out = *in
	if in.Strategies != nil {
		in, out := &in.Strategies, &out.Strategies
		*out = make([]*ApiStrategy, len(*in))
		for i := range *in {
			if (*in)[i] != nil {
				in, out := &(*in)[i], &(*out)[i]
				*out = new(ApiStrategy)
				(*in).DeepCopyInto(*out)
			}
		}
	}
}

// DeepCopy is an autogenerated deepcopy function, copying the receiver, creating a new ApiService.
func (in *ApiService) DeepCopy() *ApiService {
	if in == nil {
		return nil
	}
	out := new(ApiService)
	in.DeepCopyInto(out)
	return out
}

// DeepCopyInto is an autogenerated deepcopy function, copying the receiver, writing into out. in must be non-nil.
func (in *ApiStrategy) DeepCopyInto(out *ApiStrategy) {
	*out = *in
	in.Config.DeepCopyInto(&out.Config)
}

// DeepCopy is an autogenerated deepcopy function, copying the receiver, creating a new ApiStrategy.
func (in *ApiStrategy) DeepCopy() *ApiStrategy {
	if in == nil {
		return nil
	}
	out := new(ApiStrategy)
	in.DeepCopyInto(out)
	return out
}

// DeepCopyInto is an autogenerated deepcopy function, copying the receiver, writing into out. in must be non-nil.
func (in *Batchorder) DeepCopyInto(out *Batchorder) {
	*out = *in
	out.TypeMeta = in.TypeMeta
	in.ObjectMeta.DeepCopyInto(&out.ObjectMeta)
	in.Spec.DeepCopyInto(&out.Spec)
	out.Status = in.Status
}

// DeepCopy is an autogenerated deepcopy function, copying the receiver, creating a new Batchorder.
func (in *Batchorder) DeepCopy() *Batchorder {
	if in == nil {
		return nil
	}
	out := new(Batchorder)
	in.DeepCopyInto(out)
	return out
}

// DeepCopyObject is an autogenerated deepcopy function, copying the receiver, creating a new runtime.Object.
func (in *Batchorder) DeepCopyObject() runtime.Object {
	if c := in.DeepCopy(); c != nil {
		return c
	}
	return nil
}

// DeepCopyInto is an autogenerated deepcopy function, copying the receiver, writing into out. in must be non-nil.
func (in *BatchorderList) DeepCopyInto(out *BatchorderList) {
	*out = *in
	out.TypeMeta = in.TypeMeta
	in.ListMeta.DeepCopyInto(&out.ListMeta)
	if in.Items != nil {
		in, out := &in.Items, &out.Items
		*out = make([]Batchorder, len(*in))
		for i := range *in {
			(*in)[i].DeepCopyInto(&(*out)[i])
		}
	}
}

// DeepCopy is an autogenerated deepcopy function, copying the receiver, creating a new BatchorderList.
func (in *BatchorderList) DeepCopy() *BatchorderList {
	if in == nil {
		return nil
	}
	out := new(BatchorderList)
	in.DeepCopyInto(out)
	return out
}

// DeepCopyObject is an autogenerated deepcopy function, copying the receiver, creating a new runtime.Object.
func (in *BatchorderList) DeepCopyObject() runtime.Object {
	if c := in.DeepCopy(); c != nil {
		return c
	}
	return nil
}

// DeepCopyInto is an autogenerated deepcopy function, copying the receiver, writing into out. in must be non-nil.
func (in *BatchorderSpec) DeepCopyInto(out *BatchorderSpec) {
	*out = *in
	if in.ProductList != nil {
		in, out := &in.ProductList, &out.ProductList
		*out = make([]Product, len(*in))
		for i := range *in {
			(*in)[i].DeepCopyInto(&(*out)[i])
		}
	}
	in.SubscriptionPlan.DeepCopyInto(&out.SubscriptionPlan)
	in.Security.DeepCopyInto(&out.Security)
	if in.CreateTime != nil {
		in, out := &in.CreateTime, &out.CreateTime
		*out = (*in).DeepCopy()
	}
	if in.UpdateTime != nil {
		in, out := &in.UpdateTime, &out.UpdateTime
		*out = (*in).DeepCopy()
	}
}

// DeepCopy is an autogenerated deepcopy function, copying the receiver, creating a new BatchorderSpec.
func (in *BatchorderSpec) DeepCopy() *BatchorderSpec {
	if in == nil {
		return nil
	}
	out := new(BatchorderSpec)
	in.DeepCopyInto(out)
	return out
}

// DeepCopyInto is an autogenerated deepcopy function, copying the receiver, writing into out. in must be non-nil.
func (in *BatchorderStatus) DeepCopyInto(out *BatchorderStatus) {
	*out = *in
}

// DeepCopy is an autogenerated deepcopy function, copying the receiver, creating a new BatchorderStatus.
func (in *BatchorderStatus) DeepCopy() *BatchorderStatus {
	if in == nil {
		return nil
	}
	out := new(BatchorderStatus)
	in.DeepCopyInto(out)
	return out
}

// DeepCopyInto is an autogenerated deepcopy function, copying the receiver, writing into out. in must be non-nil.
func (in *Lease) DeepCopyInto(out *Lease) {
	*out = *in
	out.TypeMeta = in.TypeMeta
	in.ObjectMeta.DeepCopyInto(&out.ObjectMeta)
	in.Spec.DeepCopyInto(&out.Spec)
	out.Status = in.Status
}

// DeepCopy is an autogenerated deepcopy function, copying the receiver, creating a new Lease.
func (in *Lease) DeepCopy() *Lease {
	if in == nil {
		return nil
	}
	out := new(Lease)
	in.DeepCopyInto(out)
	return out
}

// DeepCopyObject is an autogenerated deepcopy function, copying the receiver, creating a new runtime.Object.
func (in *Lease) DeepCopyObject() runtime.Object {
	if c := in.DeepCopy(); c != nil {
		return c
	}
	return nil
}

// DeepCopyInto is an autogenerated deepcopy function, copying the receiver, writing into out. in must be non-nil.
func (in *LeaseList) DeepCopyInto(out *LeaseList) {
	*out = *in
	out.TypeMeta = in.TypeMeta
	in.ListMeta.DeepCopyInto(&out.ListMeta)
	if in.Items != nil {
		in, out := &in.Items, &out.Items
		*out = make([]Lease, len(*in))
		for i := range *in {
			(*in)[i].DeepCopyInto(&(*out)[i])
		}
	}
}

// DeepCopy is an autogenerated deepcopy function, copying the receiver, creating a new LeaseList.
func (in *LeaseList) DeepCopy() *LeaseList {
	if in == nil {
		return nil
	}
	out := new(LeaseList)
	in.DeepCopyInto(out)
	return out
}

// DeepCopyObject is an autogenerated deepcopy function, copying the receiver, creating a new runtime.Object.
func (in *LeaseList) DeepCopyObject() runtime.Object {
	if c := in.DeepCopy(); c != nil {
		return c
	}
	return nil
}

// DeepCopyInto is an autogenerated deepcopy function, copying the receiver, writing into out. in must be non-nil.
func (in *LeaseSpec) DeepCopyInto(out *LeaseSpec) {
	*out = *in
	if in.AcquireTime != nil {
		in, out := &in.AcquireTime, &out.AcquireTime
		*out = (*in).DeepCopy()
	}
}

// DeepCopy is an autogenerated deepcopy function, copying the receiver, creating a new LeaseSpec.
func (in *LeaseSpec) DeepCopy() *LeaseSpec {
	if in == nil {
		return nil
	}
	out := new(LeaseSpec)
	in.DeepCopyInto(out)
	return out
}

// DeepCopyInto is an autogenerated deepcopy function, copying the receiver, writing into out. in must be non-nil.
func (in *LeaseStatus) DeepCopyInto(out *LeaseStatus) {
	*out = *in
}

// DeepCopy is an autogenerated deepcopy function, copying the receiver, creating a new LeaseStatus.
func (in *LeaseStatus) DeepCopy() *LeaseStatus {
	if in == nil {
		return nil
	}
	out := new(LeaseStatus)
	in.DeepCopyInto(out)
	return out
}

// DeepCopyInto is an autogenerated deepcopy function, copying the receiver, writing into out. in must be non-nil.
func (in *Order) DeepCopyInto(out *Order) {
	*out = *in
	out.TypeMeta = in.TypeMeta
	in.ObjectMeta.DeepCopyInto(&out.ObjectMeta)
	in.Spec.DeepCopyInto(&out.Spec)
	out.Status = in.Status
}

// DeepCopy is an autogenerated deepcopy function, copying the receiver, creating a new Order.
func (in *Order) DeepCopy() *Order {
	if in == nil {
		return nil
	}
	out := new(Order)
	in.DeepCopyInto(out)
	return out
}

// DeepCopyObject is an autogenerated deepcopy function, copying the receiver, creating a new runtime.Object.
func (in *Order) DeepCopyObject() runtime.Object {
	if c := in.DeepCopy(); c != nil {
		return c
	}
	return nil
}

// DeepCopyInto is an autogenerated deepcopy function, copying the receiver, writing into out. in must be non-nil.
func (in *OrderList) DeepCopyInto(out *OrderList) {
	*out = *in
	out.TypeMeta = in.TypeMeta
	in.ListMeta.DeepCopyInto(&out.ListMeta)
	if in.Items != nil {
		in, out := &in.Items, &out.Items
		*out = make([]Order, len(*in))
		for i := range *in {
			(*in)[i].DeepCopyInto(&(*out)[i])
		}
	}
}

// DeepCopy is an autogenerated deepcopy function, copying the receiver, creating a new OrderList.
func (in *OrderList) DeepCopy() *OrderList {
	if in == nil {
		return nil
	}
	out := new(OrderList)
	in.DeepCopyInto(out)
	return out
}

// DeepCopyObject is an autogenerated deepcopy function, copying the receiver, creating a new runtime.Object.
func (in *OrderList) DeepCopyObject() runtime.Object {
	if c := in.DeepCopy(); c != nil {
		return c
	}
	return nil
}

// DeepCopyInto is an autogenerated deepcopy function, copying the receiver, writing into out. in must be non-nil.
func (in *OrderSpec) DeepCopyInto(out *OrderSpec) {
	*out = *in
	in.Security.DeepCopyInto(&out.Security)
	in.SubscriptionPlan.DeepCopyInto(&out.SubscriptionPlan)
	if in.CreateTime != nil {
		in, out := &in.CreateTime, &out.CreateTime
		*out = (*in).DeepCopy()
	}
	if in.UpdateTime != nil {
		in, out := &in.UpdateTime, &out.UpdateTime
		*out = (*in).DeepCopy()
	}
}

// DeepCopy is an autogenerated deepcopy function, copying the receiver, creating a new OrderSpec.
func (in *OrderSpec) DeepCopy() *OrderSpec {
	if in == nil {
		return nil
	}
	out := new(OrderSpec)
	in.DeepCopyInto(out)
	return out
}

// DeepCopyInto is an autogenerated deepcopy function, copying the receiver, writing into out. in must be non-nil.
func (in *OrderStatus) DeepCopyInto(out *OrderStatus) {
	*out = *in
}

// DeepCopy is an autogenerated deepcopy function, copying the receiver, creating a new OrderStatus.
func (in *OrderStatus) DeepCopy() *OrderStatus {
	if in == nil {
		return nil
	}
	out := new(OrderStatus)
	in.DeepCopyInto(out)
	return out
}

// DeepCopyInto is an autogenerated deepcopy function, copying the receiver, writing into out. in must be non-nil.
func (in *Product) DeepCopyInto(out *Product) {
	*out = *in
	if in.ApprovalTime != nil {
		in, out := &in.ApprovalTime, &out.ApprovalTime
		*out = (*in).DeepCopy()
	}
}

// DeepCopy is an autogenerated deepcopy function, copying the receiver, creating a new Product.
func (in *Product) DeepCopy() *Product {
	if in == nil {
		return nil
	}
	out := new(Product)
	in.DeepCopyInto(out)
	return out
}

// DeepCopyInto is an autogenerated deepcopy function, copying the receiver, writing into out. in must be non-nil.
func (in *ProductDeployCondition) DeepCopyInto(out *ProductDeployCondition) {
	*out = *in
	in.LastProbeTime.DeepCopyInto(&out.LastProbeTime)
}

// DeepCopy is an autogenerated deepcopy function, copying the receiver, creating a new ProductDeployCondition.
func (in *ProductDeployCondition) DeepCopy() *ProductDeployCondition {
	if in == nil {
		return nil
	}
	out := new(ProductDeployCondition)
	in.DeepCopyInto(out)
	return out
}

// DeepCopyInto is an autogenerated deepcopy function, copying the receiver, writing into out. in must be non-nil.
func (in *Security) DeepCopyInto(out *Security) {
	*out = *in
	in.Config.DeepCopyInto(&out.Config)
}

// DeepCopy is an autogenerated deepcopy function, copying the receiver, creating a new Security.
func (in *Security) DeepCopy() *Security {
	if in == nil {
		return nil
	}
	out := new(Security)
	in.DeepCopyInto(out)
	return out
}

// DeepCopyInto is an autogenerated deepcopy function, copying the receiver, writing into out. in must be non-nil.
func (in *SubscriptionPlan) DeepCopyInto(out *SubscriptionPlan) {
	*out = *in
	in.Config.DeepCopyInto(&out.Config)
	if in.Strategies != nil {
		in, out := &in.Strategies, &out.Strategies
		*out = make([]*ApiStrategy, len(*in))
		for i := range *in {
			if (*in)[i] != nil {
				in, out := &(*in)[i], &(*out)[i]
				*out = new(ApiStrategy)
				(*in).DeepCopyInto(*out)
			}
		}
	}
	in.CreateTime.DeepCopyInto(&out.CreateTime)
}

// DeepCopy is an autogenerated deepcopy function, copying the receiver, creating a new SubscriptionPlan.
func (in *SubscriptionPlan) DeepCopy() *SubscriptionPlan {
	if in == nil {
		return nil
	}
	out := new(SubscriptionPlan)
	in.DeepCopyInto(out)
	return out
}

// DeepCopyInto is an autogenerated deepcopy function, copying the receiver, writing into out. in must be non-nil.
func (in *SubscriptionPlanList) DeepCopyInto(out *SubscriptionPlanList) {
	*out = *in
	if in.Items != nil {
		in, out := &in.Items, &out.Items
		*out = make([]SubscriptionPlan, len(*in))
		for i := range *in {
			(*in)[i].DeepCopyInto(&(*out)[i])
		}
	}
}

// DeepCopy is an autogenerated deepcopy function, copying the receiver, creating a new SubscriptionPlanList.
func (in *SubscriptionPlanList) DeepCopy() *SubscriptionPlanList {
	if in == nil {
		return nil
	}
	out := new(SubscriptionPlanList)
	in.DeepCopyInto(out)
	return out
}

// DeepCopyInto is an autogenerated deepcopy function, copying the receiver, writing into out. in must be non-nil.
func (in SubscriptionPlans) DeepCopyInto(out *SubscriptionPlans) {
	{
		in := &in
		*out = make(SubscriptionPlans, len(*in))
		for i := range *in {
			(*in)[i].DeepCopyInto(&(*out)[i])
		}
	}
}

// DeepCopy is an autogenerated deepcopy function, copying the receiver, creating a new SubscriptionPlans.
func (in SubscriptionPlans) DeepCopy() SubscriptionPlans {
	if in == nil {
		return nil
	}
	out := new(SubscriptionPlans)
	in.DeepCopyInto(out)
	return *out
}
