/*

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package v1

import (
	"fmt"

	"github.com/spf13/cast"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime"
)

// EDIT THIS FILE!  THIS IS SCAFFOLDING FOR YOU TO OWN!
// NOTE: json tags are required.  Any new fields you add must have json tags for the fields to be serialized.

type PluginType string

const (
	RequestTermination     PluginType = "request-termination"
	ACL                    PluginType = "acl"
	CORS                   PluginType = "cors"
	RateLimiting           PluginType = "rate-limiting"
	OAuth2                 PluginType = "oauth2"
	IPRestriction          PluginType = "ip-restriction"
	JWT                    PluginType = "jwt"
	JWTHs256               PluginType = "jwt-HS256"
	JWTRs256               PluginType = "jwt-RS256"
	BASIC                  PluginType = "basic"
	RequestSizeLimiting    PluginType = "request-size-limiting"
	Prometheus             PluginType = "prometheus"
	IPRestrictionBlackList            = "blacklist"
	IPRestrictionWhiteList            = "whitelist"
)

const (
	// ProductStatusUnpublished unpublished
	ProductStatusUnpublished ProductStatus = 0
	// ProductStatusPublished 已发布
	ProductStatusPublished ProductStatus = 1
	// ProductStatusReleased 已上架
	ProductStatusReleased ProductStatus = 2
	// ProductStatusDeprecated 已下架
	ProductStatusDeprecated ProductStatus = 3
	// APIProductStatusRejected 未通过
	APIProductStatusRejected ProductStatus = 4
)

func IsSecureModePlugin(s *ApiStrategy) bool {
	if nil == s {
		return false
	}
	if s.Type == JWT || s.Type == OAuth2 || s.Type == BASIC {
		return true
	}
	return false
}

func IsAccessControlPlugin(s *ApiStrategy) bool {
	if nil == s {
		return false
	}
	if s.Type == ACL || s.Type == RequestTermination {
		return true
	}
	return false
}

type ProductStatus int

func (s ProductStatus) Offline() bool {
	return s == ProductStatusDeprecated
}

func (s ProductStatus) Available() bool {
	return s == ProductStatusReleased
}

type SubscriptionPlans []SubscriptionPlan
type subscriptionPlans map[string]SubscriptionPlan

func (s SubscriptionPlans) Type() SubscriptionType {
	if len(s) > 0 {
		return s[0].Type
	}
	return Empty
}

func (s SubscriptionPlans) Get(plan SubscriptionPlan) *SubscriptionPlan {
	key := func(p SubscriptionPlan) string {
		if PricingPackage == plan.Type {
			return fmt.Sprintf("%s-%s", p.Type, p.Name)
		}
		return string(p.Type)
	}
	planMap := subscriptionPlans{}
	for _, item := range s {
		planMap[key(item)] = item
	}
	res, ok := planMap[key(plan)]
	if !ok {
		return nil
	}
	return res.DeepCopy()
}

func (s SubscriptionPlans) HasType(planType SubscriptionType) bool {
	for _, plan := range s {
		if planType == plan.Type {
			return true
		}
	}
	return false
}

// ApiProductSpec defines the desired state of ApiProduct
type ApiProductSpec struct {
	// INSERT ADDITIONAL SPEC FIELDS - desired state of cluster
	// Important: Run "make" to regenerate code after modifying this file
	Id             string        `json:"id"`
	Name           string        `json:"name"`
	DisplayName    string        `json:"display_name"`
	Description    string        `json:"description,omitempty"`
	Icon           string        `json:"icon,omitempty"`
	SwaggerFileUrl string        `json:"swagger_file_url"`
	Introduce      string        `json:"introduce,omitempty"`
	Label          []string      `json:"label,omitempty"`
	ReleaseTime    string        `json:"release_time,omitempty"`
	Status         ProductStatus `json:"status"`

	ApiDefinition     ApiDefinition     `json:"apidefinition,omitempty"`
	SubscriptionPlans SubscriptionPlans `json:"subscription_plans,omitempty"`
	ApiDoc            string            `json:"api_doc,omitempty"`

	OwnerUserName  string   `json:"owner_user_name"`
	OwnerUserEmail string   `json:"owner_user_email,omitempty"`
	DocIds         []string `json:"doc_id_list,omitempty"`
	ProjectId      string   `json:"project_id,omitempty"`
	ProjectName    string   `json:"project_name"`
	FunctionId     string   `json:"function_id,omitempty"`

	ApprovalOpinion string `json:"approval_opinion,omitempty"`
	ApprovalTime    string `json:"approval_time,omitempty"`

	CreateTime     *metav1.Time `json:"create_time,omitempty"`
	CreateUserName string       `json:"create_user_name,omitempty"`
	UpdateTime     *metav1.Time `json:"update_time,omitempty"`
	UpdateUserName string       `json:"update_user_name,omitempty"`
}

type ApiDefinition struct {
	Id              string       `json:"id,omitempty"`
	HttpProtocol    []string     `json:"http_protocol,omitempty"`
	ApiPaths        []*ApiPath   `json:"paths,omitempty"`
	ApiSecurityMode *ApiStrategy `json:"security_mode,omitempty"`
	ApiService      ApiService   `json:"service,omitempty"`

	// for swagger test
	ApiServiceIDForTest string `json:"service_id_test,omitempty"`
	ApiPathIDForTest    string `json:"path_id_test,omitempty"`
	ApiPathNameForTest  string `json:"path_name_test,omitempty"`
}

type ApiService struct {
	Id         string         `json:"id,omitempty"`
	EndPoint   string         `json:"endpoint,omitempty"`
	Status     int            `json:"status,omitempty"`
	Strategies []*ApiStrategy `json:"strategies,omitempty"`
}

type ApiPath struct {
	Id            string         `json:"id,omitempty"`
	HttpMethod    []string       `json:"http_method,omitempty"`
	Path          string         `json:"path,omitempty"`
	EnalbeSubPath bool           `json:"enable_subpath,omitempty"`
	Strategies    []*ApiStrategy `json:"strategies,omitempty"`
}

type SubscriptionType string

const (
	MonlthlyPlan   SubscriptionType = "monthly_plan"
	LongTime       SubscriptionType = "longtime"
	PricingPackage SubscriptionType = "pricing_package"
	Empty          SubscriptionType = ""
	MonthNum                        = "monthly_plan"
	APICalls                        = "api_calls"
	Validity                        = "validity"
	DataPlan                        = "data_plan"
)

// GetMonthlyConfig gey monthly config, return int64 for json codec
func (s SubscriptionConfig) GetMonthNum() (int64, error) {
	num, ok := s[MonthNum]
	if !ok {
		return 0, fmt.Errorf("%q needed", MonthNum)
	}
	return cast.ToInt64E(num)
}

// GetMonthlyConfig gey monthly config, return int64 for json codec
func (s SubscriptionConfig) GetValidity() (int64, error) {
	validity, ok := s[Validity]
	if !ok {
		return 0, fmt.Errorf("%q needed", Validity)
	}
	return cast.ToInt64E(validity)
}

// GetMonthlyConfig gey monthly config, return int64 for json codec
func (s SubscriptionConfig) GetAPICalls() (int64, error) {
	apiCalls, ok := s[APICalls]
	if !ok {
		return 0, fmt.Errorf("%q needed", APICalls)
	}
	return cast.ToInt64E(apiCalls)
}

// GetMonthlyConfig gey monthly config, return int64 for json codec
func (s SubscriptionConfig) AddAPICalls(delta int64) SubscriptionConfig {
	apiCalls := cast.ToInt64(s[APICalls])
	s[APICalls] = apiCalls + delta
	return s
}

// GetDataPlan gey data plan, return int64 for json codec
func (s SubscriptionConfig) GetDataPlan() (string, error) {
	dataPlan, ok := s[DataPlan]
	if !ok {
		return "", fmt.Errorf("%q needed", DataPlan)
	}
	return cast.ToStringE(dataPlan)
}

// SetDataPlan set data plan
func (s SubscriptionConfig) SetDataPlan(dataPlan string) SubscriptionConfig {
	s[DataPlan] = dataPlan
	return s
}

type SubscriptionPlan struct {
	Id         string             `json:"id,omitempty"`
	Name       string             `json:"name,omitempty"`
	Type       SubscriptionType   `json:"type,omitempty"`
	Config     SubscriptionConfig `json:"config,omitempty"`
	Strategies []*ApiStrategy     `json:"strategies,omitempty"`
	CreateTime metav1.MicroTime   `json:"create_time,omitempty"`
}

type SubscriptionPlanList struct {
	Items []SubscriptionPlan `json:"items,omitempty"`
}

// TODO: the name is confusing, should replace with KVConfig

// +k8s:deepcopy-gen=false
type SubscriptionConfig map[string]interface{}

func (in *SubscriptionConfig) DeepCopy() *SubscriptionConfig {
	if in == nil {
		return nil
	}
	out := new(SubscriptionConfig)
	in.DeepCopyInto(out)
	return out
}

func (in *SubscriptionConfig) DeepCopyInto(out *SubscriptionConfig) {
	*out = runtime.DeepCopyJSON(*in)
	return
}

// +k8s:deepcopy-gen=false
type PluginConfig map[string]interface{}

func (in *PluginConfig) DeepCopy() *PluginConfig {
	if in == nil {
		return nil
	}
	out := new(PluginConfig)
	in.DeepCopyInto(out)
	return out
}

func (in *PluginConfig) DeepCopyInto(out *PluginConfig) {
	*out = runtime.DeepCopyJSON(*in)
	return
}

type ApiStrategy struct {
	Id     string       `json:"id,omitempty"`
	Type   PluginType   `json:"type,omitempty"`
	Config PluginConfig `json:"config,omitempty"`
}

type ProductDeployPhase string

const (
	// ProductDeployed means all of the object product has been deployed to Kong
	Deployed ProductDeployPhase = "deployed"
	// ProductError means some server error happened, and the task will be retried
	DeployError ProductDeployPhase = "error" // will retry
	// ProductError means some client error happened, and the task will not be retried
	DeployFailed ProductDeployPhase = "failed"
)

type ProductDeployConditionType string

const (
	ServiceDeployed       ProductDeployConditionType = "ServiceSeployed"
	RoutesDeployed        ProductDeployConditionType = "RoutesDeployed"
	ACLDeployed           ProductDeployConditionType = "ACLDeployed"
	RateLimitingDeployed  ProductDeployConditionType = "RateLimitingDeployed"
	IPRestrictionDeployed ProductDeployConditionType = "IPRestrictionDeployed"
	OAuth2Deployed        ProductDeployConditionType = "OAuth2Deployed"
	JWTDeployed           ProductDeployConditionType = "JWTDeployed"
)

type ConditionStatus string

// These are valid condition statuses. "ConditionTrue" means a resource is in the condition.
// "ConditionFalse" means a resource is not in the condition.

const (
	ConditionTrue  ConditionStatus = "True"
	ConditionFalse ConditionStatus = "False"
)

type ProductDeployCondition struct {
	Type          ProductDeployConditionType `json:"type"`
	Status        ConditionStatus            `json:"status"`
	LastProbeTime metav1.Time                `json:"lastProbeTime,omitempty"`
}

// ApiProductStatus defines the observed state of ApiProduct
type ApiProductStatus struct {
	// INSERT ADDITIONAL STATUS FIELD - define observed state of cluster
	// Important: Run "make" to regenerate code after modifying this file

	// A status represent
	DeployStatus ProductDeployPhase `json:"deploy_status"`

	// The deploy condition for kong object defined in product
	DeployConditions []ProductDeployCondition `json:"deploy_conditions"`

	// A human readable message details indicating the prodcuct deploy status.
	// +optional
	Message string `json:"message"`

	// SubscriptionTimes represents for subscription times
	// +optional
	SubscriptionTimes int64 `json:"subscription_times"`

	// Available represents the status for product if it's ok to access
	// +optional
	Available bool `json:"available"`
}

// +kubebuilder:object:root=true

// ApiProduct is the Schema for the apiproducts API
type ApiProduct struct {
	metav1.TypeMeta   `json:",inline"`
	metav1.ObjectMeta `json:"metadata,omitempty"`

	Spec   ApiProductSpec   `json:"spec,omitempty"`
	Status ApiProductStatus `json:"status,omitempty"`
}

// +kubebuilder:object:root=true

// ApiProductList contains a list of ApiProduct
type ApiProductList struct {
	metav1.TypeMeta `json:",inline"`
	metav1.ListMeta `json:"metadata,omitempty"`
	Items           []ApiProduct `json:"items"`
}

func init() {
	SchemeBuilder.Register(&ApiProduct{}, &ApiProductList{})
}
