/*

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package v1

import (
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

// EDIT THIS FILE!  THIS IS SCAFFOLDING FOR YOU TO OWN!
// NOTE: json tags are required.  Any new fields you add must have json tags for the fields to be serialized.

const (
	UnPaid   PaymentStatus = "unpaid"
	Paid     PaymentStatus = "paid"
	Canceled PaymentStatus = "canceled"
)

type PaymentStatus string

type Security struct {
	ID     string       `json:"id,omitempty"`
	Type   string       `json:"type,omitempty"`
	Config PluginConfig `json:"config,omitempty"`
}

type OrderApprovalStatus int

const (
	// 待审核
	OrderApprovalStatusUnapproved OrderApprovalStatus = 0
	// 审核通过
	OrderApprovalStatusApproved OrderApprovalStatus = 1
	// 审核拒绝
	OrderApprovalStatusRejected OrderApprovalStatus = 2
	// 已废弃
	OrderApprovalStatusDeprecated OrderApprovalStatus = 3
)

// OrderSpec defines the desired state of Order
type OrderSpec struct {
	// INSERT ADDITIONAL SPEC FIELDS - desired state of cluster
	// Important: Run "make" to regenerate code after modifying this file
	ID                            string              `json:"id"`
	OrderName                     string              `json:"order_name"`
	ProductID                     string              `json:"product_id"`
	ProductName                   string              `json:"product_name"`
	ProductProjectName            string              `json:"product_project_name"`
	ProductDeveloperName          string              `json:"product_developer_name"`
	OwnerUserID                   string              `json:"order_user_id"`
	OwnerUserName                 string              `json:"owner_user_name"`
	Type                          string              `json:"type"`
	ConsumeApplicationName        string              `json:"consume_application_name"`
	ConsumeApplicationID          string              `json:"consume_application_id"`
	ConsumeApplicationProjectName string              `json:"consume_application_project_name"`
	Status                        PaymentStatus       `json:"status"`
	ApprovalStatus                OrderApprovalStatus `json:"approval_status"`
	ApprovalOpinion               string              `json:"approval_opinion"`
	Security                      Security            `json:"security,omitempty"`
	SubscriptionPlan              SubscriptionPlan    `json:"subscription_plan,omitempty"`
	CreateTime                    *metav1.Time        `json:"create_time"`
	CreateUserName                string              `json:"create_user_name"`
	UpdateTime                    *metav1.Time        `json:"update_time"`
	UpdateUserName                string              `json:"update_user_name"`
	ApiProductServiceName         string              `json:"apiproductservice_name"`
}

type SubscriptionPhase string

const (
	SubscriptionPending    SubscriptionPhase = "pending"
	SubscriptionProcessing SubscriptionPhase = "processing"
	SubscriptionFinished   SubscriptionPhase = "finished"
	SubscriptionCanceled   SubscriptionPhase = "finished"
)

// OrderStatus defines the observed state of Order
type OrderStatus struct {
	// INSERT ADDITIONAL STATUS FIELD - define observed state of cluster
	// Important: Run "make" to regenerate code after modifying this file
	Message           string            `json:"message,omitempty"`
	SubscriptionPhase SubscriptionPhase `json:"subscription_phase,inline"`
}

// +kubebuilder:object:root=true

// Order is the Schema for the orders API
type Order struct {
	metav1.TypeMeta   `json:",inline"`
	metav1.ObjectMeta `json:"metadata,omitempty"`

	Spec   OrderSpec   `json:"spec,omitempty"`
	Status OrderStatus `json:"status,omitempty"`
}

// +kubebuilder:object:root=true

// OrderList contains a list of Order
type OrderList struct {
	metav1.TypeMeta `json:",inline"`
	metav1.ListMeta `json:"metadata,omitempty"`
	Items           []Order `json:"items"`
}

func init() {
	SchemeBuilder.Register(&Order{}, &OrderList{})
}
