package common

import (
	"fmt"
	"time"

	"github.com/sony/sonyflake"
	"github.com/spf13/cast"
)

// TraceID for the logs
type TraceID string

// var GatewayClient rest.Client

const (
	RequestID                 string = "RequestID"
	ReconcileDefaultDelayTime        = 1 * time.Minute
)

func GetSnowFlakeID() string {
	var st sonyflake.Settings
	sf := sonyflake.NewSonyflake(st)
	id, _ := sf.NextID()
	return cast.ToString(id)
}

// ReconcileFlag describes error handling reconcile strategy for http errors from k8s or kong
type ReconcileFlag int64

type ReconcileOption struct {
	Flag       ReconcileFlag
	RetryAfter time.Duration
	//SessionID  string
}

func (f ReconcileFlag) Has(flag ReconcileFlag) bool {
	return f&flag != 0
}

func (f ReconcileFlag) String() string {
	return fmt.Sprintf("0x%b", f)
}

const (
	// NoFlag will not trigger anything, works with legacy code
	NoFlag ReconcileFlag = iota
	// Not handling the event any longer
	NotReconcileHttpErrorAnyMore
	NotReconcileHttpConflictAnyMore
	// Requeue the event
	ReconcileHttpErrorNoDelay
	ReconcileFailDirectly
)

var (
	ReconcileDefaultDelay   = &ReconcileOption{Flag: NoFlag, RetryAfter: ReconcileDefaultDelayTime}
	NoReconcile             = &ReconcileOption{Flag: ReconcileFailDirectly, RetryAfter: 0}
	HttpConflictNoReconcile = &ReconcileOption{Flag: NotReconcileHttpConflictAnyMore, RetryAfter: ReconcileDefaultDelayTime}
	ReconcileDebugDelay     = &ReconcileOption{Flag: NoFlag, RetryAfter: 1 * time.Nanosecond}
)
