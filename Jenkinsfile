// https://jenkins.io/doc/book/pipeline/syntax/
@Library('alauda-cicd') _

// image can be used for promoting...
def IMAGE
def DEBUG = false
def deployment
def RELEASE_VERSION
def RELEASE_BUILD
def release
def PROXY_CREDENTIALS
def FOLDER = "src/alauda.io/amp-controller"
pipeline {
	agent { label 'golang-1.11' }

	options {
		buildDiscarder(logRotator(numToKeepStr: '10'))
		disableConcurrentBuilds()
		// skipDefaultCheckout()
	}

	environment {
		FOLDER = 'src/alauda.io/amp-controller'
		GOPATH = "${WORKSPACE}"

		// for building an scanning
		REPOSITORY = "amp-controller"
		OWNER = "mathildetech"
		// sonar feedback user
		// needs to change together with the credentialsID
		BITBUCKET_FEEDBACK_ACCOUNT = "alaudabot"
		SONARQUBE_BITBUCKET_CREDENTIALS = "alaudabot"
		NAMESPACE = "apiproject-apins"
		DEPLOYMENT_NAME = "amp-controller"
		CONTAINER = "manager"
		DINGDING_BOT = "devops-chat-bot"
		TAG_CREDENTIALS = "alaudabot-bitbucket"
//         PROXY_CREDENTIALS_ID = 'proxy'

// 		IMAGE_REPOSITORY = "index.alauda.cn/alaudak8s/amp-controller"
		IMAGE_REPOSITORY = "alaudak8s/amp-controller"
// 		IMAGE_TEST_REPOSITORY = "index.alauda.cn/alaudak8s/devops-apiserver-tests"
// 		IMAGE_CREDENTIALS = "alaudak8s"

		CHARTS_PIPELINE = "/common/common-charts-pipeline"
		CHART_NAME = "amp"
		CHART_COMPONENT = "ampController"
	}

	stages {
		stage('Checkout') {
			steps {
				script {
					dir(FOLDER) {
						container('tools') {
							// checkout code
 							// withCredentials([
 							// 	usernamePassword(credentialsId: PROXY_CREDENTIALS_ID, passwordVariable: 'PROXY_ADDRESS', usernameVariable: 'PROXY_ADDRESS_PASS')
 							// ]) { PROXY_CREDENTIALS = "${PROXY_ADDRESS}" }
 							// sh "git config --global http.proxy ${PROXY_CREDENTIALS}"
							def scmVars
							retry(2) { scmVars = checkout scm }
							release = deploy.release(scmVars)

							RELEASE_BUILD = release.version
							RELEASE_VERSION = release.majorVersion
							// echo "release ${RELEASE_VERSION} - release build ${RELEASE_BUILD}"
							echo """
								release ${RELEASE_VERSION}
								version ${release.version}
								is_release ${release.is_release}
								is_build ${release.is_build}
								is_master ${release.is_master}
								deploy_env ${release.environment}
								auto_test ${release.auto_test}
								environment ${release.environment}
								majorVersion ${release.majorVersion}
							"""
							// copying kubectl from tools
							sh "cp /usr/local/bin/kubectl ."
						}
					}
				}
			}
		}
		stage('CI'){
			failFast true
			parallel {
				stage('Build') {
					steps {
						script {
							dir(FOLDER) {
								// container('golang') { sh "CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -a -o ./bin/amp-controller" }
								container('tools') {
									// sh "upx ./bin/amp-controller || true"
									// sh "cp Dockerfile ./bin/"
									// currently is building code inside the container
 									// IMAGE = deploy.dockerBuild(
 										// "Dockerfile", //Dockerfile
 										// ".", // build context
 										// IMAGE_REPOSITORY, // repo address
 										// RELEASE_BUILD, // tag
 										// IMAGE_CREDENTIALS, // credentials for pushing
 										// ).setArg("commit_id", "${release.commit}").setArg("app_version", "${RELEASE_BUILD}")
    								IMAGE = deploy.dockerBuildWithRegister(
    									dockerfile: "Dockerfile",
    									address: IMAGE_REPOSITORY,
    									tag: RELEASE_BUILD
                                        ).setArg("commit_id", "${release.commit}").setArg("version", "${RELEASE_BUILD}")

									// start and push
									IMAGE.start().push()
								}
							}
						}

					}
				}
				stage('Code Scan') {
					steps {
						script {
							dir(FOLDER) {
								// todo
							}
						}
					}
				}
			}
		}
		// after build it should start deploying
		// stage('Deploying') {
		// 	when {
		// 		expression {
		// 			release.auto_test && !params.DEBUG
		// 		}
		// 	}
		// 	steps {
		// 		script {
		// 			dir(FOLDER) {
		// 				container('tools') {
		// 					release.setupTestEnv()

		// 					deployment = deploy.getDeploymentObj(NAMESPACE, DEPLOYMENT_NAME)
		// 					deployment.saveState()
		// 					deployment.setImage(IMAGE.getImage(), "amp-controller-manager")
		// 					deployment.apply()
		// 				}
		// 			}
		// 		}
		// 	}
		// }
		// stage('Promoting') {
		// 	// limit this stage to master only
		// 	when {
		// 		expression {
		// 			release.auto_test && release.deploy_env != '' && !params.DEBUG
		// 		}
		// 	}
		// 	steps {
		// 		script {
		// 			dir(FOLDER) {
		// 				container('tools') {
		// 					release.setupDeployEnv()

		// 					deployment = deploy.getDeploymentObj(NAMESPACE, DEPLOYMENT_NAME)
		// 					deployment.saveState()
		// 					deployment.setImage(IMAGE.getImage(), "amp-controller-manager")
		// 					deployment.apply()
		// 				}
		// 			}
		// 		}
		// 	}
		// }
		stage('Tag git') {
			when {
				expression {
					release.tag && !params.DEBUG
				}
			}
			steps {
				script {
					dir(FOLDER) {
						container('tools') {
							deploy.gitTag(
								TAG_CREDENTIALS,
								RELEASE_BUILD,
								OWNER,
								REPOSITORY
								)
						}
					}
				}
			}
		}


		 stage('Chart Update') {
            when {
                expression { release.shouldUpdateChart() }
            }
            steps {
                script {
                    echo "will trigger charts-pipeline using branch ${release.chartBranch}"

					deploy.triggerChart([
						chart: CHART_NAME,
						component: CHART_COMPONENT,
						version: RELEASE_VERSION,
						imageTag: RELEASE_BUILD,
						branch: release.chartBranch,
						prBranch: release.change["branch"],
						env: release.environment
					]).start()
                }
            }
        }

	}

	post {
		success {
			dir(FOLDER) {
				script {
					container('tools') {
						def msg = "流水线完成了"
						if (release.is_master) {
							msg = "上线啦！"
						}
						deploy.notificationSuccess(DEPLOYMENT_NAME, DINGDING_BOT, msg, RELEASE_BUILD)
						if (release != null) { release.cleanEnv() }
					}
				}
			}
		}
		failure {
			dir(FOLDER) {
				script {
					container('tools') {
						if (release != null) {
							release.setupTestEnv()
							if (deployment != null) {
								deployment.rollbackConfig().apply()
							}
						}
						deploy.notificationFailed(DEPLOYMENT_NAME, DINGDING_BOT, "流水线失败了", RELEASE_BUILD)
						if (release != null) { release.cleanEnv() }
					}
				}
			}
		}
	}
}

