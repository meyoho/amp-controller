package gateway

import (
	b64 "encoding/base64"

	"github.com/dgrijalva/jwt-go"
	"github.com/go-logr/logr"
	"github.com/sethvargo/go-password/password"
	apiError "k8s.io/apimachinery/pkg/api/errors"

	ampv1 "alauda.io/amp-controller/api/v1"

	"context"
	"fmt"

	"alauda.io/amp-controller/gateway/kong"
)

func GetCurrentACLBindingNew(consumerName string, product *ampv1.ApiProduct, logger logr.Logger) (*ACL, error) {
	var err error
	var client kong.Client
	if client, err = kong.NewGateWayClient(); nil != err {
		return nil, err
	}
	acl := &ACL{
		ID:       string(product.GetUID()),
		Consumer: Consumer{Username: consumerName},
	}
	if err := client.Get(context.TODO(), acl); nil != err {
		return nil, err
	}
	logger.Info("GetCurrentACLBinding, get acl", "acl", acl)
	return acl, nil
}

func SyncGatewayACLBindingNew(consumerName string, product *ampv1.ApiProduct, logger logr.Logger) error {
	var client kong.Client
	var err error

	apiService := product.Spec.ApiDefinition.ApiService

	if client, err = kong.NewGateWayClient(); nil != err {
		logger.Info(fmt.Sprintf("generate new kong client error err=%s", err.Error()))
		return err
	}

	consumer := Consumer{Username: consumerName}

	// Add consumer to acl
	acl := &ACL{
		ID:       string(product.GetUID()),
		Group:    fmt.Sprintf("%s-%s", WhiteListPrefix, apiService.Id),
		Consumer: consumer,
	}
	if err := client.Create(context.TODO(), acl); nil != err && !apiError.IsAlreadyExists(err) {
		logger.Info(fmt.Sprintf("create kong acl error err=%s", err.Error()))
		return err
	}

	return nil
}

func SyncDeleteGatewayACLNew(consumerName string, product *ampv1.ApiProduct, logger logr.Logger) error {
	var err error
	var client kong.Client

	apiService := product.Spec.ApiDefinition.ApiService

	if client, err = kong.NewGateWayClient(); nil != err {
		return err
	}
	// delete consumer from acl
	acl := &ACL{
		ID:       string(product.GetUID()),
		Group:    fmt.Sprintf("%s-%s", WhiteListPrefix, apiService.Id),
		Consumer: Consumer{Username: consumerName},
	}
	logger.Info("SyncDeleteGatewayACL, delete acl", "acl", acl)
	if err := client.Delete(context.TODO(), acl); nil != err && !apiError.IsNotFound(err) {
		logger.Info(fmt.Sprintf("delete kong acl error err=%s", err.Error()))
		return err
	}
	return nil
}

// SyncGatewayBindingDelete ...
func SyncGatewayBindingDeleteNew(consumer *ampv1.ApiConsumeApplication, authType ampv1.PluginType) error {
	consumerName := consumer.GetName()
	consumeApplicationUID := string(consumer.GetUID())

	var err error
	switch authType {
	case ampv1.OAuth2:
		err = SyncOAuth2BindingDeleteNew(consumerName, consumeApplicationUID)
	case ampv1.JWT:
		err = SyncJWTBindingDeleteNew(consumerName, consumeApplicationUID)
	case ampv1.BASIC:
		err = SyncBasicBindingDeleteNew(consumerName, consumeApplicationUID)
	}
	if nil != err {
		return err
	}
	return nil
}

func SyncGatewayBindingNew(consumer *ampv1.ApiConsumeApplication, authType ampv1.PluginType) (ampv1.APInfo, error) {

	var info ampv1.APInfo
	var err error

	consumerName := consumer.GetName()
	consumeApplicationUID := string(consumer.GetUID())

	switch authType {
	case ampv1.OAuth2:
		info, err = SyncOAuth2BindingNew(consumerName, consumeApplicationUID)
	case ampv1.JWTHs256:
		info, err = SyncJWTBindingNew(consumerName, consumeApplicationUID, AlgorithmHS256)
	case ampv1.JWTRs256:
		info, err = SyncJWTBindingNew(consumerName, consumeApplicationUID, AlgorithmRS256)
	case ampv1.BASIC:
		info, err = SyncBasicBindingNew(consumerName, consumeApplicationUID)
	}
	if nil != err {
		return nil, err
	}

	return info, nil
}

// SyncOAuth2BindingNew ...
func SyncOAuth2BindingNew(consumerName string, consumeApplicationUID string) (ampv1.APInfo, error) {
	var client kong.Client
	var err error

	if client, err = kong.NewGateWayClient(); nil != err {
		return nil, err
	}
	consumer := Consumer{Username: consumerName}

	// in case it's a update
	credentials := &OAuth2Credential{
		ID:           consumeApplicationUID,
		Name:         consumerName,
		RedirectUris: []string{"http://www.alauda.cn"},
		Consumer:     consumer,
	}

	err = client.Create(context.TODO(), credentials)
	if nil != err && !apiError.IsAlreadyExists(err) && !apiError.IsConflict(err) {
		return nil, err
	}

	info := ampv1.APInfo{
		ampv1.APISecurityType:    string(ampv1.OAuth2),
		ampv1.OAuth2ClientId:     credentials.ClientID,
		ampv1.OAuth2ClientSecret: credentials.ClientSecret,
	}
	return info, nil
}

// SyncJWTBinding ...
func SyncJWTBindingNew(consumerName string, consumeApplicationUID string, algorithm Algorithm) (ampv1.APInfo, error) {
	var client kong.Client
	var err error

	if client, err = kong.NewGateWayClient(); nil != err {
		return nil, err
	}

	// create consumer jwt
	consumer := Consumer{Username: consumerName}

	privateKey, err := NewPrivateKey()
	if nil != err {
		return nil, err
	}
	RSAPublicKey, err := EncodePublicKeyPEM(privateKey.Public())
	if err != nil {
		return nil, err
	}

	credential := &JWTCredential{
		ID:           consumeApplicationUID,
		Algorithm:    algorithm,
		RSAPublicKey: string(RSAPublicKey),
		Consumer:     consumer,
	}

	if AlgorithmHS256 != credential.Algorithm && AlgorithmRS256 != credential.Algorithm {
		credential.Algorithm = AlgorithmHS256
		credential.RSAPublicKey = ""
	}

	err = client.Create(context.TODO(), credential)
	if nil != err && !apiError.IsAlreadyExists(err) && !apiError.IsConflict(err) {
		return nil, err
	}

	info := ampv1.APInfo{
		ampv1.APIPluginID:     credential.ID,
		ampv1.APISecurityType: string(ampv1.JWT),
		ampv1.JWTKey:          credential.Key,
		ampv1.JWTSecret:       credential.Secret,
		ampv1.JWTAlgorithm:    string(credential.Algorithm),
	}

	if AlgorithmRS256 == credential.Algorithm || AlgorithmES256 == credential.Algorithm {
		info[ampv1.JWTRSAPublicKey] = string(RSAPublicKey)
		tokenString, err := NewBearerToken(privateKey, credential.Key)
		if nil != err {
			return nil, err
		}
		info[ampv1.Beareroken] = tokenString
	}

	if AlgorithmHS256 == credential.Algorithm {
		token := jwt.NewWithClaims(jwt.SigningMethodHS256, jwt.MapClaims{
			"iss": credential.Key,
		})
		tokenString, err := token.SignedString([]byte(credential.Secret))
		if nil != err {
			return nil, err
		}
		info[ampv1.Beareroken] = tokenString
	}
	return info, nil
}

// SyncBasicBinding ...
func SyncBasicBindingNew(consumerName string, consumeApplicationUID string) (ampv1.APInfo, error) {
	var client kong.Client
	var err error

	if client, err = kong.NewGateWayClient(); nil != err {
		return nil, err
	}

	// create consumer
	consumer := Consumer{
		Username: consumerName,
	}

	var username, pwd string
	username = randomUsername()
	pwd, _ = password.Generate(32, 10, 10, true, false)

	credential := &BasicCredential{
		ID:       consumeApplicationUID,
		Username: username,
		Password: pwd,
		Consumer: consumer,
	}

	err = client.Create(context.TODO(), credential)
	if nil != err && !apiError.IsAlreadyExists(err) && !apiError.IsConflict(err) {
		return nil, err
	}

	info := ampv1.APInfo{
		ampv1.APIPluginID:     credential.ID,
		ampv1.APISecurityType: string(ampv1.BASIC),
		ampv1.BasicUserName:   credential.Username,
		ampv1.BasicPassword:   pwd,
	}
	data := credential.Username + ":" + pwd
	info[ampv1.BasicToken] = b64.StdEncoding.EncodeToString([]byte(data))
	return info, nil
}

// SyncJWTBindingDelete ...
func SyncJWTBindingDeleteNew(consumerName string, consumeApplicationUID string) error {
	var client kong.Client
	var err error

	if client, err = kong.NewGateWayClient(); nil != err {
		return err
	}
	credential := &JWTCredential{
		ID: consumeApplicationUID,
		Consumer: Consumer{
			Username: consumerName,
		},
	}

	if err := client.Delete(context.TODO(), credential); nil != err && !apiError.IsNotFound(err) {
		return err
	}
	return nil
}

// SyncOAuth2BindingDelete ...
func SyncOAuth2BindingDeleteNew(consumerName string, consumeApplicationUID string) error {
	var client kong.Client
	var err error

	if client, err = kong.NewGateWayClient(); nil != err {
		return err
	}
	credentials := &OAuth2Credential{
		ID: consumeApplicationUID,
		Consumer: Consumer{
			Username: consumerName,
		},
	}
	if err := client.Delete(context.TODO(), credentials); nil != err && !apiError.IsNotFound(err) {
		return err
	}
	return nil
}

// SyncBasicBindingDelete ...
func SyncBasicBindingDeleteNew(consumerName string, consumeApplicationUID string) error {
	var client kong.Client
	var err error

	if client, err = kong.NewGateWayClient(); nil != err {
		return err
	}

	credential := &BasicCredential{
		ID: consumeApplicationUID,
		Consumer: Consumer{
			Username: consumerName,
		},
	}

	if err := client.Delete(context.TODO(), credential); nil != err && !apiError.IsNotFound(err) {
		return err
	}
	return nil
}
