// Package gateway provide struct convert, sync function between amp crd and Kong object
// diff.go will handle update event for product crd
// parse.go focus on parse amp struct to Kong object
// sync.go will sync Kong object to Kong gateway
// interface.go and client.go describe a Kong client
package gateway
