package gateway

import (
	"context"
	"fmt"

	"github.com/google/uuid"
	"github.com/juju/errors"

	ampv1 "alauda.io/amp-controller/api/v1"
	"alauda.io/amp-controller/gateway/kong"
)

// Validator validates amp objects
type Validator interface {
	APIProductValidator
	KongHttpValidator
	ConflictValidator
}

type KongHttpValidator interface {
	ValidateConsumer(consumer *Consumer) error
	ValidatePlugin(plugin *Plugin) error
}

// APIProductValidator ...
type APIProductValidator interface {
	ValidateAPIProduct(product *ampv1.ApiProduct) error
	MutateAPIProduct(product *ampv1.ApiProduct) (bool, error)
}

type ConflictValidator interface {
	ValidateConflictService(apiState, kongState *ampv1.ApiProduct) error
}

type validator struct {
	ctx    context.Context
	client kong.Client
}

func (v *validator) ValidateAPIProduct(product *ampv1.ApiProduct) error {
	if nil == product {
		return nil
	}
	if product.Name == "" {
		return errors.Annotate(errors.New("invalidate product: empty name"), "ValidateAPIProduct")
	}
	if product.Spec.ProjectName == "" {
		return errors.Annotate(errors.New("invalidate product: empty project name"), "ValidateAPIProduct")
	}
	if product.Spec.OwnerUserName == "" {
		return errors.Annotate(errors.New("invalidate product: empty developer name"), "ValidateAPIProduct")
	}
	annotations := product.GetAnnotations()
	if nil == annotations {
		return errors.Annotate(errors.New("invalidate product: empty annotation"), "ValidateAPIProduct")
	}
	if _, err := uuid.Parse(annotations[AnnotationRequestTerminationID]); nil != err {
		return errors.New("invalidate product: missing request-termination-id")
	}
	if _, err := uuid.Parse(annotations[AnnotationACLID]); nil != err {
		return errors.New("invalidate product: missing acl-id")
	}
	//if _, err := uuid.Parse(product.Spec.Id); nil != err {
	//	return errors.New("invalidate product: missing request-termination-id")
	//}
	//if _, err := uuid.Parse(product.Spec.ApiDefinition.Id); nil != err {
	//	return false
	//}
	if nil == product.Spec.ApiDefinition.ApiSecurityMode {
		return errors.New("invalidate product: missing security mode")
	}
	if _, err := uuid.Parse(product.Spec.ApiDefinition.ApiSecurityMode.Id); nil != err {
		return errors.New("invalidate product: missing security mode id")
	}
	if _, err := uuid.Parse(product.Spec.ApiDefinition.ApiService.Id); nil != err {
		return errors.New("invalidate product: missing api service id")
	}
	if len(product.Spec.ApiDefinition.ApiService.EndPoint) == 0 {
		return errors.New("invalidate product: missing api service endpoint")
	}
	for _, svcPlugin := range product.Spec.ApiDefinition.ApiService.Strategies {
		if _, err := uuid.Parse(svcPlugin.Id); nil != err {
			svcPlugin.Id = uuid.New().String()
		}
	}
	for _, apiRoute := range product.Spec.ApiDefinition.ApiPaths {
		// ignore empty route id, might be new route
		if _, err := uuid.Parse(apiRoute.Id); nil != err {
			apiRoute.Id = uuid.New().String()
		}
		for _, routePlugin := range apiRoute.Strategies {
			if _, err := uuid.Parse(routePlugin.Id); nil != err {
				routePlugin.Id = uuid.New().String()
			}
		}
	}
	return nil
}

func (v *validator) MutateAPIProduct(product *ampv1.ApiProduct) (bool, error) {
	if nil == product {
		return false, nil
	}
	var mutated, initalized bool
	var err error
	if product.Spec.OwnerUserName == "" {
		return false, errors.Annotate(errors.New("invalidate product: empty developer name"), "MutateAPIProduct")
	}
	if product.Spec.ProjectName == "" {
		return false, errors.Annotate(errors.New("invalidate product: empty project name"), "MutateAPIProduct")
	}

	annotations := product.GetAnnotations()
	if nil == annotations {
		annotations = make(map[string]string, 10)
		annotations[PluginSynced] = PluginSyncedFalse
		annotations[RoutePrefix] = product.GetName()
		mutated = true
	} else {
		initalized = annotations[PluginSynced] == PluginSyncedTrue
	}

	if _, err := uuid.Parse(annotations[AnnotationRequestTerminationID]); nil != err {
		if initalized {
			return false, errors.Annotate(err, "MutateAPIProduct")
		}
		annotations[AnnotationRequestTerminationID] = uuid.New().String()
		mutated = true
	}

	if _, err := uuid.Parse(annotations[AnnotationACLID]); nil != err {
		if initalized {
			return false, errors.Annotate(err, "MutateAPIProduct")
		}
		annotations[AnnotationACLID] = uuid.New().String()
		mutated = true
	}

	if _, err := uuid.Parse(product.Spec.Id); nil != err {
		product.Spec.Id = uuid.New().String()
		mutated = true
	}

	if _, err := uuid.Parse(product.Spec.ApiDefinition.ApiSecurityMode.Id); nil != err {
		product.Spec.ApiDefinition.ApiSecurityMode.Id = uuid.New().String()
		mutated = true
	}

	for _, svcPlugin := range product.Spec.ApiDefinition.ApiService.Strategies {
		if nil != svcPlugin {
			if _, err := uuid.Parse(svcPlugin.Id); nil != err {
				// new strategies could be added in a update, just ignor uuid error
				svcPlugin.Id = uuid.New().String()
				mutated = true
			}
		}
	}

	for _, apiRoute := range product.Spec.ApiDefinition.ApiPaths {
		if nil != apiRoute {
			if _, err := uuid.Parse(apiRoute.Id); nil != err {
				// new api path could be added in a update, just ignor uuid error
				apiRoute.Id = uuid.New().String()
				mutated = true
			}

			for _, routePlugin := range apiRoute.Strategies {
				if _, err := uuid.Parse(routePlugin.Id); nil != err {
					// new strategies could be added in a update, just ignor uuid error
					routePlugin.Id = uuid.New().String()
					mutated = true
				}
			}
		}
	}
	return mutated, err

}

func (v *validator) ValidateConsumer(consumer *Consumer) error {
	if nil == consumer {
		return nil
	}
	if consumer.Username == "" {
		return errors.New("user name can't be empty")
	}
	newConsumer := consumer.DeepCopy()
	obj := NewValidationObject(newConsumer)
	return v.client.Create(v.ctx, obj)
}

func (v *validator) ValidatePlugin(plugin *Plugin) error {
	if nil == plugin {
		return nil
	}
	// need to use a deep copy, otherwise the origin object will be overrode
	newPlugin := plugin.DeepCopy()
	obj := NewValidationObject(newPlugin)
	return v.client.Create(v.ctx, obj)
}

// ValidateConflictService validate whether api product service's name, uuid, owner is the
// same with the config in Kong's service
func (v *validator) ValidateConflictService(apiState, kongState *ampv1.ApiProduct) error {
	if nil == kongState || nil == apiState {
		return nil
	}
	productName := apiState.GetName()
	kongServiceName := kongState.GetName()

	if productName != kongServiceName {
		return fmt.Errorf("conflict: product name is different with kong service name: productName=%s, kongName=%s", productName, kongServiceName)
	}

	productServiceID := apiState.Spec.ApiDefinition.ApiService.Id
	kongServiceID := kongState.Spec.ApiDefinition.ApiService.Id

	if productServiceID != kongServiceID {
		return fmt.Errorf("conflict: product name is already exists in kong: serviceName=%s, productServiceID=%s, kongServiceID=%s", kongServiceName, productServiceID, kongServiceID)
	}

	productOwner := apiState.Spec.OwnerUserName
	kongOwner := kongState.Spec.OwnerUserName

	if productOwner != kongOwner {
		return fmt.Errorf("conflict: product owner user is different with kong service: productOwner=%s, kongOwner=%s", productOwner, kongOwner)
	}

	// TODO: add project name and product name validation in service tags

	return nil
}

// NewValidator ...
func NewValidator(ctx context.Context, client kong.Client) Validator {
	return &validator{ctx: ctx, client: client}
}
