package gateway

import (
	"context"
	"crypto"
	b64 "encoding/base64"
	"encoding/json"
	"fmt"
	"path"
	"reflect"
	"strings"

	"github.com/sethvargo/go-password/password"

	"github.com/alauda/kube-rest/pkg/types"
	"github.com/dgrijalva/jwt-go"
	"github.com/go-logr/logr"
	"github.com/google/uuid"
	"github.com/juju/errors"
	"github.com/sony/sonyflake"
	"github.com/spf13/cast"
	apiError "k8s.io/apimachinery/pkg/api/errors"
	logf "sigs.k8s.io/controller-runtime/pkg/runtime/log"

	ampv1 "alauda.io/amp-controller/api/v1"
	"alauda.io/amp-controller/common"
	"alauda.io/amp-controller/gateway/diff"
	"alauda.io/amp-controller/gateway/diff/sets"
	"alauda.io/amp-controller/gateway/kong"
)

type collection struct {
	services []*Service
	plugins  []*Plugin
	routes   []*Route

	serviceForSwagger *Service
	routeForSwagger   *Route
}

// Syncer handle route, service, plugin synchronization
type Syncer interface {
	diff.Differ
	Mutated() bool
	Do() error
	SyncForTest(action string) error
}

var _ Syncer = &syncer{}

type syncer struct {
	origin        *ampv1.ApiProduct
	modified      *ampv1.ApiProduct
	current       collection
	target        collection
	mutated       bool
	parser        Parser
	validator     Validator
	client        kong.Client
	clientForTest kong.Client
	context       context.Context
	Log           logr.Logger
}

func (s *syncer) parse(strategies []*ampv1.ApiStrategy, apiPaths []*ampv1.ApiPath) error {
	if nil == s.modified {
		return errors.New("product is nil")
	}

	newAnno := s.modified.GetAnnotations()
	if nil == newAnno {
		return errors.New("product corrupt: nil annotations")
	}
	isCreate := true

	if nil != s.origin {
		s.Log.Info("making parse from origin")
		isCreate = false
		s.modified.Spec.ProjectName = s.origin.Spec.ProjectName
		s.modified.Spec.OwnerUserName = s.origin.Spec.OwnerUserName
	}
	kongService := s.parser.ParseService(s.modified.GetName(), s.modified.Spec.ProjectName, s.modified.Spec.OwnerUserName, &s.modified.Spec.ApiDefinition.ApiService)
	if isCreate {
		aclID := newAnno[AnnotationACLID]
		term, err := s.parser.ParseAnnotationPlugin(aclID, string(ampv1.ACL), true, kongService)
		if nil != err {
			return err
		}
		s.target.plugins = append(s.target.plugins, term)
	}
	if s.modified.Status.Available != s.modified.Spec.Status.Available() || isCreate {
		tID := newAnno[AnnotationRequestTerminationID]
		term, err := s.parser.ParseAnnotationPlugin(tID, string(ampv1.RequestTermination), !s.modified.Spec.Status.Available(), kongService)
		if err != nil {
			return err
		}
		s.target.plugins = append(s.target.plugins, term)
	}

	// handle target
	s.target.services = append(s.target.services, kongService)

	// parse kong service plugins
	for _, st := range s.modified.Spec.ApiDefinition.ApiService.Strategies {
		s.target.plugins = append(s.target.plugins, s.parser.ParseStrategyPlugin(st, kongService, nil))
	}

	// parse kong route plugins
	for _, path := range s.modified.Spec.ApiDefinition.ApiPaths {
		routeBasePath := newAnno[RoutePrefix]
		if routeBasePath == "" {
			routeBasePath = s.modified.GetName()
		}
		protocols := s.modified.Spec.ApiDefinition.HttpProtocol
		route := s.parser.ParseRoute(path, routeBasePath, protocols, kongService)
		s.target.routes = append(s.target.routes, route)
		for _, pathSt := range path.Strategies {
			plugin := s.parser.ParseStrategyPlugin(pathSt, nil, route)
			if err := s.validator.ValidatePlugin(plugin); nil != err {
				return err
			}
			s.target.plugins = append(s.target.plugins, plugin)
		}
	}
	if nil != s.modified.Spec.ApiDefinition.ApiSecurityMode {
		s.target.plugins = append(s.target.plugins, s.parser.ParseStrategyPlugin(s.modified.Spec.ApiDefinition.ApiSecurityMode, kongService, nil))
	}

	// handle current
	for _, strategy := range strategies {
		s.current.plugins = append(s.current.plugins, s.parser.ParseStrategyPlugin(strategy, nil, nil))
	}
	for _, path := range apiPaths {
		route := s.parser.ParseRoute(path, "", nil, nil)
		s.current.routes = append(s.current.routes, route)
		for _, pathSt := range path.Strategies {
			s.current.plugins = append(s.current.plugins, s.parser.ParseStrategyPlugin(pathSt, nil, nil))
		}
	}

	return nil
}

func (s *syncer) Mutated() bool {
	return s.mutated
}

func (s *syncer) getOrigin(name string) error {
	s.Log.Info("get origin")
	defaultCtx := s.context
	apiService := ampv1.ApiService{}
	s.origin = nil

	service := &Service{Name: &name}

	if err := s.client.Get(defaultCtx, service); nil != err {
		return err
	}

	parseToStrategy := func(pl *PluginList) []*ampv1.ApiStrategy {
		if nil == pl {
			return nil
		}
		sts := []*ampv1.ApiStrategy{}
		for _, plugin := range pl.Data {
			sts = append(sts, &ampv1.ApiStrategy{Id: plugin.ID, Type: ampv1.PluginType(plugin.Name), Config: plugin.Config})
		}
		return sts
	}

	// get service plugin list
	apiService.Id = service.ID
	pList := &PluginList{Service: &Service{ID: apiService.Id}}
	params := types.QueryParameters{"size": cast.ToString(kong.GatewayConfig.DefautListSize)}
	defaultOptions := &types.Options{Params: params}
	if err := s.client.List(defaultCtx, pList, defaultOptions); nil != err && !apiError.IsNotFound(err) {
		return err
	}
	apiService.Strategies = parseToStrategy(pList)

	s.origin = &ampv1.ApiProduct{}
	s.origin.SetName(*service.Name)
	s.origin.Spec.ApiDefinition.ApiService = apiService

	// get tags arbitrarily
	if len(service.Tags) == 3 {
		_, project, owner, _ := service.Tags.Dump()
		s.origin.Spec.OwnerUserName = owner
		s.origin.Spec.ProjectName = project
	}

	// get route list and plugin list
	rtList := &RouteList{Service: &Service{ID: apiService.Id}}
	if err := s.client.List(defaultCtx, rtList, defaultOptions); nil != err {
		if apiError.IsNotFound(err) {
			return nil
		} else {
			return err
		}
	}
	apiPaths := []*ampv1.ApiPath{}
	for _, route := range rtList.Data {
		if len(route.Paths) == 0 {
			continue
		}
		clone := &ampv1.ApiPath{Id: route.ID, Path: path.Join("/", path.Base(route.Paths[0]))}
		pList := &PluginList{Route: &Route{ID: route.ID}}
		if err := s.client.List(defaultCtx, pList, defaultOptions); nil != err && !apiError.IsNotFound(err) {
			return err
		}
		clone.Strategies = parseToStrategy(pList)
		apiPaths = append(apiPaths, clone)
	}

	s.origin.Spec.ApiDefinition.ApiPaths = apiPaths

	return nil
}

func (s *syncer) Diff() error {
	// Do something dirty, including diff, validate and parse
	var recycled, recycledAPIPathStrategies []*ampv1.ApiStrategy
	var recycledAPIPath []*ampv1.ApiPath

	securePlugin := func(strategy *ampv1.ApiStrategy) bool {
		if !ampv1.IsSecureModePlugin(strategy) {
			return false
		}
		// diff secure mode plugin and parse id
		if s.modified.Spec.ApiDefinition.ApiSecurityMode.Type == strategy.Type {
			s.modified.Spec.ApiDefinition.ApiSecurityMode.Id = strategy.Id
			return true
		}
		return false
	}

	aclPlugin := func(strategy *ampv1.ApiStrategy) bool {
		if !ampv1.IsAccessControlPlugin(strategy) {
			return false
		}
		annotations := s.modified.GetAnnotations()
		if nil != annotations {
			if strategy.Type == ampv1.ACL {
				annotations[AnnotationACLID] = strategy.Id
			}
			if strategy.Type == ampv1.RequestTermination {
				annotations[AnnotationRequestTerminationID] = strategy.Id
			}
			s.modified.SetAnnotations(annotations)
		}
		return true
	}

	if nil != s.origin {
		s.Log.Info("making diff")
		// diff service and parse id
		apiService := s.origin.Spec.ApiDefinition.ApiService
		if len(apiService.Strategies) > 0 {
			servicePluginSet := sets.NewStrategy(s.modified.Spec.ApiDefinition.ApiService.Strategies...)
			for _, st := range servicePluginSet.DifferenceAndParse(apiService.Strategies...) {
				if !securePlugin(st) && !aclPlugin(st) {
					recycled = append(recycled, st)
				}
			}
		}

		// diff api service
		// s.modified.Spec.ApiDefinition.ApiService.Id = apiService.Id

		apiPaths := s.origin.Spec.ApiDefinition.ApiPaths
		if len(apiPaths) > 0 {
			s.Log.Info("making diff for api paths")
			// diff api path and plugin, parse id
			apiPathPluginSet := sets.NewAPIPath(s.modified.Spec.ApiDefinition.ApiPaths...)
			recycledAPIPath, recycledAPIPathStrategies = apiPathPluginSet.DifferenceAndParse(apiPaths...)
			recycled = append(recycled, recycledAPIPathStrategies...)
		}
	}

	// TODO: support update ApiSecureMode
	// parse && validate
	return s.parse(recycled, recycledAPIPath)
}

func (s *syncer) sync() error {
	defaultCtx := s.context

	// sync current
	for _, plugin := range s.current.plugins {
		if nil == plugin {
			continue
		}
		index := fmt.Sprintf("%s/%s", plugin.Name, plugin.ID)
		s.Log.Info("Deleting", "plugin", index)
		if err := s.client.Delete(defaultCtx, plugin); nil != err {
			// s.Log.Info("error while deleting plugin", "error", err, "plugin", index)
			if !apiError.IsNotFound(err) && !apiError.IsConflict(err) {
				return err
			}
		}
	}

	for _, route := range s.current.routes {
		if nil == route {
			continue
		}
		index := fmt.Sprintf("%s/%s", route.Name, route.ID)
		s.Log.Info("Deleting", "route", index)
		if err := s.client.Delete(defaultCtx, route); nil != err {
			// s.Log.Info("error while deleting route", "error", err, "route", index)
			if !apiError.IsNotFound(err) && !apiError.IsConflict(err) {
				return err
			}
		}
	}

	// sync target
	// FIXME: compare && sync, two way merge should using Kong data, not from origin crd;
	//  it becomes three way merge in current implementation
	for _, service := range s.target.services {
		if nil == service {
			continue
		}
		index := fmt.Sprintf("%s/%s", *service.Name, service.ID)
		s.Log.Info("CreateOrUpdate", "service", index)
		// FIXME: this will cause config display problems
		clone := &Service{Name: service.Name}
		if err := s.client.Get(defaultCtx, clone); nil != err && !apiError.IsNotFound(err) {
			return err
		}
		if reflect.DeepEqual(clone, service) {
			s.Log.Info("CreateOrUpdate: your data is up to date with origin, nothing to update", "service", index)
			continue
		}
		if err := s.client.CreateOrUpdate(defaultCtx, service); nil != err {
			// s.Log.Info("error syncing service", "error", err, "service", index)
			return err
		}
	}

	for _, route := range s.target.routes {
		if nil == route {
			continue
		}
		index := fmt.Sprintf("%s/%s", route.Name, route.ID)
		s.Log.Info("CreateOrUpdate", "route", index)
		if err := s.client.CreateOrUpdate(defaultCtx, route); nil != err {
			// s.Log.Info("error syncing route", "error", err, "route", index)
			return err
		}
	}

	for _, plugin := range s.target.plugins {
		if nil == plugin {
			continue
		}
		index := fmt.Sprintf("%s/%s", plugin.Name, plugin.ID)
		s.Log.Info("CreateOrUpdate", "plugin", index)
		if err := s.client.CreateOrUpdate(defaultCtx, plugin); nil != err {
			// s.Log.Info("error syncing plugin", "error", err, "plugin", index)
			return err
		}
	}
	return nil
}

func (s *syncer) SyncForTest(action string) error {
	if s.modified.Spec.SwaggerFileUrl != "" {
		defaultCtx := s.context
		if action == "create" {
			// create swagger service, path and plugins
			endpoint := s.modified.Spec.ApiDefinition.ApiService.EndPoint
			svcID := s.modified.Spec.ApiDefinition.ApiServiceIDForTest
			svcName := s.modified.GetName() + "-test"
			serviceForSwagger := &Service{
				ID:   svcID,
				URL:  endpoint,
				Name: &svcName,
			}

			// path
			pathID := s.modified.Spec.ApiDefinition.ApiPathIDForTest
			routeBasePath := svcName
			// random string
			pathName := s.modified.Spec.ApiDefinition.ApiPathNameForTest
			protocols := []string{"http"}
			routeForSwagger := &Route{
				ID:        pathID,
				Methods:   []string{"GET", "HEAD", "POST", "PUT", "DELETE", "CONNECT", "OPTIONS", "TRACE", "PATCH"},
				Paths:     []string{path.Join("/", routeBasePath, pathName)},
				Protocols: protocols,
				Service:   Service{ID: svcID},
			}
			// create or update
			if err := s.clientForTest.CreateOrUpdate(defaultCtx, serviceForSwagger); nil != err {
				return err
			}
			if err := s.clientForTest.CreateOrUpdate(defaultCtx, routeForSwagger); nil != err {
				return err
			}
		} else if action == "delete" {
			svcID := s.modified.Spec.ApiDefinition.ApiServiceIDForTest
			serviceForSwagger := &Service{
				ID: svcID,
			}
			pathID := s.modified.Spec.ApiDefinition.ApiPathIDForTest
			routeForSwagger := &Route{
				ID: pathID,
			}
			// delete
			if err := s.clientForTest.Delete(defaultCtx, routeForSwagger); nil != err {
				s.Log.Info("error when delete path for test")
				return err
			}
			if err := s.clientForTest.Delete(defaultCtx, serviceForSwagger); nil != err {
				s.Log.Info("error when delete service for test")
				return err
			}
		}
	}
	return nil
}

func (s *syncer) Do() error {
	if err := s.Diff(); nil != err {
		return err
	}
	return s.sync()
}

func getOriginalConfiguration(product *ampv1.ApiProduct) (*ampv1.ApiProduct, error) {
	// var result *ampv1.ApiProduct
	annotations := product.GetAnnotations()
	if nil != annotations {
		if origin, ok := annotations[ampv1.LastAppliedConfigAnnotation]; ok {
			result := &ampv1.ApiProduct{}
			if err := json.Unmarshal([]byte(origin), result); nil != err {
				return nil, err
			}
			return result, nil
		}
	}
	return nil, nil
}

// NewSyncer create syncer for api product
func NewSyncer(instance *ampv1.ApiProduct, sessionID string) (Syncer, error) {
	var err error
	var client kong.Client
	var clientForTest kong.Client

	if nil == instance {
		return nil, errors.New("NewSyncer: instance is nil")
	}
	ctx := context.WithValue(context.Background(), common.RequestID, sessionID)
	if client, err = kong.NewGateWayClient(); nil != err {
		return nil, err
	}
	if clientForTest, err = kong.NewGateWayClientForTest(); nil != err {
		return nil, err
	}
	sc := &syncer{
		modified:      instance,
		parser:        NewParser(),
		validator:     NewValidator(ctx, client),
		client:        client,
		clientForTest: clientForTest,
		context:       ctx,
		Log:           logf.Log.WithValues(common.RequestID, sessionID),
	}

	if err := sc.getOrigin(instance.GetName()); nil != err && !apiError.IsNotFound(err) {
		// return http error
		return nil, err
	}
	err = nil
	if sc.origin == nil {
		// not configured in kong, create
		if sc.mutated, err = sc.validator.MutateAPIProduct(sc.modified); nil != err {
			return nil, err
		}
	} else {
		// update
		if err := sc.validator.ValidateConflictService(sc.modified, sc.origin); nil != err {
			return nil, err
		}
		if err := sc.validator.ValidateAPIProduct(sc.modified); nil != err {
			return nil, err
		}
	}
	sc.Log.Info("get sycer", "sc", sc, "origin", nil != sc.origin)
	return sc, nil
}

// SyncGatewayConsumer ...
func SyncGatewayConsumer(application *ampv1.ApiConsumeApplication) error {
	consumer := &Consumer{
		Username: application.GetName(),
		ID:       application.Spec.Id,
		Tags:     []string{application.GetName(), application.Spec.ProjectName, application.Spec.OwnerUserName},
	}
	client, err := kong.NewGateWayClient()
	if nil != err {
		return err
	}
	if nil != application.DeletionTimestamp {
		err = client.Delete(context.TODO(), consumer)
	} else {
		err = client.Create(context.TODO(), consumer)
		if nil != err && !apiError.IsAlreadyExists(err) && !apiError.IsConflict(err) {
			return err
		}
		return nil
	}
	return err
}

// SyncJWTBinding ...
func SyncJWTBinding(productSvc *ampv1.ApiProductService, product *ampv1.ApiProduct, order *ampv1.Order) (ampv1.APInfo, error) {
	var client kong.Client
	var err error

	if client, err = kong.NewGateWayClient(); nil != err {
		return nil, err
	}

	// create consumer jwt
	consumer := Consumer{Username: productSvc.Spec.ConsumeApplicationName}

	apiSecurity := product.Spec.ApiDefinition.ApiSecurityMode
	if nil == apiSecurity {
		return nil, nil
	}

	algorithm := cast.ToString(apiSecurity.Config[ampv1.JWTAlgorithm])
	key := cast.ToString(order.Spec.Security.Config[ampv1.JWTKey])
	secret := cast.ToString(order.Spec.Security.Config[ampv1.JWTSecret])

	var privateKey crypto.Signer
	var RSAPublicKey string
	var tokenString string
	orderName := order.GetName()
	if IsBatchOrder(orderName) {
		RSAPublicKey = cast.ToString(order.Spec.Security.Config[ampv1.JWTRSAPublicKey])
		tokenString = cast.ToString(order.Spec.Security.Config[ampv1.Beareroken])
		if RSAPublicKey == "" || tokenString == "" {
			return nil, errors.New("RSAPublicKey or tokenString not set for jwt Authentication.")
		}
	} else {
		privateKey, err = NewPrivateKey()
		if nil != err {
			return nil, err
		}
		RSAPublicKeyByte, err := EncodePublicKeyPEM(privateKey.Public())
		if err != nil {
			return nil, err
		}
		RSAPublicKey = string(RSAPublicKeyByte)
		tokenString, err = NewBearerToken(privateKey, key)
		if nil != err {
			return nil, err
		}
	}

	credential := &JWTCredential{
		ID:           string(productSvc.GetUID()),
		Key:          key,
		Secret:       secret,
		Algorithm:    Algorithm(algorithm),
		RSAPublicKey: RSAPublicKey,
		Consumer:     consumer,
	}

	if AlgorithmHS256 != credential.Algorithm && AlgorithmRS256 != credential.Algorithm {
		credential.Algorithm = AlgorithmHS256
		credential.RSAPublicKey = ""
	}
	if err := client.Create(context.TODO(), credential); nil != err && !apiError.IsAlreadyExists(err) {
		return nil, err
	}
	info := ampv1.APInfo{
		ampv1.APIPluginID:     credential.ID,
		ampv1.APISecurityType: string(ampv1.JWT),
		ampv1.JWTKey:          credential.Key,
		ampv1.JWTSecret:       credential.Secret,
		ampv1.JWTAlgorithm:    string(credential.Algorithm),
	}

	if AlgorithmRS256 == credential.Algorithm || AlgorithmES256 == credential.Algorithm {
		info[ampv1.JWTRSAPublicKey] = RSAPublicKey
		info[ampv1.Beareroken] = tokenString
	}

	if AlgorithmHS256 == credential.Algorithm {
		token := jwt.NewWithClaims(jwt.SigningMethodHS256, jwt.MapClaims{
			"iss": credential.Key,
		})
		tokenString, err := token.SignedString([]byte(credential.Secret))
		if nil != err {
			return nil, err
		}
		info[ampv1.Beareroken] = tokenString
	}
	return info, nil
}

// SyncJWTBindingDelete ...
func SyncJWTBindingDelete(productServiceID string, consumeApplicationName string) error {
	var client kong.Client
	var err error

	if client, err = kong.NewGateWayClient(); nil != err {
		return err
	}
	credential := &JWTCredential{
		ID: productServiceID,
		Consumer: Consumer{
			Username: consumeApplicationName,
		},
	}

	if err := client.Delete(context.TODO(), credential); nil != err && !apiError.IsNotFound(err) {
		return err
	}
	return nil
}

func randomUsername() string {
	var st sonyflake.Settings
	sf := sonyflake.NewSonyflake(st)
	id, _ := sf.NextID()
	return cast.ToString(id)
}

// SyncBasicBinding ...
func SyncBasicBinding(productSvc *ampv1.ApiProductService, apiSecurity *ampv1.ApiStrategy, order *ampv1.Order) (ampv1.APInfo, error) {
	var client kong.Client
	var err error

	if client, err = kong.NewGateWayClient(); nil != err {
		return nil, err
	}

	// create consumer
	consumer := Consumer{
		Username: productSvc.Spec.ConsumeApplicationName,
		// ID:       order.Spec.ConsumeApplicationID,
	}

	if nil == apiSecurity {
		return nil, nil
	}

	var username, pwd string
	orderName := order.GetName()
	if IsBatchOrder(orderName) {
		username = cast.ToString(order.Spec.Security.Config[ampv1.BasicUserName])
		pwd = cast.ToString(order.Spec.Security.Config[ampv1.BasicPassword])
		if username == "" || pwd == "" {
			return nil, errors.New("username or password not set for basic Authentication.")
		}
	} else {
		username = randomUsername()
		pwd, _ = password.Generate(32, 10, 10, true, false)
	}

	credential := &BasicCredential{
		ID:       string(productSvc.GetUID()),
		Username: username,
		Password: pwd,
		Consumer: consumer,
	}

	if err := client.Create(context.TODO(), credential); nil != err && !apiError.IsAlreadyExists(err) {
		return nil, err
	}
	info := ampv1.APInfo{
		ampv1.APIPluginID:     credential.ID,
		ampv1.APISecurityType: string(ampv1.BASIC),
		ampv1.BasicUserName:   credential.Username,
		ampv1.BasicPassword:   pwd,
	}
	data := credential.Username + ":" + pwd
	info[ampv1.BasicToken] = b64.StdEncoding.EncodeToString([]byte(data))
	return info, nil
}

// SyncBasicBindingDelete ...
func SyncBasicBindingDelete(productServiceID string, consumeApplicationName string) error {
	var client kong.Client
	var err error

	if client, err = kong.NewGateWayClient(); nil != err {
		return err
	}

	credential := &BasicCredential{
		ID: productServiceID,
		Consumer: Consumer{
			Username: consumeApplicationName,
		},
	}

	if err := client.Delete(context.TODO(), credential); nil != err && !apiError.IsNotFound(err) {
		return err
	}
	return nil
}

func GetCurrentACLBinding(ctx context.Context, productSvc *ampv1.ApiProductService, logger logr.Logger) (*ACL, error) {
	var err error
	var client kong.Client
	if client, err = kong.NewGateWayClient(); nil != err {
		return nil, err
	}
	acl := &ACL{
		ID:       string(productSvc.GetUID()),
		Consumer: Consumer{Username: productSvc.Spec.ConsumeApplicationName},
	}
	if err := client.Get(ctx, acl); nil != err {
		return nil, err
	}
	logger.Info("GetCurrentACLBinding, get acl", "acl", acl)
	return acl, nil
}

func SyncDeleteGatewayACL(ctx context.Context, productSvc *ampv1.ApiProductService, logger logr.Logger) error {
	var err error
	var client kong.Client
	if client, err = kong.NewGateWayClient(); nil != err {
		return err
	}
	// delete consumer from acl
	acl := &ACL{
		ID:       string(productSvc.GetUID()),
		Consumer: Consumer{Username: productSvc.Spec.ConsumeApplicationName},
	}
	logger.Info("SyncDeleteGatewayACL, delete acl", "acl", acl)
	if err := client.Delete(ctx, acl); nil != err && !apiError.IsNotFound(err) {
		return err
	}
	return nil
}

func SyncGatewayACLBinding(ctx context.Context, currentACL *ACL, productSvc *ampv1.ApiProductService, product *ampv1.ApiProduct, logger logr.Logger) error {
	var err error
	var client kong.Client
	if client, err = kong.NewGateWayClient(); nil != err {
		return err
	}
	createACL := func(logger2 logr.Logger) error {
		apiService := product.Spec.ApiDefinition.ApiService

		consumer := Consumer{Username: productSvc.Spec.ConsumeApplicationName}

		// Add consumer to acl
		acl := &ACL{
			ID:       string(productSvc.GetUID()),
			Group:    fmt.Sprintf("%s-%s", WhiteListPrefix, apiService.Id),
			Consumer: consumer,
		}
		logger2.Info("SyncGatewayACLBinding, create acl", "acl", acl)
		if err := client.Create(ctx, acl); nil != err && !apiError.IsAlreadyExists(err) {
			return err
		}
		return nil
	}
	if nil == currentACL && ampv1.ServiceOpened == productSvc.Status.Phase {
		if err := createACL(logger); nil != err {
			return err
		}
		return nil
	}
	if nil != currentACL && ampv1.ServiceOpened == productSvc.Status.Phase {
		apiService := product.Spec.ApiDefinition.ApiService
		group := fmt.Sprintf("%s-%s", WhiteListPrefix, apiService.Id)
		if currentACL.Group != group || currentACL.Consumer.Username != productSvc.Spec.ConsumeApplicationName {
			return fmt.Errorf("error create acl for api product service: another acl with same id exists, apiService=%s ACLID=%s", productSvc.GetName(), currentACL.ID)
		}
	}
	return nil
}

// SyncGatewayBinding sync consumer and product in order
// binding consumer to service
// * associating consumer to acl
// * if oauth2:
//     * create oauth2 route for service if not present
//     * create consumer credentials
// * if jwt:
//     * create consumer credentials
func SyncGatewayBinding(productSvc *ampv1.ApiProductService, product *ampv1.ApiProduct, order *ampv1.Order) (ampv1.APInfo, error) {

	var err error

	apiSecurity := product.Spec.ApiDefinition.ApiSecurityMode
	if nil == apiSecurity {
		return nil, nil
	}

	var info ampv1.APInfo
	switch apiSecurity.Type {
	case ampv1.OAuth2:
		info, err = SyncOAuth2Binding(productSvc, product, order)
	case ampv1.JWT:
		info, err = SyncJWTBinding(productSvc, product, order)
	case ampv1.BASIC:
		info, err = SyncBasicBinding(productSvc, apiSecurity, order)
	}

	if nil != err {
		return nil, err
	}

	if nil == info {
		info = ampv1.APInfo{}
	}
	httpProtocol, httpsProtocol := ContainsHTTPProtocol(product.Spec.ApiDefinition.HttpProtocol)
	if httpProtocol {
		info[ampv1.APIEndpoint] = fmt.Sprintf("%s/%s", kong.GatewayConfig.ProxyServer.String(), product.GetName())
	}
	if httpsProtocol {
		info[ampv1.APISecureEndpoint] = fmt.Sprintf("%s/%s", kong.GatewayConfig.ProxySecureServer.String(), product.GetName())
	}
	return info, nil
}

// SyncGatewayBindingDelete ...
func SyncGatewayBindingDelete(instance *ampv1.ApiProductService) error {
	var client kong.Client
	var err error

	if client, err = kong.NewGateWayClient(); nil != err {
		return err
	}

	// delete consumer from acl
	acl := &ACL{
		ID: instance.Spec.ID,
		Consumer: Consumer{
			Username: instance.Spec.ConsumeApplicationName,
		},
	}
	if err := client.Delete(context.TODO(), acl); nil != err && !apiError.IsNotFound(err) {
		return err
	}

	authType := ampv1.PluginType(instance.Spec.Info["security_type"])
	switch authType {
	case ampv1.OAuth2:
		err = SyncOAuth2BindingDelete(instance.Spec.ID, instance.Spec.ConsumeApplicationName)
	case ampv1.JWT:
		err = SyncJWTBindingDelete(instance.Spec.ID, instance.Spec.ConsumeApplicationName)
	case ampv1.BASIC:
		err = SyncBasicBindingDelete(instance.Spec.ID, instance.Spec.ConsumeApplicationName)
	}
	if nil != err {
		return err
	}
	return nil
}

// SyncOAuth2Binding ...
func SyncOAuth2Binding(productSvc *ampv1.ApiProductService, product *ampv1.ApiProduct, order *ampv1.Order) (ampv1.APInfo, error) {
	var client kong.Client
	var err error

	if client, err = kong.NewGateWayClient(); nil != err {
		return nil, err
	}
	consumer := Consumer{Username: productSvc.Spec.ConsumeApplicationName}
	service := &Service{ID: product.Spec.ApiDefinition.ApiService.Id}

	if err := client.Get(context.TODO(), service); nil != err {
		return nil, err
	}

	route := &Route{
		ID:        uuid.New().String(),
		Paths:     []string{path.Join("/", product.GetName(), OAuth2Endpoint)},
		Methods:   []string{"POST"},
		Protocols: []string{"https"},
		Service:   *service,
	}
	if err := client.Create(context.TODO(), route); nil != err {
		return nil, err
	}

	var clientID, clientSecret string
	orderName := order.GetName()
	if IsBatchOrder(orderName) {
		clientID = cast.ToString(order.Spec.Security.Config[ampv1.OAuth2ClientId])
		clientSecret = cast.ToString(order.Spec.Security.Config[ampv1.OAuth2ClientSecret])
		if clientID == "" || clientSecret == "" {
			return nil, errors.New("clientID or clientSecret not set for oauth2 Authentication.")
		}
	} else {
		apiSecurity := product.Spec.ApiDefinition.ApiSecurityMode
		clientID = cast.ToString(apiSecurity.Config[ampv1.OAuth2ClientId])
		clientSecret = cast.ToString(apiSecurity.Config[ampv1.OAuth2ClientSecret])
	}

	// in case it's a update
	credentials := &OAuth2Credential{
		ID:           string(productSvc.GetUID()),
		Name:         productSvc.Spec.ConsumeApplicationName,
		RedirectUris: []string{"http://www.alauda.cn"},
		Consumer:     consumer,
	}
	if "" != clientID && "" != clientSecret {
		credentials.ClientID = clientID
		credentials.ClientSecret = clientSecret
	}
	if err := client.Create(context.TODO(), credentials); nil != err && !apiError.IsAlreadyExists(err) {
		return nil, err
	}

	productName := product.GetName()
	info := ampv1.APInfo{
		ampv1.APIPluginID:         credentials.ID,
		ampv1.APISecurityType:     string(ampv1.OAuth2),
		ampv1.OAuth2ClientId:      credentials.ClientID,
		ampv1.OAuth2ClientSecret:  credentials.ClientSecret,
		ampv1.OAuth2TokenEndpoint: fmt.Sprintf("%s/%s%s", kong.GatewayConfig.ProxySecureServer.String(), productName, OAuth2Endpoint),
	}
	return info, nil
}

// SyncOAuth2BindingDelete ...
func SyncOAuth2BindingDelete(productServiceID string, consumeApplicationName string) error {
	var client kong.Client
	var err error

	if client, err = kong.NewGateWayClient(); nil != err {
		return err
	}
	credentials := &OAuth2Credential{
		ID: productServiceID,
		Consumer: Consumer{
			Username: consumeApplicationName,
		},
	}
	if err := client.Delete(context.TODO(), credentials); nil != err && !apiError.IsNotFound(err) {
		return err
	}
	return nil
}

// ContainsHTTPProtocol ...
func ContainsHTTPProtocol(protocols []string) (bool, bool) {
	var http, https bool
	for _, protocol := range protocols {
		if strings.EqualFold(protocol, "http") {
			http = true
		}
		if strings.EqualFold(protocol, "https") {
			https = true
		}
	}
	return http, https
}
