package sets

import (
	ampv1 "alauda.io/amp-controller/api/v1"
)

// Strategy is a set of Strategy, implemented via map[ampv1.PluginType]*ampv1.ApiStrategy for minimal memory consumption.
type Strategy map[ampv1.PluginType]*ampv1.ApiStrategy

// NewStrategy creates a Strategy from a list of values.
func NewStrategy(items ...*ampv1.ApiStrategy) Strategy {
	s := make(Strategy, len(items))
	s.Insert(items...)
	return s
}

// Insert adds items to the set.
func (s Strategy) Insert(items ...*ampv1.ApiStrategy) {
	for _, item := range items {
		s[item.Type] = item
	}
}

// Has returns true if and only if item is contained in the set.
func (s Strategy) Has(t ampv1.PluginType) bool {
	_, contained := s[t]
	return contained
}

// DifferenceAndParse returns a set of objects that are not in s
// For example:
// s1 = {a1, a2, a3}
// s2 = {a1, a2, a4, a5}
// s1.Difference({a1, a2, a4, a5} ...) = {a4, a5}
// s2.Difference({a1, a2, a3} ...) = {a3}
func (s Strategy) DifferenceAndParse(items ...*ampv1.ApiStrategy) []*ampv1.ApiStrategy {
	var result []*ampv1.ApiStrategy
	for _, item := range items {
		if s.Has(item.Type) {
			s[item.Type].Id = item.Id
		} else {
			result = append(result, item)
		}
	}
	return result
}
