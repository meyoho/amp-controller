package sets

import (
	ampv1 "alauda.io/amp-controller/api/v1"
)

// Strategy is a set of Strategy, implemented via map[ampv1.PluginType]ampv1.ApiStrategy for minimal memory consumption.
type Subscription map[string]ampv1.SubscriptionPlan

// NewStrategy creates a Strategy from a list of values.
func NewSubscription(items ...ampv1.SubscriptionPlan) Subscription {
	s := make(Subscription, len(items))
	s.Insert(items...)
	return s
}

// Insert adds items to the set.
func (s Subscription) Insert(items ...ampv1.SubscriptionPlan) {
	for _, item := range items {
		s[item.Id] = item
	}
}

// Has returns true if and only if item is contained in the set.
func (s Subscription) Has(t ampv1.SubscriptionPlan) bool {
	_, contained := s[t.Id]
	return contained
}

// DifferenceAndParse returns a set of objects that are not in s
// For example:
// s1 = {a1, a2, a3}
// s2 = {a1, a2, a4, a5}
// s1.Difference({a1, a2, a4, a5} ...) = {a3}
// s2.Difference({a1, a2, a3} ...) = {a4, a5}
func (s Subscription) Difference(items ...ampv1.SubscriptionPlan) []ampv1.SubscriptionPlan {
	var result []ampv1.SubscriptionPlan
	for _, item := range items {
		if !s.Has(item) {
			result = append(result, item)
		}
	}
	return result
}
