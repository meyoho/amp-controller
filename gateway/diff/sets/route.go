package sets

import (
	ampv1 "alauda.io/amp-controller/api/v1"
)

// APIPath is a set of Strategy, implemented via map[ampv1.PluginType]*ampv1.ApiStrategy for minimal memory consumption.
type APIPath map[string]*ampv1.ApiPath

// NewAPIPath creates a APIPath from a list of values.
func NewAPIPath(items ...*ampv1.ApiPath) APIPath {
	s := make(APIPath, len(items))
	s.Insert(items...)
	return s
}

// Insert adds items to the set.
func (p APIPath) Insert(items ...*ampv1.ApiPath) {
	for _, item := range items {
		p[item.Path] = item
	}
}

// Has returns true if and only if item is contained in the set.
func (p APIPath) Has(key string) bool {
	_, contained := p[key]
	return contained
}

// DifferenceAndParse returns a set of objects that are not in s
// For example:
// s1 = {a1, a2, a3}
// s2 = {a1, a2, a4, a5}
// s1.Difference({a1, a2, a4, a5} ...) = {a3}
// s2.Difference({a1, a2, a3} ...) = {a4, a5}
func (p APIPath) DifferenceAndParse(items ...*ampv1.ApiPath) ([]*ampv1.ApiPath, []*ampv1.ApiStrategy) {
	var paths []*ampv1.ApiPath
	var strategies []*ampv1.ApiStrategy
	for _, item := range items {
		if p.Has(item.Path) {
			path := p[item.Path]
			path.Id = item.Id
			set := NewStrategy(path.Strategies...)
			strategies = append(strategies, set.DifferenceAndParse(item.Strategies...)...)
		} else {
			paths = append(paths, item)
		}
	}
	return paths, strategies
}
