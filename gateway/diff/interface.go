package diff

// Differ crack the api product into services, api paths and plugins
// parsing the Kong object uuid in api product from current to target
type Differ interface {
	Diff() error
}
