package kong

import (
	"crypto/tls"
	"net"
	"net/http"
	"net/url"
	"strings"
	"time"

	"github.com/juju/errors"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/runtime/schema"
	"k8s.io/apimachinery/pkg/runtime/serializer"
	"k8s.io/client-go/rest"
)

var (
	Scheme               = runtime.NewScheme()
	Codecs               = serializer.NewCodecFactory(Scheme)
	GatewayConfig        = KongConfig{}
	GatewayConfigForTest = KongConfig{}
)

const (
	ContentType              = "Content-Type"
	DefaultContentType       = "application/json"
	DefaultAcceptContentType = "application/json"
	DefaultUserAgent         = "amp-controller"
	DefaultApiPath           = "/not-exists"
	DefaultClientQPS         = 1e6
	DefaultClientBurst       = 1e6
)

// KongConfig kong config
type KongConfig struct {
	ProxySecureServer *url.URL
	ProxyServer       *url.URL
	Server            *url.URL
	Burst             int
	QPS               float32
	DefautListSize    int64
	ServerForTest     *url.URL
	StandBy           bool
}

func (cfg *KongConfig) String() string {
	return cfg.Server.String()
}

// GetRestConfig get rest config
func GetRestConfig() (*rest.Config, error) {
	if nil == GatewayConfig.Server {
		return nil, errors.New("nil kong server")
	}
	config := &rest.Config{
		Host: GatewayConfig.Server.String(),
		ContentConfig: rest.ContentConfig{
			AcceptContentTypes:   DefaultAcceptContentType,
			ContentType:          DefaultContentType,
			NegotiatedSerializer: Codecs,
			GroupVersion:         &schema.GroupVersion{},
		},
		UserAgent: DefaultUserAgent,
		APIPath:   DefaultApiPath,
		Burst:     GatewayConfig.Burst,
		QPS:       GatewayConfig.QPS,
	}
	if 0 == config.Burst {
		config.Burst = DefaultClientBurst
	}
	if 0 == config.QPS {
		config.QPS = DefaultClientQPS
	}
	if strings.EqualFold("https", GatewayConfig.Server.Scheme) {
		t := &http.Transport{
			Proxy: http.ProxyFromEnvironment,
			DialContext: (&net.Dialer{
				Timeout:   30 * time.Second,
				KeepAlive: 30 * time.Second,
				DualStack: true,
			}).DialContext,
			ForceAttemptHTTP2:     true,
			MaxIdleConns:          100,
			IdleConnTimeout:       90 * time.Second,
			TLSHandshakeTimeout:   10 * time.Second,
			ExpectContinueTimeout: 1 * time.Second,
			TLSClientConfig:       &tls.Config{InsecureSkipVerify: true},
		}
		config.Transport = t
	}
	return config, nil
}
