package kong

import (
	"context"
	"net/url"
	"reflect"

	log2 "alauda.io/amp-controller/log"

	"github.com/alauda/kube-rest/pkg/config"
	"github.com/alauda/kube-rest/pkg/rest"
	"github.com/alauda/kube-rest/pkg/types"

	logf "sigs.k8s.io/controller-runtime/pkg/runtime/log"
)

var log = logf.Log.WithName("gateway")

var _ Client = &client{}

type client struct {
	Client rest.Client
}

var defaultOptions = &types.Options{Header: url.Values{"Content-Type": []string{"application/json"}}}

// Create implements client.Client
func (c *client) Create(ctx context.Context, obj rest.Object) error {
	logger := log2.GetLogger(ctx, log)
	err := c.Client.Create(ctx, obj, defaultOptions)
	if nil != err {
		bt, _ := obj.Data()
		logger.Info("rest create error", "error", err, "url", obj.TypeLink(), "body", bt)
	}
	return err
}

// Update implements client.Client
func (c *client) CreateOrUpdate(ctx context.Context, obj rest.Object) error {
	logger := log2.GetLogger(ctx, log)
	err := c.Client.Update(ctx, obj, defaultOptions)
	if nil != err {
		bt, _ := obj.Data()
		logger.Info("rest update error", "error", err, "type", reflect.TypeOf(err), "url", obj.SelfLink(), "body", bt)
	}
	return err
}

func (c *client) Get(ctx context.Context, obj rest.Object) error {
	var err error
	logger := log2.GetLogger(ctx, log)
	if err = c.Client.Get(ctx, obj); nil != err {
		logger.Info("rest get error", "error", err, "url", obj.SelfLink())
	}
	return err
}

func (c *client) List(ctx context.Context, list rest.ObjectList, option types.Option) error {
	logger := log2.GetLogger(ctx, log)
	err := c.Client.List(ctx, list, option)
	if nil != err {
		logger.Info("rest list error", "error", err, "url", list.TypeLink())
	}
	return err
}

func (c *client) Delete(ctx context.Context, obj rest.Object) error {
	logger := log2.GetLogger(ctx, log)
	err := c.Client.Delete(ctx, obj, nil)
	if nil != err {
		logger.Info("rest delete error", "error", err, "url", obj.SelfLink())
	}
	return err
}

// NewGateWayClient creates a new gateway client
func NewGateWayClient() (Client, error) {
	config, err := config.GetHTTPSConfig(GatewayConfig.Server.String(), nil)
	if nil != err {
		return nil, err
	}
	c, err := rest.NewForConfig(config)
	if nil != err {
		return nil, err
	}
	return &client{Client: c}, nil
}

// NewGateWayClient creates a new gateway client
func NewGateWayClientForTest() (Client, error) {
	config, err := config.GetHTTPSConfig(GatewayConfig.ServerForTest.String(), nil)
	if nil != err {
		return nil, err
	}
	c, err := rest.NewForConfig(config)
	if nil != err {
		return nil, err
	}
	return &client{Client: c}, nil
}
