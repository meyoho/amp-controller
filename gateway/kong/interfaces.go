package kong

import (
	"context"

	"github.com/alauda/kube-rest/pkg/rest"
	"github.com/alauda/kube-rest/pkg/types"
)

// error will be returned when status code < 200 or > 206
// Reader knows how to read and list Kong rest.Objects.
type Reader interface {
	// Get retrieves an obj for the given rest.Object key from the Kong Gateway.
	// obj must be a struct pointer so that obj can be updated with the response
	// returned by the Server.
	Get(ctx context.Context, obj rest.Object) error

	// List retrieves list of rest.Objects for a given namespace and list options. On a
	// successful call, Items field in the list will be populated with the
	// result returned from the server.
	List(ctx context.Context, list rest.ObjectList, option types.Option) error
}

// Writer knows how to create, delete, and update Kong rest.Objects.
type Writer interface {
	// Create saves the rest.Object obj in the Kong Gateway.
	Create(ctx context.Context, obj rest.Object) error

	// Delete deletes the given obj from Kong Gateway.
	Delete(ctx context.Context, obj rest.Object) error

	// Update updates the given obj in the Kong Gateway. obj must be a
	// struct pointer so that obj can be updated with the content returned by the Server.
	CreateOrUpdate(ctx context.Context, obj rest.Object) error
}

// Client is the interface for gateway
type Client interface {
	Reader
	Writer
}
