package gateway

import (
	"crypto"
	cryptorand "crypto/rand"
	"crypto/rsa"
	"crypto/x509"
	"encoding/pem"
	"errors"
	"flag"
	"net/url"
	"strings"

	"github.com/dgrijalva/jwt-go"
	logf "sigs.k8s.io/controller-runtime/pkg/runtime/log"

	"alauda.io/amp-controller/gateway/kong"
)

var log = logf.Log.WithName("gateway")

const (
	PluginSynced      = "amp.alauda.io/plugin-synced"
	PluginSyncedFalse = "false"
	PluginSyncedTrue  = "true"

	AnnotationRequestTerminationID = "amp.alauda.io/request-termination-id"
	AnnotationACLID                = "amp.alauda.io/acl-id"
	WhiteListPrefix                = "white-group-"
	RoutePrefix                    = "amp.alauda.io/route-prefix"

	// PublicKeyBlockType is a possible value for pem.Block.Type.
	PublicKeyBlockType = "PUBLIC KEY"
	rsaKeySize         = 2048

	OAuth2Endpoint = "/oauth2/token"
)

// EncodePublicKeyPEM returns PEM-encoded public data
func EncodePublicKeyPEM(key crypto.PublicKey) ([]byte, error) {
	der, err := x509.MarshalPKIXPublicKey(key)
	if err != nil {
		return []byte{}, err
	}
	block := pem.Block{
		Type:  PublicKeyBlockType,
		Bytes: der,
	}
	return pem.EncodeToMemory(&block), nil
}

// NewPrivateKey creates an RSA private key
func NewPrivateKey() (crypto.Signer, error) {
	return rsa.GenerateKey(cryptorand.Reader, rsaKeySize)
}

// NewBearerToken creates an RSA private key
func NewBearerToken(key crypto.Signer, iss string) (string, error) {
	token := jwt.New(jwt.GetSigningMethod("RS256"))
	token.Claims = &jwt.StandardClaims{Issuer: iss}
	return token.SignedString(key)
}

// KongConfig kong config
type GatewayFlags struct {
	ProxySecureServer string
	ProxyServer       string
	Server            string
	ServerForTest     string
	Burst             int
	QPS               float32
	DefautListSize    int64
	StandBy           bool
}

func (f *GatewayFlags) ParseGatewayConfig() error {
	kong.GatewayConfig.StandBy = f.StandBy
	if f.StandBy {
		return nil
	}
	var err error
	if "" == f.ServerForTest {
		return errors.New("gateway-admin-server-test is empty")
	}
	log.Info("server", "adminServer", f.Server)
	if "" == f.Server {
		return errors.New("gateway-admin-server is empty")
	}
	if "" == f.ProxyServer {
		return errors.New("gateway-proxy-server is empty")
	}
	if "" == f.ProxySecureServer {
		return errors.New("gateway-proxy-secure-server is empty")
	}
	if kong.GatewayConfig.Server, err = url.Parse(f.Server); nil != err {
		return err
	}
	if kong.GatewayConfig.ServerForTest, err = url.Parse(f.ServerForTest); nil != err {
		return err
	}
	if kong.GatewayConfig.ProxySecureServer, err = url.Parse(f.ProxySecureServer); nil != err {
		return err
	}
	if kong.GatewayConfig.ProxyServer, err = url.Parse(f.ProxyServer); nil != err {
		return err
	}
	kong.GatewayConfig.DefautListSize = f.DefautListSize
	return nil
}

// AddGatewayFlags parse the flags
func AddGatewayFlags(f *GatewayFlags) {
	flag.StringVar(&f.Server, "gateway-admin-server", "", "kong gateway admin host ip address")
	flag.StringVar(&f.ProxySecureServer, "gateway-proxy-secure-server", "", "kong gateway secure host ip address")
	flag.StringVar(&f.ProxyServer, "gateway-proxy-server", "", "kong gateway host ip address")
	flag.Int64Var(&f.DefautListSize, "default-list-size", 1000, "kong gateway default size for list")
	flag.BoolVar(&f.StandBy, "gateway-standby", false, "kong gateway standby mode")
	flag.StringVar(&f.ServerForTest, "gateway-admin-server-test", "", "kong gateway admin host ip address for test")
}

func IsBatchOrder(orderName string) bool {
	return strings.HasPrefix(orderName, "batch-")
}
