package parser

import "alauda.io/amp-controller/gateway"

var _ Parser = &kongParser{}

type kongParser struct{}

func (p *kongParser) ParseJWTCredential() *gateway.JWTCredential {
	return nil
}

func (p *kongParser) ParseOAuth2Credential() (*gateway.OAuth2Credential, *gateway.Route) {
	return nil, nil
}

func (p *kongParser) ParseBasicAuthCredential() *gateway.BasicCredential {
	return nil
}

func (p *kongParser) ParseACL() *gateway.ACL {
	return nil
}
