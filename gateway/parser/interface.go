package parser

import (
	"alauda.io/amp-controller/gateway"
)

type KongParser interface {
	ParseJWTCredential() *gateway.JWTCredential
	ParseOAuth2Credential() (*gateway.OAuth2Credential, *gateway.Route)
	ParseBasicAuthCredential() *gateway.BasicCredential
	ParseACL() *gateway.ACL
}

type Parser interface {
	KongParser
}
