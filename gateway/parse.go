package gateway

import (
	"fmt"
	"path"

	"github.com/google/uuid"
	"github.com/juju/errors"
	"github.com/spf13/cast"

	ampv1 "alauda.io/amp-controller/api/v1"
)

// KongParser parse API Product object to Kong object
type KongParser interface {
	ParseService(name, project, owner string, service *ampv1.ApiService) *Service
	ParseRoute(apiPath *ampv1.ApiPath, routeBasePath string, protocols []string, service *Service) *Route
	ParseStrategyPlugin(p *ampv1.ApiStrategy, s *Service, r *Route) *Plugin
	ParseAnnotationPlugin(pID, pType string, enabled bool, s *Service) (*Plugin, error)
}

// Parser parse amp and kong objects
type Parser interface {
	KongParser
}

type parser struct{}

func (p *parser) ParseService(name, project, owner string, service *ampv1.ApiService) *Service {
	s := &Service{
		ID:   service.Id,
		URL:  service.EndPoint,
		Name: &name,
		Tags: NewTags(name, project, owner),
	}
	return s
}

func (p *parser) ParseRoute(apiPath *ampv1.ApiPath, routeBasePath string, protocols []string, service *Service) *Route {
	if nil == service {
		return &Route{ID: apiPath.Id}
	}
	if 0 == len(protocols) {
		protocols = []string{"http", "https"}
	}
	route := &Route{
		ID:        apiPath.Id,
		Methods:   apiPath.HttpMethod,
		Paths:     []string{path.Join("/", routeBasePath, apiPath.Path)},
		Protocols: protocols,
		Service:   Service{ID: service.ID},
	}

	return route
}

func (p *parser) ParseStrategyPlugin(strategy *ampv1.ApiStrategy, service *Service, route *Route) *Plugin {
	// get jwt, oauth2, rate-limiting, ip-restriction from api service
	if nil == strategy {
		return nil
	}
	if _, err := uuid.Parse(strategy.Id); nil != err {
		return nil
	}
	if nil == service && nil == route {
		// for deletion
		return &Plugin{
			ID:   strategy.Id,
			Name: ampv1.PluginType(strategy.Type),
		}
	}
	var svcClone *Service
	if nil != service {
		svcClone = &Service{
			ID: service.ID,
		}
	}
	var rtClone *Route
	if nil != route {
		rtClone = &Route{
			ID: route.ID,
		}
	}
	switch strategy.Type {
	case ampv1.JWT:
		strategy = &ampv1.ApiStrategy{
			Id:   strategy.Id,
			Type: strategy.Type,
		}
	case ampv1.BASIC:
		strategy = &ampv1.ApiStrategy{
			Id:   strategy.Id,
			Type: "basic-auth",
		}
	case ampv1.OAuth2:
		strategy.Config = ampv1.PluginConfig{
			"enable_client_credentials": true,
			"token_expiration":          0,
		}
	case ampv1.RateLimiting:
		// Add default rate-limiting policy to local
		if nil != strategy.Config && len(strategy.Config) != 0 {
			strategy.Config["policy"] = "local"
		}
	case ampv1.CORS:
	case ampv1.RequestSizeLimiting:
		config := getRequestSizeLimitingConfig(strategy.Config)
		strategy.Config = config
	case ampv1.Prometheus:
		strategy.Config = nil
	case ampv1.IPRestriction:
		config := getIPRestrictionConfig(strategy.Config)
		if nil == config {
			return nil
		}
		strategy.Config = config
	default:
		return nil
	}
	plugin := &Plugin{
		ID:      strategy.Id,
		Name:    ampv1.PluginType(strategy.Type),
		Service: svcClone,
		Route:   rtClone,
		Enabled: true,
		Config:  strategy.Config,
	}
	return plugin
}

func (p *parser) ParseAnnotationPlugin(pID, pType string, enabled bool, s *Service) (*Plugin, error) {
	if nil == s {
		return nil, errors.New("ParseAnnotationPlugin: nil servcie")
	}
	if _, err := uuid.Parse(pID); nil != err {
		return nil, errors.Annotate(err, "ParseAnnotationPlugin")
	}
	switch pType {
	case string(ampv1.RequestTermination):
		return newRequestTerminationPlugin(*s, pID, enabled), nil
	case string(ampv1.ACL):
		return newACLPlugin(*s, pID, enabled), nil
	}
	return nil, nil
}

// NewParser ...
func NewParser() Parser {
	return &parser{}
}

func newRequestTerminationPlugin(service Service, requestTerminationID string, enabled bool) *Plugin {
	requestTerminationPlugin := Plugin{
		ID:      requestTerminationID,
		Name:    ampv1.RequestTermination,
		Service: &Service{ID: service.ID},
		Route:   nil,
		Enabled: enabled,
		Config: ampv1.PluginConfig{
			"status_code": 403,
			"message":     "request terminated",
		},
	}
	return &requestTerminationPlugin
}

func newACLPlugin(service Service, ACLID string, enabled bool) *Plugin {
	group := fmt.Sprintf("%s-%s", WhiteListPrefix, service.ID)
	requestTerminationPlugin := Plugin{
		ID:      ACLID,
		Name:    ampv1.ACL,
		Service: &Service{ID: service.ID},
		Route:   nil,
		Enabled: enabled,
		Config: ampv1.PluginConfig{
			"whitelist": []string{group},
		},
	}
	return &requestTerminationPlugin
}

func getIPRestrictionConfig(config ampv1.PluginConfig) ampv1.PluginConfig {
	if nil == config {
		return nil
	}
	var hasBlack, hasWhite, ok bool
	var b, w interface{}
	var bList, wList []interface{}
	w, hasWhite = config[ampv1.IPRestrictionWhiteList]
	b, hasBlack = config[ampv1.IPRestrictionBlackList]
	if hasBlack && nil != b {
		if bList, ok = b.([]interface{}); !ok {
			return nil
		}
		if len(bList) > 0 {
			config[ampv1.IPRestrictionWhiteList] = nil
			return config
		}
	}
	if !hasWhite || nil == w {
		return nil
	}
	if wList, ok = w.([]interface{}); !ok {
		return nil
	}
	if len(wList) == 0 {
		return nil
	}
	config[ampv1.IPRestrictionBlackList] = nil
	return config
}

func getRequestSizeLimitingConfig(config ampv1.PluginConfig) ampv1.PluginConfig {
	// if allowed_payload_size is not valid, use the default 128 MB
	var s int64
	var err error
	if nil == config {
		return nil
	}
	size, ok := config["allowed_payload_size"]
	if !ok {
		return nil
	}
	// use int64 to prevent json codec errors
	if s, err = cast.ToInt64E(size); err != nil {
		return nil
	}
	return ampv1.PluginConfig{"allowed_payload_size": s}
}
