package gateway

import (
	"encoding/json"
	"errors"
	"fmt"
	"path"
	"reflect"
	"strings"

	"github.com/alauda/kube-rest/pkg/rest"

	v1 "alauda.io/amp-controller/api/v1"
)

// Consumer represents a Consumer in Kong.
// Read https://getkong.org/docs/0.13.x/admin-api/#consumer-object
// +k8s:deepcopy-gen=true
type Consumer struct {
	Username string   `json:"username"`
	ID       string   `json:"id,omitempty"`
	Tags     []string `json:"tags,omitempty"`
}

// TypeLink ...
func (c *Consumer) TypeLink(segments ...string) string {
	return path.Join("/consumers", "/", path.Join(segments...))
}

// SelfLink ...
func (c *Consumer) SelfLink(segments ...string) string {
	return path.Join("/consumers", "/", c.Username, path.Join(segments...))
}

// Data ...
func (c *Consumer) Data() ([]byte, error) {
	return json.Marshal(c)
}

// Parse ...
func (c *Consumer) Parse(bt []byte) error {
	clone := new(Consumer)
	if err := json.Unmarshal(bt, clone); nil != err {
		return err
	}
	*c = *clone
	return nil
}

type tags []string

func (st tags) Dump() (string, string, string, error) {
	if 3 < len(st) {
		return "", "", "", errors.New("invalid tags")
	}
	return st[0], st[1], st[2], nil
}

func (st tags) Name() (string, error) {
	if 3 < len(st) {
		return "", errors.New("invalid tags")
	}
	return st[0], nil
}

func (st tags) Project() (string, error) {
	if 3 < len(st) {
		return "", errors.New("invalid tags")
	}
	return st[1], nil
}

func (st tags) Owner() (string, error) {
	if 3 < len(st) {
		return "", errors.New("invalid tags")
	}
	return st[2], nil
}

func NewTags(name, project, owner string) tags {
	return tags{name, project, owner}
}

// Service represents a Service in Kong.
// Read https://getkong.org/docs/0.13.x/admin-api/#Service-object
// +k8s:deepcopy-gen=true
type Service struct {
	ID   string  `json:"id,omitempty"`
	URL  string  `json:"url,omitempty"`
	Name *string `json:"name,omitempty"`
	Tags tags    `json:"tags,omitempty"`
}

// TypeLink ...
func (c *Service) TypeLink(segments ...string) string {
	return "/services"
}

// SelfLink ...
func (c *Service) SelfLink(segments ...string) string {
	var subPath string
	if nil != c.Name && "" != *c.Name {
		subPath = *c.Name
	}
	if "" == subPath {
		subPath = c.ID
	}
	return path.Join("/services", "/", subPath)
}

// Data ...
func (c *Service) Data() ([]byte, error) {
	return json.Marshal(c)
}

// Parse ...
func (c *Service) Parse(bt []byte) error {
	clone := new(Service)
	if err := json.Unmarshal(bt, clone); nil != err {
		return err
	}
	*c = *clone
	return nil
}

// Route represents a Route in Kong.
// Read https://getkong.org/docs/0.13.x/admin-api/#Route-object
// +k8s:deepcopy-gen=true
type Route struct {
	ID        string   `json:"id,omitempty"`
	Name      string   `json:"name,omitempty"`
	Protocols []string `json:"protocols,omitempty"`
	Methods   []string `json:"methods,omitempty"`
	Paths     []string `json:"paths"`
	Service   Service  `json:"service,omitempty"`
}

// TypeLink ...
func (r *Route) TypeLink(segments ...string) string {
	return "/routes"
}

// SelfLink ...
func (r *Route) SelfLink(segments ...string) string {
	subPath := r.ID
	if "" == subPath {
		subPath = r.Name
	}
	return path.Join("/routes", "/", subPath)
}

// Data ...
func (r *Route) Data() ([]byte, error) {
	return json.Marshal(r)
}

// Parse ...
func (r *Route) Parse(bt []byte) error {
	clone := new(Route)
	if err := json.Unmarshal(bt, clone); nil != err {
		return err
	}
	*r = *clone
	return nil
}

type credentialTags []string

func NewCredentialTags(product, consumer, order string) credentialTags {
	return credentialTags{product, consumer, order}
}

func (c credentialTags) APIProduct() (string, error) {
	if len(c) < 3 {
		return "", errors.New("invalid tags")
	}
	return c[0], nil
}

func (c credentialTags) Consumer() (string, error) {
	if len(c) < 3 {
		return "", errors.New("invalid tags")
	}
	return c[1], nil
}

func (c credentialTags) Order() (string, error) {
	if len(c) < 3 {
		return "", errors.New("invalid tags")
	}
	return c[2], nil
}

// OAuth2Credential represents a OAuth2 credential in Kong.
// +k8s:deepcopy-gen=true
type OAuth2Credential struct {
	ID           string         `json:"id,omitempty"`
	Name         string         `json:"name"`
	ClientID     string         `json:"client_id,omitempty"`
	ClientSecret string         `json:"client_secret,omitempty"`
	RedirectUris []string       `json:"redirect_uris"`
	Tags         credentialTags `json:"tags,omitempty"`
	Consumer     Consumer       `json:"-"`
}

// TypeLink ...
func (r *OAuth2Credential) TypeLink(segments ...string) string {
	return path.Join("/consumers", r.Consumer.Username, "oauth2")
}

// SelfLink ...
func (r *OAuth2Credential) SelfLink(segments ...string) string {
	return path.Join("/consumers", r.Consumer.Username, "oauth2", r.ID)
}

// Data ...
func (r *OAuth2Credential) Data() ([]byte, error) {
	return json.Marshal(r)
}

// Parse ...
func (r *OAuth2Credential) Parse(bt []byte) error {
	clone := new(OAuth2Credential)
	if err := json.Unmarshal(bt, clone); nil != err {
		return err
	}
	*r = *clone
	return nil
}

type Algorithm string

const (
	AlgorithmHS256 Algorithm = "HS256"
	AlgorithmRS256 Algorithm = "RS256"
	AlgorithmES256 Algorithm = "ES256"
)

// JWTCredential represents a JWT credential in Kong.
// +k8s:deepcopy-gen=true
type JWTCredential struct {
	ID           string         `json:"id,omitempty"`
	Key          string         `json:"key,omitempty"`
	Secret       string         `json:"secret,omitempty"`
	Algorithm    Algorithm      `json:"algorithm,omitempty"`
	RSAPublicKey string         `json:"rsa_public_key,omitempty"`
	Tags         credentialTags `json:"tags,omitempty"`
	Consumer     Consumer       `json:"-"`
}

// TypeLink ...
func (j *JWTCredential) TypeLink(segments ...string) string {
	return path.Join("/consumers", j.Consumer.Username, "jwt")
}

// SelfLink ...
func (j *JWTCredential) SelfLink(segments ...string) string {
	return path.Join("/consumers", j.Consumer.Username, "jwt", j.ID)
}

// Data ...
func (j *JWTCredential) Data() ([]byte, error) {
	return json.Marshal(j)
}

// Parse ...
func (j *JWTCredential) Parse(bt []byte) error {
	clone := new(JWTCredential)
	if err := json.Unmarshal(bt, clone); nil != err {
		return err
	}
	*j = *clone
	return nil
}

// BasicCredential represents a basic credential in Kong.
// +k8s:deepcopy-gen=true
type BasicCredential struct {
	ID       string         `json:"id,omitempty"`
	Username string         `json:"username,omitempty"`
	Password string         `json:"password,omitempty"`
	Tags     credentialTags `json:"tags,omitempty"`
	Consumer Consumer       `json:"-"`
}

// TypeLink ...
func (b *BasicCredential) TypeLink(segments ...string) string {
	return path.Join("/consumers", b.Consumer.Username, "basic-auth")
}

// SelfLink ...
func (b *BasicCredential) SelfLink(segments ...string) string {
	return path.Join("/consumers", b.Consumer.Username, "basic-auth", b.ID)
}

// Data ...
func (b *BasicCredential) Data() ([]byte, error) {
	return json.Marshal(b)
}

// Parse ...
func (b *BasicCredential) Parse(bt []byte) error {
	clone := new(BasicCredential)
	if err := json.Unmarshal(bt, clone); nil != err {
		return err
	}
	*b = *clone
	return nil
}

// ACL represents a ACL in Kong.
// +k8s:deepcopy-gen=true
type ACL struct {
	ID       string   `json:"id,omitempty"`
	Group    string   `json:"group"`
	Consumer Consumer `json:"-"`
}

// TypeLink ...
func (r *ACL) TypeLink(segments ...string) string {
	return path.Join("/consumers", r.Consumer.Username, "acls")
}

// SelfLink ...
func (r *ACL) SelfLink(segments ...string) string {
	return path.Join("/consumers", r.Consumer.Username, "acls", r.ID)
}

// Data ...
func (r *ACL) Data() ([]byte, error) {
	return json.Marshal(r)
}

// Parse ...
func (r *ACL) Parse(bt []byte) error {
	clone := new(ACL)
	if err := json.Unmarshal(bt, clone); nil != err {
		return err
	}
	*r = *clone
	return nil
}

// Plugin represents a Plugin in Kong.
// Read https://getkong.org/docs/0.13.x/admin-api/#Plugin-object
// +k8s:deepcopy-gen=true
type Plugin struct {
	ID       string          `json:"id,omitempty"`
	Name     v1.PluginType   `json:"name"`
	Service  *Service        `json:"service,omitempty"`
	Route    *Route          `json:"route,omitempty"`
	Enabled  bool            `json:"enabled"`
	Config   v1.PluginConfig `json:"config,omitempty"`
	Consumer *Consumer       `json:"consumer,omitempty"`
}

// TypeLink ...
func (r *Plugin) TypeLink(segments ...string) string {
	return path.Join("/plugins")
}

// SelfLink ...
func (r *Plugin) SelfLink(segments ...string) string {
	return path.Join("/plugins", "/", r.ID)
}

// Data ...
func (r *Plugin) Data() ([]byte, error) {
	return json.Marshal(r)
}

// Parse ...
func (r *Plugin) Parse(bt []byte) error {
	clone := new(Plugin)
	if err := json.Unmarshal(bt, clone); nil != err {
		return err
	}
	*r = *clone
	return nil
}

const validationFomular = "/schemas/%ss/validate"

type validationObject struct {
	object rest.Object
}

func (v *validationObject) ObjectType() string {
	objType := reflect.TypeOf(v.object).String()
	ss := strings.Split(objType, ".")
	return strings.ToLower(ss[len(ss)-1])
}

func (v *validationObject) TypeLink(segments ...string) string {
	return fmt.Sprintf(validationFomular, v.ObjectType())
}

func (v *validationObject) SelfLink(segments ...string) string {
	return fmt.Sprintf(validationFomular, v.ObjectType())
}

func (v *validationObject) Data() ([]byte, error) {
	return v.object.Data()
}

func (v *validationObject) Parse(bt []byte) error {
	return v.object.Parse(bt)
}

// PluginList represents a list of Plugin in Kong.
// Read https://getkong.org/docs/0.13.x/admin-api/#Plugin-object
// +k8s:deepcopy-gen=true
type PluginList struct {
	Data     []Plugin  `json:"data,omitempty"`
	Next     *string   `json:"next,omitempty"`
	Service  *Service  `json:"-"`
	Route    *Route    `json:"-"`
	Consumer *Consumer `json:"-"`
}

// TypeLink ...
func (r *PluginList) TypeLink() string {
	if nil != r.Service {
		return path.Join("/services", r.Service.ID, "/plugins")
	}
	if nil != r.Route {
		return path.Join("/routes", r.Route.ID, "/plugins")
	}
	if nil != r.Consumer {
		return path.Join("/consumer", r.Consumer.ID, "/plugins")
	}
	return path.Join("/plugins")
}

// Parse ...
func (r *PluginList) Parse(bt []byte) error {
	clone := new(PluginList)
	if err := json.Unmarshal(bt, clone); nil != err {
		return err
	}
	*r = *clone
	return nil
}

// Route represents a Route in Kong.
// Read https://getkong.org/docs/0.13.x/admin-api/#Route-object
// +k8s:deepcopy-gen=true
type RouteList struct {
	Data    []Route  `json:"data,omitempty"`
	Next    *string  `json:"next,omitempty"`
	Service *Service `json:"-"`
}

// TypeLink ...
func (r *RouteList) TypeLink() string {
	if nil != r.Service {
		return path.Join("/services", r.Service.ID, "/routes")
	}
	return path.Join("/routes")
}

// Parse ...
func (r *RouteList) Parse(bt []byte) error {
	clone := new(RouteList)
	if err := json.Unmarshal(bt, clone); nil != err {
		return err
	}
	*r = *clone
	return nil
}

// NewValidationObject generate a new obj for kong schema validation
// obj must be a struct pointer so that obj can be updated with the response
// returned by the Server.

func NewValidationObject(obj rest.Object) rest.Object {
	return &validationObject{object: obj}
}
