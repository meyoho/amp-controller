package sync

import (
	"context"

	"alauda.io/amp-controller/gateway"
	"alauda.io/amp-controller/gateway/parser"

	"github.com/go-logr/logr"

	ampv1 "alauda.io/amp-controller/api/v1"
)

type kongState struct {
	acl *gateway.ACL
}

type leaseSyncer struct {
	lease    *ampv1.Lease
	product  *ampv1.ApiProduct
	consumer *ampv1.ApiConsumeApplication
	parser   parser.KongParser
	context  context.Context
	Log      logr.Logger
}

// Do add consumer to acl; delete consumer from acl
func (ls *leaseSyncer) Do() error {
	return nil
}

func NewLeaseSyncer() Syncer {
	return nil
}
