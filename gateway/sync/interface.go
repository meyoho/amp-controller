package sync

type Syncer interface {
	Do() error
}
