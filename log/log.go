package log

import (
	"context"
	"fmt"

	"alauda.io/amp-controller/common"

	"github.com/go-logr/logr"
	"github.com/google/uuid"
	logf "sigs.k8s.io/controller-runtime/pkg/runtime/log"
)

// WithSessionID ...
func WithSessionID(logger logr.Logger, sessionID *string) logr.Logger {
	if nil == sessionID {
		id := uuid.New().String()
		sessionID = &id
	}
	if nil == logger {
		return logf.Log.WithValues(common.RequestID, *sessionID)
	}
	return logger.WithValues(common.RequestID, *sessionID)
}

// GetLogger ...
func GetLogger(ctx context.Context, logger logr.Logger) logr.Logger {
	var reqID string
	if nil != ctx {
		id := ctx.Value(common.RequestID)
		if nil == id {
			return logger
		}
		reqID = fmt.Sprintf("%s", id)
	}
	return WithSessionID(logger, &reqID)

}
