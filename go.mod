module alauda.io/amp-controller

go 1.12

require (
	github.com/alauda/kube-rest v0.1.4
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/fortytw2/leaktest v1.3.0 // indirect
	github.com/go-logr/logr v0.1.0
	github.com/google/uuid v1.1.1
	github.com/juju/errors v0.0.0-20190930114154-d42613fe1ab9
	github.com/juju/loggo v0.0.0-20190526231331-6e530bcce5d8 // indirect
	github.com/juju/testing v0.0.0-20191001232224-ce9dec17d28b // indirect
	github.com/olivere/elastic v6.2.26+incompatible
	github.com/onsi/ginkgo v1.10.1
	github.com/onsi/gomega v1.7.0
	github.com/sethvargo/go-password v0.1.3
	github.com/sony/sonyflake v1.0.0
	github.com/spf13/cast v1.3.0
	gopkg.in/mgo.v2 v2.0.0-20190816093944-a6b53ec6cb22 // indirect
	k8s.io/api v0.17.0 // indirect
	k8s.io/apiextensions-apiserver v0.0.0-20191117020858-b615a37f53e7 // indirect
	k8s.io/apimachinery v0.17.0
	k8s.io/client-go v0.0.0-20191115215802-0a8a1d7b7fae
	k8s.io/klog v1.0.0
	sigs.k8s.io/controller-runtime v0.3.1-0.20191029211253-40070e2a1958
)
