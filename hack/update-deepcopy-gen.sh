#!/bin/bash

$GOPATH/src/k8s.io/code-generator/generate-groups.sh deepcopy \
	alauda.io/amp-controller/gateway \
	alauda.io \
	amp-controller:gateway \
    --go-header-file hack/boilerplate.go.txt

