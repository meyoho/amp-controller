#!/bin/bash

KUBECONFIG="${HOME}/.kube/config"

./bin/manager -kubeconfig="${KUBECONFIG}" \
	-gateway-admin-server=http://localhost:8001 \
	-gateway-admin-server-test=http://localhost:8001 \
	-gateway-proxy-server=http://localhost:8000  \
	-gateway-proxy-secure-server=https://localhost:8443 \
	-metering-standby=true \
	-debug
