#!/bin/bash

KUBECONFIG="${HOME}/.kube/config"

./bin/manager -kubeconfig="${KUBECONFIG}" \
	-metering-standby=false \
	-gateway-standby=true \
	-elasticsearch-url=http://localhost:9200 \
	-elasticsearch-user= \
	-elasticsearch-password= \
	-lease-concurrence=2 \
	-metrics-addr=:8081 \
	-metering-ratio=30 \
	-debug
