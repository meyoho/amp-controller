/*

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package controllers

import (
	"context"
	"flag"
	"fmt"

	"alauda.io/amp-controller/common"

	"k8s.io/apimachinery/pkg/types"

	"sigs.k8s.io/controller-runtime/pkg/controller"

	"alauda.io/amp-controller/gateway/kong"

	"github.com/go-logr/logr"
	"k8s.io/apimachinery/pkg/api/errors"
	ctrl "sigs.k8s.io/controller-runtime"
	"sigs.k8s.io/controller-runtime/pkg/client"

	ampv1 "alauda.io/amp-controller/api/v1"
	"alauda.io/amp-controller/gateway"
	logC "alauda.io/amp-controller/log"
)

const APIConsumeApplicationFinalizers = "apiconsumeapplication.finalizers.amp.alauda.io"

// ApiConsumeApplicationReconciler reconciles a ApiConsumeApplication object
type ApiConsumeApplicationReconciler struct {
	client.Client
	Log logr.Logger
}

// +kubebuilder:rbac:groups=amp.alauda.io,resources=apiconsumeapplications,verbs=get;list;watch;create;update;patch;delete
// +kubebuilder:rbac:groups=amp.alauda.io,resources=apiconsumeapplications/status,verbs=get;update;patch

func (r *ApiConsumeApplicationReconciler) Reconcile(request ctrl.Request) (ctrl.Result, error) {
	// Fetch the ApiConsumeApplication instance
	// _ = context.Background()
	logger := r.Log.WithValues("apiconsumeapplication", request.NamespacedName)
	logger = logC.WithSessionID(logger, nil)

	logger.Info("api consume application reconcile", "request", request)
	if kong.GatewayConfig.StandBy {
		//logger.Info("standby mode, ignore")
		return ctrl.Result{}, nil
	}

	errorHandler := ErrorHandler{Log: logger}
	instance := &ampv1.ApiConsumeApplication{}
	err := r.Get(context.TODO(), request.NamespacedName, instance)
	if err != nil {
		if errors.IsNotFound(err) {
			// Object not found, return.  Created objects are automatically garbage collected.
			// For additional cleanup logic use finalizers.

			// TODO: add cleanup finalizers
			return ctrl.Result{}, nil
		}
		// Error reading the object - requeue the request.
		return ctrl.Result{}, err
	}

	if instance.ObjectMeta.DeletionTimestamp.IsZero() {
		// The object is not being deleted, so if it does not have our finalizer,
		// then lets add the finalizer and update the object. This is equivalent
		// registering our finalizer.
		if !containsString(instance.ObjectMeta.Finalizers, APIConsumeApplicationFinalizers) {
			instance.ObjectMeta.Finalizers = append(instance.ObjectMeta.Finalizers, APIConsumeApplicationFinalizers)
			if err := r.Update(context.Background(), instance); err != nil {
				if errNew := r.updateConsumeApplicationStatus(context.TODO(), instance, logger, err); errNew != nil {
					logger.Info(fmt.Sprintf("update ConsumeApplication  status error, err=%s", errNew.Error()))
				}
				return errorHandler.ReconcileError(err, common.ReconcileDefaultDelay)
			}
		}
	} else {
		// The object is being deleted
		if containsString(instance.ObjectMeta.Finalizers, APIConsumeApplicationFinalizers) {
			// delete consumer from kong
			if err := gateway.SyncGatewayConsumer(instance); nil != err {
				logger.Error(err, "error delete gateway consumer", "consumer", instance)
				if errNew := r.updateConsumeApplicationStatus(context.TODO(), instance, logger, err); errNew != nil {
					logger.Info(fmt.Sprintf("update ConsumeApplication  status error, err=%s", errNew.Error()))
				}
				return errorHandler.ReconcileError(err, common.ReconcileDefaultDelay)
			}
			// remove our finalizer from the list and update it.
			instance.ObjectMeta.Finalizers = removeString(instance.ObjectMeta.Finalizers, APIConsumeApplicationFinalizers)
			if err := r.Update(context.Background(), instance); err != nil {
				if errNew := r.updateConsumeApplicationStatus(context.TODO(), instance, logger, err); errNew != nil {
					logger.Info(fmt.Sprintf("update ConsumeApplication  status error, err=%s", errNew.Error()))
				}
				return errorHandler.ReconcileError(err, common.ReconcileDefaultDelay)
			}
		}
		return ctrl.Result{}, err
	}

	annotations := instance.GetAnnotations()
	if nil == annotations {
		annotations = make(map[string]string, 1)
	}

	if ampv1.PluginSyncedTrue != annotations[ampv1.PluginSynced] {
		// sync consumer to kong
		if err := gateway.SyncGatewayConsumer(instance); nil != err {
			logger.Error(err, "error sync gateway consumer", "consumer", instance)
			return errorHandler.ReconcileError(err, common.ReconcileDefaultDelay)
		}
		if err = r.generateCertificationInfo(instance, logger); err != nil {
			logger.Info(fmt.Sprintf("generate consumer certification info acl error err=%s", err.Error()))
			if errNew := r.updateConsumeApplicationStatus(context.TODO(), instance, logger, err); errNew != nil {
				logger.Info(fmt.Sprintf("update ConsumeApplication  status error, err=%s", errNew.Error()))
			}
			return errorHandler.ReconcileError(err, common.ReconcileDefaultDelay)
		}
		annotations[ampv1.PluginSynced] = ampv1.PluginSyncedTrue
		instance.SetAnnotations(annotations)
		r.Update(context.TODO(), instance)
		return ctrl.Result{}, nil
	}

	if synced, ok := annotations[ampv1.ConsumerCertificationSynced]; ok {
		product := &ampv1.ApiProduct{}
		if productName, ok := annotations[ampv1.ConsumerSelfProductName]; ok && productName != "" {
			productKey := types.NamespacedName{Name: productName}
			if err := r.Get(context.TODO(), productKey, product); nil != err {
				logger.Info(fmt.Sprintf("get consumer self product error err=%s", err.Error()))
				if errNew := r.updateConsumeApplicationStatus(context.TODO(), instance, logger, err); errNew != nil {
					logger.Info(fmt.Sprintf("update ConsumeApplication  status error, err=%s", errNew.Error()))
				}
				return errorHandler.ReconcileError(err, common.ReconcileDefaultDelay)
			}
		}
		if synced == ampv1.ConsumerCertificationSyncedTrue {
			_, err := gateway.GetCurrentACLBindingNew(instance.GetName(), product, logger)
			if nil != err && !errors.IsNotFound(err) {
				logger.Info(fmt.Sprintf("get consumer acl error err=%s", err.Error()))
				if errNew := r.updateConsumeApplicationStatus(context.TODO(), instance, logger, err); errNew != nil {
					logger.Info(fmt.Sprintf("update ConsumeApplication  status error, err=%s", errNew.Error()))
				}
				return errorHandler.ReconcileError(err, common.ReconcileDefaultDelay)
			}

			if errors.IsNotFound(err) {
				if err = gateway.SyncGatewayACLBindingNew(instance.GetName(), product, logger); err != nil {
					logger.Info(fmt.Sprintf("create consumer acl error err=%s", err.Error()))
					if errNew := r.updateConsumeApplicationStatus(context.TODO(), instance, logger, err); errNew != nil {
						logger.Info(fmt.Sprintf("update ConsumeApplication  status error, err=%s", errNew.Error()))
					}
					return errorHandler.ReconcileError(err, common.ReconcileDefaultDelay)
				}
			}

		} else if synced == ampv1.ConsumerCertificationSyncedFalse {
			acl, err := gateway.GetCurrentACLBindingNew(instance.GetName(), product, logger)
			if err != nil && !errors.IsNotFound(err) {
				return errorHandler.ReconcileError(err, common.ReconcileDefaultDelay)
			}
			if acl != nil {
				if err = gateway.SyncDeleteGatewayACLNew(instance.GetName(), product, logger); err != nil {
					logger.Info(fmt.Sprintf("delete consumer acl error err=%s", err.Error()))
					if errNew := r.updateConsumeApplicationStatus(context.TODO(), instance, logger, err); errNew != nil {
						logger.Info(fmt.Sprintf("update ConsumeApplication  status error, err=%s", errNew.Error()))
					}
					return errorHandler.ReconcileError(err, common.ReconcileDefaultDelay)
				}
			}
		}
	}

	return ctrl.Result{}, nil
}

func (r *ApiConsumeApplicationReconciler) generateCertificationInfo(instance *ampv1.ApiConsumeApplication, logger logr.Logger) error {
	if instance.Spec.Info != nil {
		return nil
	}

	typeList := []ampv1.PluginType{
		ampv1.OAuth2,
		ampv1.JWTHs256,
		ampv1.JWTRs256,
		ampv1.BASIC,
	}

	var APInfoList []ampv1.APInfo
	for _, typ := range typeList {
		kvConfig, err := gateway.SyncGatewayBindingNew(instance, typ)
		if nil != err {
			// ignore invalid task, e.g. 404, 409
			return err
		}

		var info ampv1.APInfo
		if nil != kvConfig {
			info = make(ampv1.APInfo, 10)

			for k, v := range kvConfig {
				info[k] = v
			}
		}
		APInfoList = append(APInfoList, info)
	}
	instance.Spec.Info = APInfoList

	return nil
}

func (r *ApiConsumeApplicationReconciler) updateConsumeApplicationStatus(ctx context.Context, instance *ampv1.ApiConsumeApplication,
	logger logr.Logger, err error) error {
	if err != nil {
		message := fmt.Sprintf("error reconcil ConsumeApplication: err=%s", err.Error())
		instance.Status.Message = message
		return r.Status().Update(ctx, instance)
	}
	return nil

}

var APIConsumeApplicationConcurrence int

func init() {
	flag.IntVar(&APIConsumeApplicationConcurrence, "api-consume-application-concurrence", 1, "max concurrent reconciles for api product service")
	//flag.Parse()
}

func (r *ApiConsumeApplicationReconciler) SetupWithManager(mgr ctrl.Manager) error {
	return ctrl.NewControllerManagedBy(mgr).
		For(&ampv1.ApiConsumeApplication{}).
		WithOptions(controller.Options{MaxConcurrentReconciles: APIConsumeApplicationConcurrence}).
		Complete(r)
}
