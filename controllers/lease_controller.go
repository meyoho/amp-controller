/*

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package controllers

import (
	"context"
	"flag"
	"fmt"
	"time"

	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"sigs.k8s.io/controller-runtime/pkg/controller"

	"alauda.io/amp-controller/metering"

	"github.com/google/uuid"
	apiError "k8s.io/apimachinery/pkg/api/errors"
	"k8s.io/apimachinery/pkg/types"

	"alauda.io/amp-controller/common"

	"github.com/go-logr/logr"
	"k8s.io/apimachinery/pkg/runtime"
	ctrl "sigs.k8s.io/controller-runtime"
	"sigs.k8s.io/controller-runtime/pkg/client"

	ampv1 "alauda.io/amp-controller/api/v1"
)

// LeaseReconciler reconciles a Lease object
type LeaseReconciler struct {
	client.Client
	Log    logr.Logger
	Scheme *runtime.Scheme
	//workqueue workqueue.DelayingInterface
}

const (
	FiveSeconds    = time.Second * 5
	FifteenSeconds = time.Second * 15
	OneDay         = time.Hour * 24
	FifteenMinutes = time.Minute * 15
)

// +kubebuilder:rbac:groups=amp.alauda.io,resources=leases,verbs=get;list;watch;create;update;patch;delete
// +kubebuilder:rbac:groups=amp.alauda.io,resources=leases/status,verbs=get;update;patch

// lease controller handles api product service status only, it consume api service subscriptions

func (r *LeaseReconciler) Reconcile(req ctrl.Request) (ctrl.Result, error) {
	eventTime := metav1.NowMicro()
	sessionID := uuid.New().String()
	defaultContext := context.WithValue(context.Background(), common.RequestID, sessionID)
	logger := r.Log.WithValues("request", req).WithValues(common.RequestID, sessionID)
	errorHandler := ErrorHandler{Log: logger}

	logger.Info("lease reconcile")
	if metering.MeteringConfig.StandBy {
		//logger.Info("standby mode, ignore")
		return ctrl.Result{}, nil
	}

	// your logic here
	var err error

	lease := &ampv1.Lease{}

	err = r.Get(defaultContext, req.NamespacedName, lease)
	if nil != err {
		logger.Info("reconcile lease fail: can't get lease", "error", err.Error())
		return errorHandler.ReconcileError(err, common.ReconcileDefaultDelay)
	}

	// get acl from Kong ?

	// expired, get available status

	// get api product service, if not found, delete lease

	var apiProductService *ampv1.ApiProductService

	if apiProductService, err = r.getAPIProductService(defaultContext, lease, logger); nil != err {
		if apiError.IsNotFound(err) {
			logger.Info("reconcile lease fail: apiproductservice not found, delete lease", "apiproductservice", lease.Spec.APIProductSericeName)
			return errorHandler.ReconcileError(r.Delete(defaultContext, lease), common.ReconcileDefaultDelay)
		}
		logger.Info("reconcile lease fail: can't get apiproductservice", "error", err.Error())
		return errorHandler.ReconcileError(err, common.ReconcileDefaultDelay)
	}
	if ampv1.ServiceTerminated == apiProductService.Spec.SubscriptionStatus || ampv1.ServiceTerminated == apiProductService.Status.Phase {
		logger.Info("apiproductservice terminated, delete lease", "apiproductservice", lease.Spec.APIProductSericeName)
		return errorHandler.ReconcileError(r.Delete(defaultContext, lease), common.ReconcileDefaultDelay)
	}

	if lease.GetName() != string(apiProductService.GetUID()) {
		logger.Info("cant start lease: api product service mismatch", "lease", lease.GetName(), "productService", apiProductService.GetUID())
		return ctrl.Result{}, nil
	}

	if apiProductService.SubscriptionPlanLongTime() {
		apiProductService.Status.Condition = nil
		apiProductService.Status.Phase = ampv1.ServiceOpened
		if err := r.updateAPIProductServiceStatus(defaultContext, apiProductService, lease, logger); nil != err {
			return errorHandler.ReconcileError(err, common.ReconcileDefaultDelay)
		}
		logger.Info("successfully reconciled, long time subscription plan")
		return ctrl.Result{}, nil
	}

	operator, err := metering.NewMeteringOperator(apiProductService, lease, eventTime, logger)
	if nil != err {
		logger.Info("reconcile lease fail: can't create operator", "error", err.Error())
		return ctrl.Result{}, nil
	}

	if nil == operator {
		logger.Info("reconcile lease fail: operator is nil, will retry later")
		return ctrl.Result{RequeueAfter: 2 * time.Minute}, nil
	}

	operator.Calculate()
	updateService := operator.SetResult(apiProductService)

	if updateService {
		// delete subscription plan
		status := *apiProductService.Status.DeepCopy()
		if err := r.updateAPIProductService(defaultContext, apiProductService, lease, logger); nil != err {
			return errorHandler.ReconcileError(err, common.ReconcileDefaultDelay)
		}
		apiProductService.Status = status
	}
	//apiProductService.Status.Phase = ampv1.ServiceOpened
	if err := r.updateAPIProductServiceStatus(defaultContext, apiProductService, lease, logger); nil != err {
		return errorHandler.ReconcileError(err, common.ReconcileDefaultDelay)
	}
	logger.Info("successfully reconciled")
	return ctrl.Result{RequeueAfter: operator.NextScheduleTime()}, nil

	//if err := r.expireAPIProductService(defaultContext, lease, apiProductService, logger); nil != err {
	//	return errorHandler.ReconcileError(err, common.ReconcileDefaultDelay)
	//}

	//logger.Info("successfully reconciled: expired, stop scheduling")
	//return ctrl.Result{}, nil
}

func (r *LeaseReconciler) updateAPIProductService(defaultContext context.Context, service *ampv1.ApiProductService, lease *ampv1.Lease, logger logr.Logger) error {
	if err := r.Update(defaultContext, service); nil != err {
		if apiError.IsNotFound(err) {
			logger.Info("error reconcile lease: api product service not found, delete lease", "err", err.Error())
			return r.Delete(defaultContext, lease)
		}
		logger.Info("error reconcile lease: while update api product service subscription plans", "err", err.Error())
		return err
	}
	logger.Info("update api product service success")
	return nil
}

func (r *LeaseReconciler) updateAPIProductServiceStatus(defaultContext context.Context, service *ampv1.ApiProductService, lease *ampv1.Lease, logger logr.Logger) error {
	if err := r.Status().Update(defaultContext, service); nil != err {
		if apiError.IsNotFound(err) {
			logger.Info("error reconcile lease: api product service not found, delete lease", "err", err.Error())
			return r.Delete(defaultContext, lease)
		}
		logger.Info("error while update api product service status", "err", err.Error())
		return err
	}
	logger.Info("update api product service status success", "status", service.Status)
	return nil
}

func (r *LeaseReconciler) expireAPIProductService(defaultContext context.Context, lease *ampv1.Lease, apiProductService *ampv1.ApiProductService, logger logr.Logger) error {
	if ampv1.ServiceExpired == apiProductService.Status.Phase {
		return nil
	}
	apiProductService.Status.Phase = ampv1.ServiceExpired
	if err := r.Status().Update(defaultContext, apiProductService); nil != err {
		if apiError.IsNotFound(err) {
			logger.Info("api product service not found, delete lease", "apiproductservice", apiProductService.GetName(), "lease", lease.GetName())
			return r.Delete(defaultContext, lease)
		}
		logger.Info("error while expire api product status", "err", err.Error())
		return err
	}
	return nil
}

func (r *LeaseReconciler) getAPIProductService(defaultContext context.Context, lease *ampv1.Lease, logger logr.Logger) (*ampv1.ApiProductService, error) {
	apiProductService := &ampv1.ApiProductService{}
	if err := r.Get(defaultContext, types.NamespacedName{Name: lease.Spec.APIProductSericeName}, apiProductService); nil != err {
		return nil, err
	}
	return apiProductService, nil
}

func (r *LeaseReconciler) getAPIProduct(defaultContext context.Context, apiProductService *ampv1.ApiProductService, lease *ampv1.Lease) (*ampv1.ApiProduct, error) {
	apiProduct := &ampv1.ApiProduct{}
	if err := r.Get(defaultContext, types.NamespacedName{Name: apiProductService.Spec.ProductName}, apiProduct); nil != err {
		if apiError.IsNotFound(err) {
			return nil, r.terminateAPIProductService(defaultContext, apiProductService, lease)
		}
		return nil, err
	}
	return apiProduct, nil
}

func (r *LeaseReconciler) getConsumer(defaultContext context.Context, apiProductService *ampv1.ApiProductService, lease *ampv1.Lease) (*ampv1.ApiConsumeApplication, error) {
	consumer := &ampv1.ApiConsumeApplication{}
	if err := r.Get(defaultContext, types.NamespacedName{Name: apiProductService.Spec.ConsumeApplicationName}, consumer); nil != err {
		if apiError.IsNotFound(err) {
			return nil, r.terminateAPIProductService(defaultContext, apiProductService, lease)
		}
		return nil, err
	}
	return consumer, nil
}

func (r *LeaseReconciler) terminateAPIProductService(defaultContext context.Context, apiProductService *ampv1.ApiProductService, lease *ampv1.Lease) error {
	apiProductService.Status.Phase = ampv1.ServiceTerminated
	mergePatch := []byte(fmt.Sprintf(`{"status":{"phase":%q}}`, ampv1.ServiceTerminated))
	if err := r.Status().Patch(defaultContext, apiProductService, client.ConstantPatch(types.MergePatchType, mergePatch)); nil != err {
		if apiError.IsNotFound(err) {
			return r.Delete(defaultContext, lease)
		}
		return err
	}
	return nil
}

var LeaseConcurrence int

func init() {
	flag.IntVar(&LeaseConcurrence, "lease-concurrence", 1, "max concurrent reconciles for lease")
	//flag.Parse()
}

func (r *LeaseReconciler) SetupWithManager(mgr ctrl.Manager) error {
	return ctrl.NewControllerManagedBy(mgr).
		For(&ampv1.Lease{}).
		WithOptions(controller.Options{MaxConcurrentReconciles: LeaseConcurrence}).
		Complete(r)
}
