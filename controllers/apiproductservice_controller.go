/*

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package controllers

import (
	"context"
	"flag"
	"fmt"
	"reflect"
	"time"

	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"sigs.k8s.io/controller-runtime/pkg/controller"

	"alauda.io/amp-controller/metering"

	"alauda.io/amp-controller/gateway/kong"

	"github.com/go-logr/logr"
	"github.com/google/uuid"
	"k8s.io/apimachinery/pkg/api/errors"
	"k8s.io/apimachinery/pkg/runtime/schema"
	"k8s.io/apimachinery/pkg/types"
	ctrl "sigs.k8s.io/controller-runtime"
	"sigs.k8s.io/controller-runtime/pkg/client"

	ampv1 "alauda.io/amp-controller/api/v1"
	"alauda.io/amp-controller/common"
	"alauda.io/amp-controller/gateway"
	logC "alauda.io/amp-controller/log"
)

// ApiProductServiceReconciler reconciles a ApiProductService object
type ApiProductServiceReconciler struct {
	client.Client
	Log logr.Logger
}

// +kubebuilder:rbac:groups=amp.alauda.io,resources=apiproductservices,verbs=get;list;watch;create;update;patch;delete
// +kubebuilder:rbac:groups=amp.alauda.io,resources=apiproductservices/status,verbs=get;update;patch

// api product service controller manage credential and lease life cycle (without deletion)
// * create or delete credential
// * TODO: if api product secure mode change, update credential for consumer
// * create lease after credential is ready

func (r *ApiProductServiceReconciler) Reconcile(request ctrl.Request) (ctrl.Result, error) {
	// Fetch the ApiProductService instance
	logger := r.Log.WithValues("request", request)
	sessionID := uuid.New().String()
	logger = logC.WithSessionID(logger, &sessionID)
	defaultContext := context.WithValue(context.Background(), common.RequestID, sessionID)
	errorHandler := ErrorHandler{Log: logger}
	instance := &ampv1.ApiProductService{}

	logger.Info("api product service reconcile")
	if kong.GatewayConfig.StandBy {
		//logger.Info("standby mode, ignore")
		return ctrl.Result{}, nil
	}

	err := r.Get(context.TODO(), request.NamespacedName, instance)
	if err != nil {
		if errors.IsNotFound(err) {
			// Object not found, return.  Created objects are automatically garbage collected.
			// For additional cleanup logic use finalizers.
			return ctrl.Result{}, nil
		}
		// Error reading the object - requeue the request.
		return ctrl.Result{}, err
	}

	// 1. check state
	if ampv1.ServiceTerminated == instance.Spec.SubscriptionStatus || ampv1.ServiceTerminated == instance.Status.Phase {
		if err := gateway.SyncGatewayBindingDelete(instance); nil != err {
			logger.Error(err, "api product service reconcile fail", "error", err)
			return errorHandler.ReconcileError(err, common.ReconcileDefaultDelay)
		}
		lease := &ampv1.Lease{}
		lease.SetName(string(instance.GetUID()))
		if err := r.Delete(defaultContext, lease); nil != err && !errors.IsNotFound(err) {
			return errorHandler.ReconcileError(err, common.ReconcileDefaultDelay)
		}
		if err := r.Delete(defaultContext, instance); nil != err && !errors.IsNotFound(err) {
			return errorHandler.ReconcileError(err, common.ReconcileDefaultDelay)
		}
		logger.Info("api product service terminated, ignore")
		return ctrl.Result{}, nil
	}

	// 2. manage credentials
	// note: manage event generation, avoid race condition[
	if err := r.syncCredentials(instance, defaultContext, logger); nil != err {
		// TODO: update status
		logger.Info(fmt.Sprintf("sync api product service error, err=%s", err.Error()))
		statusErr := r.updateProductServiceStatus(defaultContext, instance, err)
		if nil != statusErr {
			logger.Info(fmt.Sprintf("update api product service deploy status error, err=%s", statusErr.Error()))
		}
		return errorHandler.ReconcileError(err, common.ReconcileDefaultDelay)
	}

	// TODO: update labels

	// 3. check lease
	var isCreate bool
	lease := &ampv1.Lease{}
	leaseKey := types.NamespacedName{Name: string(instance.GetUID())}
	if err := r.Get(defaultContext, leaseKey, lease); nil != err {
		if errors.IsNotFound(err) {
			isCreate = true
		} else {
			return errorHandler.ReconcileError(err, common.ReconcileDefaultDelay)
		}
	}

	// 4. create or acquire lease
	if isCreate {
		// create lease
		if err := r.Create(defaultContext, NewLease(string(instance.GetUID()), instance.GetName())); nil != err && !errors.IsAlreadyExists(err) {
			deployErr := fmt.Errorf("failed to create lease err=%s", err.Error())
			if statusErr := r.updateProductServiceStatus(defaultContext, instance, deployErr); nil != statusErr {
				logger.Info("update api product service deploy status error", "error", statusErr.Error())
			}
			logger.Info("apiproductservice reconcile error", "error", deployErr.Error())
			return errorHandler.ReconcileError(err, common.ReconcileDefaultDelay)
		}
	} else {
		// acquire lease
		if err := r.acquireLeaseIfNeeded(defaultContext, lease, instance, logger); nil != err {
			deployErr := fmt.Errorf("failed to acquire lease err=%s", err.Error())
			if statusErr := r.updateProductServiceStatus(defaultContext, instance, deployErr); nil != statusErr {
				logger.Info("update api product service deploy status error", "error", statusErr.Error())
			}
			logger.Info("apiproductservice reconcile error", "error", deployErr.Error())
			if errors.IsNotFound(err) {
				return ctrl.Result{}, deployErr
			}
			return errorHandler.ReconcileError(deployErr, common.ReconcileDefaultDelay)
		}
	}

	// 5. update deploy status
	//phase := ampv1.ServiceOpened
	logger.Info("update api product service status", "status", instance.Status)
	if err := r.updateProductServiceStatus(defaultContext, instance, nil); nil != err {
		logger.Info(fmt.Sprintf("update api product service deploy status error, err=%s", err.Error()))
	}

	logger.Info("api product service reconcile success")

	return ctrl.Result{}, nil
}

func (r *ApiProductServiceReconciler) acquireLeaseIfNeeded(defaultContext context.Context, lease *ampv1.Lease, instance *ampv1.ApiProductService, logger logr.Logger) error {
	if nil == lease {
		logger.Info("lease is nil")
		return nil
	}
	if instance.SubscriptionPlanLongTime() {
		logger.Info("long time subscription plan exists")
		return nil
	}
	if ampv1.ServiceTerminated == instance.Spec.SubscriptionStatus || ampv1.ServiceTerminated == instance.Status.Phase {
		logger.Info("api product service terminated")
		return nil
	}
	subscriptions := instance.Spec.SubscriptionPlans
	if len(instance.Spec.SubscriptionPlans) == 0 && nil == instance.Status.Condition {
		logger.Info("no subscription plans")
		return nil
	}
	var needAcquire bool
	if len(subscriptions) != 0 && ampv1.ServiceExpired == instance.Status.Phase {
		logger.Info("acquire lease for finding new subscription plans when api product service is expired")
		needAcquire = true
	}
	if !needAcquire && ampv1.ServiceOpened == instance.Status.Phase {
		// miss schedule
		if nil != instance.Status.Condition {
			var interval time.Duration
			switch instance.SubscriptionPlanType() {
			case ampv1.MonlthlyPlan:
				interval = time.Duration(metering.MeteringConfig.MonthlyInterval)
			case ampv1.PricingPackage:
				interval = time.Duration(metering.MeteringConfig.PricingPackageInterval)
			default:
				return nil
			}
			now := metav1.NowMicro()
			if 0 != interval && instance.Status.Condition.LastScheduleTime.Before(&now) && now.Sub(instance.Status.Condition.LastScheduleTime.Time) > 3*interval {
				logger.Info(fmt.Sprintf("reconcile apiproductservice: acquire lease for missed schedule time is more than %s", interval.String()))
				needAcquire = true
			}
		}
	}
	if needAcquire {
		lease.Acquire()
		if err := r.Update(defaultContext, lease); nil != err {
			return err
		}
	}
	return nil
}

func (r *ApiProductServiceReconciler) updateProductServiceStatus(defaultContext context.Context, instance *ampv1.ApiProductService, err error) error {
	status := ampv1.Deployed
	message := "deploy success"
	if nil != err {
		status = ampv1.DeployFailed
		message = fmt.Sprintf("error sync api product service: err=%s", err.Error())
	}
	if instance.Status.DeployStatus == status && instance.Status.Message == message {
		return nil
	}
	instance.Status.DeployStatus = status
	instance.Status.Message = message
	return r.Status().Update(defaultContext, instance)
}

// syncCredentials sync credentials and update annotation
func (r *ApiProductServiceReconciler) syncCredentials(instance *ampv1.ApiProductService, defaultContext context.Context, logger logr.Logger) error {
	annotations := instance.GetAnnotations()
	if nil == annotations {
		annotations = make(map[string]string, 1)
	}
	acl, err := gateway.GetCurrentACLBinding(defaultContext, instance, logger)
	if nil != err && !errors.IsNotFound(err) {
		return err
	}
	var product *ampv1.ApiProduct
	var order *ampv1.Order
	if errors.IsNotFound(err) {
		if ampv1.ServiceOpened == instance.Status.Phase {
			// create acl
			if product, err = r.getProduct(defaultContext, instance); nil != err {
				if errors.IsNotFound(err) {
					logger.Info("product not found", "product", instance.Spec.ProductName)
				}
				return err
			}
			if err := gateway.SyncGatewayACLBinding(defaultContext, nil, instance, product, logger); nil != err {
				return err
			}
		}
	} else {
		if nil != acl && ampv1.ServiceExpired == instance.Status.Phase {
			// delete acl
			if err := gateway.SyncDeleteGatewayACL(defaultContext, instance, logger); nil != err {
				return err
			}
		}
	}
	if ampv1.PluginSyncedTrue != annotations[ampv1.PluginSynced] {
		// TODO: sync credentials to kong
		if nil == product {
			if product, err = r.getProduct(defaultContext, instance); nil != err {
				if errors.IsNotFound(err) {
					logger.Info("product not found", "product", instance.Spec.ProductName)
				}
				return err
			}
		}
		if order, err = r.getOrder(defaultContext, instance.GetLabels()["order"]); nil != err {
			return err
		}
		kvConfig, err := gateway.SyncGatewayBinding(instance, product, order)
		if nil != err {
			// ignore invalid task, e.g. 404, 409
			return err
		}
		logger.Info("apiproductservice reconcile", "GetConsumerCredential", kvConfig)
		if nil != kvConfig {
			if nil == instance.Spec.Info {
				instance.Spec.Info = make(ampv1.APInfo, 10)
			}
			for k, v := range kvConfig {
				instance.Spec.Info[k] = v
			}
			annotations = instance.GetAnnotations()
			if nil == annotations {
				annotations = make(map[string]string, 1)
			}
			annotations[ampv1.PluginSynced] = ampv1.PluginSyncedTrue
			instance.SetAnnotations(annotations)
			if err := r.Update(context.TODO(), instance); nil != err {
				if errors.IsNotFound(err) {
					logger.Error(err, "apiproductservice reconcile fail, update product service 404")
				} else {
					logger.Error(err, "apiproductservice reconcile, update product service failed")
				}
				return err
			}
		}
	}
	// TODO: update status
	return nil
}

func (r *ApiProductServiceReconciler) getProduct(defaultContext context.Context, instance *ampv1.ApiProductService) (*ampv1.ApiProduct, error) {
	product := &ampv1.ApiProduct{}
	if err := r.Get(defaultContext, types.NamespacedName{Name: instance.Spec.ProductName}, product); nil != err {
		return nil, err
	}
	return product, nil
}

func (r *ApiProductServiceReconciler) getOrder(defaultContext context.Context, name string) (*ampv1.Order, error) {
	order := &ampv1.Order{}
	if err := r.Get(defaultContext, types.NamespacedName{Name: name}, order); nil != err {
		return nil, err
	}
	return order, nil
}

func (r *ApiProductServiceReconciler) Terminate(defaultContext context.Context, instance *ampv1.ApiProductService) error {
	if nil == instance {
		return nil
	}
	tm := metav1.NowMicro()
	mergePatch := []byte(fmt.Sprintf(`{"status":{"phase":%q,"termination_time":%q}}`, ampv1.ServiceTerminated, tm.UTC().Format(metav1.RFC3339Micro)))
	return r.Status().Patch(defaultContext, instance, client.ConstantPatch(types.MergePatchType, mergePatch))
}

func NewLease(name, apiProductServiceName string) *ampv1.Lease {
	lease := &ampv1.Lease{}
	lease.TypeMeta.SetGroupVersionKind(
		schema.GroupVersionKind{
			Kind:    reflect.TypeOf(lease).String(),
			Group:   ampv1.GroupVersion.Group,
			Version: ampv1.GroupVersion.Version,
		})
	lease.SetName(name)
	lease.Acquire()
	lease.Spec.APIProductSericeName = apiProductServiceName
	return lease
}

var APIProductServiceConcurrence int

func init() {
	flag.IntVar(&APIProductServiceConcurrence, "api-product-service-concurrence", 1, "max concurrent reconciles for api product service")
	//flag.Parse()
}

func (r *ApiProductServiceReconciler) SetupWithManager(mgr ctrl.Manager) error {
	return ctrl.NewControllerManagedBy(mgr).
		For(&ampv1.ApiProductService{}).
		WithOptions(controller.Options{MaxConcurrentReconciles: APIProductServiceConcurrence}).
		Complete(r)
}
