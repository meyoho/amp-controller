package controllers

import (
	"context"
	cryptorand "crypto/rand"
	"crypto/rsa"
	"fmt"

	"github.com/go-logr/logr"
	"github.com/google/uuid"
	"github.com/juju/errors"
	v1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	ctrl "sigs.k8s.io/controller-runtime"
	"sigs.k8s.io/controller-runtime/pkg/client"

	metaV1 "k8s.io/apimachinery/pkg/apis/meta/v1"

	ampV1 "alauda.io/amp-controller/api/v1"
	"alauda.io/amp-controller/common"
	"alauda.io/amp-controller/gateway"
	"alauda.io/amp-controller/gateway/kong"
	logC "alauda.io/amp-controller/log"
)

// BatchOrderReconciler reconciles a Order object
type BatchOrderReconciler struct {
	client.Client
	Log logr.Logger
}

// +kubebuilder:rbac:groups=amp.alauda.io,resources=batchorders,verbs=get;list;watch;create;update;patch;delete
// +kubebuilder:rbac:groups=amp.alauda.io,resources=batchorders/status,verbs=get;update;patch

func (r *BatchOrderReconciler) Reconcile(request ctrl.Request) (ctrl.Result, error) {

	// Fetch the BatchOrder instance
	sessionID := uuid.New().String()
	defaultContext := context.WithValue(context.Background(), common.RequestID, sessionID)
	logger := r.Log.WithValues("request", request)
	logger = logC.WithSessionID(logger, &sessionID)

	errorHandler := ErrorHandler{Log: logger}

	logger.Info("batchOrder reconcile")
	if kong.GatewayConfig.StandBy {
		//logger.Info("standby mode, ignore")
		return ctrl.Result{}, nil
	}
	instance := &ampV1.Batchorder{}
	err := r.Get(defaultContext, request.NamespacedName, instance)
	if err != nil {
		if errors.IsNotFound(err) {
			return ctrl.Result{}, nil
		}
		return ctrl.Result{}, err
	}
	logger.Info("batchOrder reconcile", "get batchOrder instance", instance) // handle create

	annotations := instance.GetAnnotations()
	if nil == annotations {
		annotations = make(map[string]string, 1)
	}

	if ampV1.PluginSyncedTrue == annotations[ampV1.PluginSynced] {
		logger.Info("batchOrder have synced, ignore")
		return ctrl.Result{}, nil
	}

	developer := make(map[string][]ampV1.Product, 1)
	for _, product := range instance.Spec.ProductList {
		developer[product.Developer] = append(developer[product.Developer], product)
	}

	if len(developer) == 0 {
		return ctrl.Result{}, errors.New("batchOrder developer is empty")
	}

	// create review order for first Reconcile
	if Created, ok := annotations[ampV1.ReviewOrderCreated]; !ok || ampV1.ReviewOrderCreatedTrue != Created {
		var devList []string
		for dev, _ := range developer {
			devList = append(devList, dev)
		}

		ord := generateReviewOrder(instance, devList)
		err = r.Create(context.TODO(), ord)
		if err != nil {
			instance.Status.Message = fmt.Sprintf("error update batchOrder annotation, err=%s", err.Error())
			if errNew := r.updateBatchOrderStatus(instance, defaultContext, logger, err); errNew != nil {
				logger.Info(fmt.Sprintf("update batchOrder deploy status error, err=%s", errNew.Error()))
			}
			return errorHandler.ReconcileError(err, Reconcile20MinutesLater)
		}

		annotations[ampV1.ReviewOrderCreated] = ampV1.ReviewOrderCreatedTrue
		instance.SetAnnotations(annotations)
		if err := r.updateBatchOrder(instance, defaultContext, logger); err != nil {
			return errorHandler.ReconcileError(err, Reconcile2MinutesLater)
		}
		logger.Info("generate review order for batchOrder")
		return ctrl.Result{}, nil
	}

	for _, products := range developer {
		if products[0].Status == ampV1.OrderApprovalStatusUnapproved {
			return ctrl.Result{}, nil
		}
	}

	approvedDeveloper := make(map[string][]ampV1.Product, 1)
	for _, products := range developer {
		if products[0].Status == ampV1.OrderApprovalStatusApproved {
			approvedDeveloper[products[0].Developer] = products
		}
	}

	if ampV1.PluginSyncedTrue != annotations[ampV1.PluginSynced] {
		logger.Info("batchOrder reconcile", "get batchOrder developer", approvedDeveloper)
		var param1, param2 string
		param1 = uuid.New().String()
		param2 = uuid.New().String()
		privateKey, _ := rsa.GenerateKey(cryptorand.Reader, 2048)
		for _, products := range approvedDeveloper {
			var order *ampV1.Order
			var err error
			for _, product := range products {
				order, err = generateApprovedOrder(instance.DeepCopy(), &product, privateKey, param1, param2)
				if err == nil {
					err = r.Create(defaultContext, order)
					if err != nil {
						return errorHandler.ReconcileError(err, Reconcile2MinutesLater)
					}
				}
			}
			annotations[ampV1.PluginSynced] = ampV1.PluginSyncedTrue
			instance.SetAnnotations(annotations)
			if err = r.updateBatchOrder(instance, defaultContext, logger); err != nil {
				return errorHandler.ReconcileError(err, Reconcile2MinutesLater)
			}

		}
		if err = r.updateBatchOrderStatus(instance, defaultContext, logger, nil); err != nil {
			logger.Info(fmt.Sprintf("update batchOrder deploy status error, err=%s", err.Error()))
		}
	}

	return ctrl.Result{}, nil
}

func (r *BatchOrderReconciler) updateBatchOrder(instance *ampV1.Batchorder, ctx context.Context,
	logger logr.Logger) error {
	if err := r.Update(ctx, instance); nil != err {
		logger.Error(err, "update batchOrder  error")
		return err
	}
	return nil
}

func (r *BatchOrderReconciler) updateBatchOrderStatus(instance *ampV1.Batchorder, ctx context.Context,
	logger logr.Logger, err error) error {
	status := ampV1.BatchOrderDeployed
	message := "deploy success"
	if nil != err {
		status = ampV1.BatchOrderDeployFailed
		message = fmt.Sprintf("error deploy batchorder: err=%s", err.Error())
	}
	if instance.Status.DeployStatus == status && instance.Status.Message == message {
		return nil
	}
	instance.Status.DeployStatus = status
	instance.Status.Message = message

	return r.Status().Update(ctx, instance)
}

func generateApprovedOrder(instance *ampV1.Batchorder, product *ampV1.Product, privateKey *rsa.PrivateKey,
	param1 string, param2 string) (*ampV1.Order, error) {

	orderName := fmt.Sprintf("batch-%s", common.GetSnowFlakeID())
	authType := instance.Spec.Security.Type
	config := make(map[string]interface{}, 1)
	security := &ampV1.Security{
		Type:   authType,
		Config: config,
	}

	switch ampV1.PluginType(authType) {
	case ampV1.JWT:
		RSAPublicKey, err := gateway.EncodePublicKeyPEM(privateKey.Public())
		if err != nil {
			return nil, err
		}

		tokenString, err := gateway.NewBearerToken(privateKey, param1)
		if nil != err {
			return nil, err
		}
		security.Config[ampV1.JWTRSAPublicKey] = string(RSAPublicKey)
		security.Config[ampV1.Beareroken] = tokenString
		security.Config[ampV1.JWTKey] = param1
		security.Config[ampV1.JWTSecret] = param2

	case ampV1.OAuth2:
		security.Config[ampV1.OAuth2ClientId] = param1
		security.Config[ampV1.OAuth2ClientSecret] = param2
	case ampV1.BASIC:
		security.Config[ampV1.BasicUserName] = param1
		security.Config[ampV1.BasicPassword] = param2
	}

	ord := &ampV1.Order{
		TypeMeta: v1.TypeMeta{
			APIVersion: "amp.alauda.io/v1",
			Kind:       "Order",
		},
		ObjectMeta: v1.ObjectMeta{
			Name: orderName,
			Annotations: map[string]string{
				ampV1.PluginSynced: ampV1.PluginSyncedFalse,
			},
		},
		Spec: ampV1.OrderSpec{
			ID:                            orderName,
			OrderName:                     orderName,
			ConsumeApplicationName:        instance.Spec.ConsumeApplicationName,
			ConsumeApplicationProjectName: instance.Spec.ConsumeApplicationProjectName,
			Security:                      *security,
			SubscriptionPlan:              instance.Spec.SubscriptionPlan,
			OwnerUserName:                 instance.Spec.OwnerUserName,
			ApprovalStatus:                ampV1.OrderApprovalStatusApproved,
			ProductName:                   product.ProductName,
			ProductDeveloperName:          product.Developer,
			ProductProjectName:            product.ProductProjectName,
		},
	}
	return ord, nil
}

func generateReviewOrder(instance *ampV1.Batchorder, devList []string) *ampV1.Order {
	orderName := instance.GetName()
	currentTime := metaV1.Now()
	consumerStatusLabelKey := "approval_status." + instance.Spec.OwnerUserName
	labels := map[string]string{
		"consume_application_name":         instance.Spec.ConsumeApplicationName,
		"consume_application_project_name": instance.Spec.ConsumeApplicationProjectName,
		"order_name":                       orderName,
		"owner_user_name":                  instance.Spec.OwnerUserName,
		consumerStatusLabelKey:             "0",
		"developer_team_name":              "amp",
	}

	for _, dev := range devList {
		labels["product_developer_name."+dev] = dev
		labels["approval_status."+dev] = "0"
	}

	//labels["product_developer_name"] = devList[0]  // only as debug
	ord := &ampV1.Order{
		TypeMeta: v1.TypeMeta{
			APIVersion: "amp.alauda.io/v1",
			Kind:       "Order",
		},
		ObjectMeta: v1.ObjectMeta{
			Name: orderName,
			Annotations: map[string]string{
				ampV1.BatchOrderFlag: orderName,
			},
			Labels: labels,
		},
		Spec: ampV1.OrderSpec{
			ID:                            orderName,
			OrderName:                     orderName,
			ConsumeApplicationName:        instance.Spec.ConsumeApplicationName,
			ConsumeApplicationProjectName: instance.Spec.ConsumeApplicationProjectName,
			Security:                      instance.Spec.Security,
			SubscriptionPlan:              instance.Spec.SubscriptionPlan,
			OwnerUserName:                 instance.Spec.OwnerUserName,
			CreateUserName:                instance.Spec.OwnerUserName,
			CreateTime:                    &currentTime,
			UpdateTime:                    &currentTime,
		},
	}
	return ord
}

func (r *BatchOrderReconciler) SetupWithManager(mgr ctrl.Manager) error {
	return ctrl.NewControllerManagedBy(mgr).
		For(&ampV1.Batchorder{}).
		Complete(r)
}
