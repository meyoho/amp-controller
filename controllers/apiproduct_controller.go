/*

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package controllers

import (
	"context"
	"flag"
	"fmt"

	v1 "k8s.io/apimachinery/pkg/apis/meta/v1"

	"sigs.k8s.io/controller-runtime/pkg/controller"

	"github.com/go-logr/logr"
	"github.com/google/uuid"
	"k8s.io/apimachinery/pkg/apis/meta/v1/unstructured"
	"k8s.io/apimachinery/pkg/types"
	ctrl "sigs.k8s.io/controller-runtime"
	"sigs.k8s.io/controller-runtime/pkg/client"

	"alauda.io/amp-controller/gateway/kong"

	ampv1 "alauda.io/amp-controller/api/v1"
	"alauda.io/amp-controller/common"
	"alauda.io/amp-controller/gateway"
)

// ApiProductReconciler reconciles a ApiProduct object
type ApiProductReconciler struct {
	client.Client
	Log logr.Logger
}

// +kubebuilder:rbac:groups=amp.alauda.io,resources=apiproducts,verbs=get;list;watch;create;update;patch;delete
// +kubebuilder:rbac:groups=amp.alauda.io,resources=apiproducts/status,verbs=get;update;patch

func (r *ApiProductReconciler) Reconcile(request ctrl.Request) (ctrl.Result, error) {
	// need to avoid frequently re-reconcile
	// need to avoid revision conflict

	sessionID := uuid.New().String()
	defaultContext := context.WithValue(context.Background(), common.RequestID, sessionID)
	logger := r.Log.WithValues("request", request).WithValues(common.RequestID, sessionID)
	logger.Info("api product reconcile")

	if kong.GatewayConfig.StandBy {
		// logger.Info("standby mode, ignore")
		return ctrl.Result{}, nil
	}

	errorHandler := ErrorHandler{Log: logger}
	instance := &ampv1.ApiProduct{}

	var err error

	err = r.Get(context.TODO(), request.NamespacedName, instance)
	if nil != err {
		return errorHandler.ReconcileError(err, common.ReconcileDefaultDelay)
	}

	// when Delete, delete the service and route for swagger test
	// name of our custom finalizer
	myFinalizerName := "apiproduct.finalizers.amp.alauda.io"
	// examine DeletionTimestamp to determine if object is under deletion
	if instance.ObjectMeta.DeletionTimestamp.IsZero() {
		if !containsString(instance.ObjectMeta.Finalizers, myFinalizerName) {
			instance.ObjectMeta.Finalizers = append(instance.ObjectMeta.Finalizers, myFinalizerName)
			if err := r.Update(context.Background(), instance); err != nil {
				return ctrl.Result{}, err
			}
		}
	} else {
		// The object is being deleted
		if containsString(instance.ObjectMeta.Finalizers, myFinalizerName) {
			// our finalizer is present, so lets handle any external dependency
			if instance.Spec.SwaggerFileUrl != "" {
				syncer, err := gateway.NewSyncer(instance, sessionID)
				if nil != err {
					deployStatus := ampv1.DeployFailed
					message := fmt.Sprintf("error create syncer, err=%s", err.Error())
					if statusErr := r.patchStatus(defaultContext, *instance, deployStatus, message); nil != statusErr {
						logger.Info("create syncer fail and sync status fail", "err", statusErr, "message", message)
					}
					logger.Error(err, "error create syncer")
					// validation error, no reconcile
					// reconcile if http server error, e.g.  httpStatusCode > 500
					return errorHandler.ReconcileError(err, common.ReconcileDefaultDelay)
				}
				if err := syncer.SyncForTest("delete"); nil != err {
					return errorHandler.ReconcileError(err, common.ReconcileDefaultDelay)
				}
			}

			consumeApplicationName := instance.GetName() + "-online-debug"
			consumer := &ampv1.ApiConsumeApplication{
				ObjectMeta: v1.ObjectMeta{
					Name: consumeApplicationName,
				},
				TypeMeta: v1.TypeMeta{
					APIVersion: "amp.alauda.io/v1",
					Kind:       "ApiConsumeApplication",
				},
			}
			if err = r.Delete(defaultContext, consumer); err != nil {
				return errorHandler.ReconcileError(err, common.ReconcileDefaultDelay)
			}

			// remove our finalizer from the list and update it.
			instance.ObjectMeta.Finalizers = removeString(instance.ObjectMeta.Finalizers, myFinalizerName)
			if err := r.Update(context.Background(), instance); err != nil {
				return ctrl.Result{}, err
			}
		}
		return ctrl.Result{}, err
	}

	// when created, deploy swagger test service
	if instance.Spec.Status == ampv1.ProductStatusUnpublished {
		if instance.Spec.SwaggerFileUrl != "" {
			syncer, err := gateway.NewSyncer(instance, sessionID)
			if nil != err {
				deployStatus := ampv1.DeployFailed
				message := fmt.Sprintf("error create syncer, err=%s", err.Error())
				if statusErr := r.patchStatus(defaultContext, *instance, deployStatus, message); nil != statusErr {
					logger.Info("create syncer fail and sync status fail", "err", statusErr, "message", message)
				}
				logger.Error(err, "error create syncer")
				// validation error, no reconcile
				// reconcile if http server error, e.g.  httpStatusCode > 500
				return errorHandler.ReconcileError(err, common.ReconcileDefaultDelay)
			}
			if err := syncer.SyncForTest("create"); nil != err {
				return errorHandler.ReconcileError(err, common.ReconcileDefaultDelay)
			}
		}
	}

	if !instance.Spec.Status.Available() && !instance.Spec.Status.Offline() {
		logger.Info("Not released, ignore")
		return ctrl.Result{}, nil
	}
	// update consumer for api product
	if err := r.updateProductSelfConsumer(defaultContext, instance, logger); err != nil {
		logger.Error(err, "error update product self consumer")
		return errorHandler.ReconcileError(err, common.ReconcileDefaultDelay)
	}

	// do not overwrite data in Kong if not certain

	// get service from Kong first, if it is the same service that crd describes, e.g. name is the same, uuid is the same, do sync

	// if conflict, e.g mismatch in name, uuid, owner name, will retain config in kong, report 409 in product status

	// for legancy service, tags=null, providing the same service name and uuid when update is required, new tags will be added to service

	// for routes has it's path, reuse uuid from same service is reasonable

	// for plugins, they are identified by type, uuid not matters

	syncer, err := gateway.NewSyncer(instance, sessionID)
	if nil != err {
		deployStatus := ampv1.DeployFailed
		message := fmt.Sprintf("error create syncer, err=%s", err.Error())
		if statusErr := r.patchStatus(defaultContext, *instance, deployStatus, message); nil != statusErr {
			logger.Info("create syncer fail and sync status fail", "err", statusErr, "message", message)
		}
		logger.Error(err, "error create syncer")
		// validation error, no reconcile
		// reconcile if http server error, e.g.  httpStatusCode > 500
		return errorHandler.ReconcileError(err, common.ReconcileDefaultDelay)
	}
	if syncer.Mutated() {
		if err = r.Update(context.TODO(), instance); nil != err {
			logger.Error(err, "product not integrity, will retry in 1 minutes")
			// sync k8s fail, but data need mutate, reconcile till success
			return errorHandler.ReconcileError(err, common.ReconcileDefaultDelay)
		}
		logger.Info("api product reconcile: mutate obj")
		// won't handle this event any more for new event has generated by last update
		return ctrl.Result{}, nil
	}
	if isUpToDate(instance) {
		logger.Info("Reconciled: already up to date")
		// already synced
		return ctrl.Result{}, nil
	}

	// sync to kong
	logger.Info("start syncing")
	if err = syncer.Do(); nil != err {
		deployStatus := ampv1.DeployFailed
		// message := fmt.Sprintf("api product reconcile fail in sync gateway api, err=%s, sessionID=%s", err.Error(), sessionID)
		message := fmt.Sprintf("api product reconcile fail in sync gateway api, err=%s", err.Error())
		if statusErr := r.patchStatus(defaultContext, *instance, deployStatus, message); nil != statusErr {
			logger.Info("sync fail and sync status fail", "err", statusErr, "message", message)
		}
		// logger.Info("sync fail, will retry in 1 minutes", "err", err.Error())
		return errorHandler.ReconcileError(err, common.HttpConflictNoReconcile)
	}

	if err = r.updateStatus(instance); nil != err {
		logger.Info("sync to kong success, error sync instance status, will retry in 1 minutes", "err", err)
		return errorHandler.ReconcileError(err, common.ReconcileDefaultDelay)
	}
	logger.Info("Successfully Reconciled")
	return ctrl.Result{}, nil
}

func (r *ApiProductReconciler) updateProductSelfConsumer(ctx context.Context, instance *ampv1.ApiProduct, logger logr.Logger) error {
	consumeApplicationName := instance.GetName() + "-online-debug"
	consumer := &ampv1.ApiConsumeApplication{}
	consumerKey := types.NamespacedName{Name: consumeApplicationName}
	if err := r.Get(ctx, consumerKey, consumer); nil != err {
		//logger.Info("get api consume application", consumerKey, consumerKey)
		return err
	}

	annotations := consumer.GetAnnotations()
	if nil == annotations {
		annotations = make(map[string]string, 1)
	}

	isChanged := false
	if instance.Spec.Status.Available() {
		if val, ok := annotations[ampv1.ConsumerCertificationSynced]; !ok || val != ampv1.ConsumerCertificationSyncedTrue {
			isChanged = true
		}
		annotations[ampv1.ConsumerCertificationSynced] = ampv1.ConsumerCertificationSyncedTrue
	}

	if instance.Spec.Status.Offline() {
		if val, ok := annotations[ampv1.ConsumerCertificationSynced]; !ok || val != ampv1.ConsumerCertificationSyncedFalse {
			isChanged = true
		}
		annotations[ampv1.ConsumerCertificationSynced] = ampv1.ConsumerCertificationSyncedFalse
	}

	if isChanged {
		consumer.SetAnnotations(annotations)
		return r.Update(ctx, consumer)
	}

	return nil
}

// Helper functions to check and remove string from a slice of strings.
func containsString(slice []string, s string) bool {
	for _, item := range slice {
		if item == s {
			return true
		}
	}
	return false
}

func removeString(slice []string, s string) (result []string) {
	for _, item := range slice {
		if item == s {
			continue
		}
		result = append(result, item)
	}
	return
}

func (r *ApiProductReconciler) patchStatus(ctx context.Context, instance ampv1.ApiProduct, deployStatus ampv1.ProductDeployPhase, message string) error {
	mergePatch := []byte(fmt.Sprintf(`{"status":{"deploy_status":%q,"message":%q}}`, deployStatus, message))
	return r.Status().Patch(ctx, &instance, client.ConstantPatch(types.MergePatchType, mergePatch))
}

func (r *ApiProductReconciler) updateStatus(instance *ampv1.ApiProduct) error {
	var err error
	// FIXME: multiple update in one reconcile will cause race conditions
	if err = r.updateProductAfterSync(instance); nil != err {
		return err
	}
	if err = r.updateStatusAfterSync(instance); nil != err {
		return err
	}
	return err
}

func (r *ApiProductReconciler) updateProductAfterSync(instance *ampv1.ApiProduct) error {
	annotations := instance.GetAnnotations()
	annotations[ampv1.PluginSynced] = ampv1.PluginSyncedTrue
	delete(annotations, ampv1.LastAppliedConfigAnnotation)
	instance.SetAnnotations(annotations)
	// use update other than patch to avoid corrupt data
	return r.Update(context.TODO(), instance)
}

func (r *ApiProductReconciler) updateStatusAfterSync(instance *ampv1.ApiProduct) error {
	key := types.NamespacedName{Name: instance.GetName()}
	available := instance.Spec.Status.Available()
	r.Log.Info("get instance before update", "instance", instance.GetResourceVersion())
	obj := &unstructured.Unstructured{}
	obj.SetGroupVersionKind(instance.GroupVersionKind())
	if err := r.Get(context.TODO(), key, obj); nil != err {
		r.Log.Error(err, "err after sync: update status get instance")
		return err
	}
	r.Log.Info("get instance for update", "instance", instance.GetResourceVersion())
	unstructured.SetNestedField(obj.Object, string(ampv1.Deployed), "status", "deploy_status")
	unstructured.SetNestedField(obj.Object, available, "status", "available")
	unstructured.SetNestedField(obj.Object, "deploy success", "status", "message")
	if err := r.Status().Update(context.TODO(), obj); nil != err {
		r.Log.Error(err, "err after sync: update status")
	}
	return nil
}

func isUpToDate(instance *ampv1.ApiProduct) bool {
	if nil == instance {
		return false
	}
	annotations := instance.GetAnnotations()
	if nil == annotations {
		return false
	}
	newSyncReq := ampv1.PluginSyncedFalse == annotations[ampv1.PluginSynced]
	pluginSynced := ampv1.Deployed == instance.Status.DeployStatus
	_, isUpdate := annotations[ampv1.LastAppliedConfigAnnotation]
	statusSynced := instance.Status.Available == instance.Spec.Status.Available()
	if !isUpdate && !newSyncReq && pluginSynced && statusSynced {
		return true
	}
	return false
}

var APIProductConcurrence int

func init() {
	flag.IntVar(&APIProductConcurrence, "api-product-concurrence", 1, "max concurrent reconciles for api product")
	// flag.Parse()
}

func (r *ApiProductReconciler) SetupWithManager(mgr ctrl.Manager) error {
	return ctrl.NewControllerManagedBy(mgr).
		For(&ampv1.ApiProduct{}).
		WithOptions(controller.Options{MaxConcurrentReconciles: APIProductConcurrence}).
		Complete(r)
}
