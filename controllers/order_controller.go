/*

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package controllers

import (
	"context"
	"flag"
	"fmt"
	"reflect"
	"sort"
	"time"

	"alauda.io/amp-controller/metering"

	"sigs.k8s.io/controller-runtime/pkg/controller"

	"alauda.io/amp-controller/gateway/kong"

	"github.com/google/uuid"
	"k8s.io/apimachinery/pkg/apis/meta/v1/unstructured"

	"alauda.io/amp-controller/gateway/diff/sets"

	"github.com/go-logr/logr"
	"k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime/schema"
	"k8s.io/apimachinery/pkg/types"
	ctrl "sigs.k8s.io/controller-runtime"
	"sigs.k8s.io/controller-runtime/pkg/client"

	ampv1 "alauda.io/amp-controller/api/v1"
	"alauda.io/amp-controller/common"
	logC "alauda.io/amp-controller/log"
)

var (
	Reconcile20MinutesLater = &common.ReconcileOption{Flag: common.NoFlag, RetryAfter: 20 * time.Minute}
	Reconcile2MinutesLater  = &common.ReconcileOption{Flag: common.NoFlag, RetryAfter: 2 * time.Minute}
)

// OrderReconciler reconciles a Order object
type OrderReconciler struct {
	client.Client
	Log logr.Logger
}

// +kubebuilder:rbac:groups=amp.alauda.io,resources=orders,verbs=get;list;watch;create;update;patch;delete
// +kubebuilder:rbac:groups=amp.alauda.io,resources=orders/status,verbs=get;update;patch

func (r *OrderReconciler) Reconcile(request ctrl.Request) (ctrl.Result, error) {
	// Fetch the Order instance
	sessionID := uuid.New().String()
	defaultContext := context.WithValue(context.Background(), common.RequestID, sessionID)
	logger := r.Log.WithValues("request", request)
	logger = logC.WithSessionID(logger, &sessionID)

	errorHandler := ErrorHandler{Log: logger}

	logger.Info("order reconcile")
	if kong.GatewayConfig.StandBy {
		//logger.Info("standby mode, ignore")
		return ctrl.Result{}, nil
	}
	instance := &ampv1.Order{}
	err := r.Get(context.TODO(), request.NamespacedName, instance)
	if err != nil {
		if errors.IsNotFound(err) {
			// Object not found, return.  Created objects are automatically garbage collected.
			// For additional cleanup logic use finalizers.
			return ctrl.Result{}, nil
		}
		// Error reading the object - requeue the request.
		return ctrl.Result{}, err
	}
	//logger.Info("order reconcile", "get order instance", instance)

	if ampv1.OrderApprovalStatusApproved != instance.Spec.ApprovalStatus {
		logger.Info("order not approved, ignore")
		return ctrl.Result{}, nil
	}

	// handle create
	annotations := instance.GetAnnotations()
	if nil == annotations {
		annotations = make(map[string]string, 1)
	}

	// TODO: add status for order
	//updateOrderStatus := func(orderInstance *ampv1.Order, message string) {
	//	if startusErr := r.Status().Update(defaultContext, orderInstance); nil != startusErr {
	//		logger.Info(message, "error", startusErr.Error())
	//	}
	//}

	if ampv1.PluginSyncedTrue != annotations[ampv1.PluginSynced] {
		if err := r.createOrUpdateAPIProductService(instance, logger, defaultContext); nil != err {
			instance.Status.Message = fmt.Sprintf("error create or update APIProductService, err=%s", err.Error())
			//updateOrderStatus(instance, "reconcile order: fail to update order status")
			logger.Info("reconcile order: fail to create or update api product service", "error", instance.Status.Message)
			return errorHandler.ReconcileError(err, Reconcile20MinutesLater)
		}

		// update order annotation
		if err := r.updateOrderAnnotation(instance, logger, defaultContext); nil != err {
			instance.Status.Message = fmt.Sprintf("error update order annotation, err=%s", err.Error())
			//updateOrderStatus(instance, "reconcile order: fail to update order annotation")
			logger.Info("reconcile order: fail to create or update order annotation", "error", instance.Status.Message)
			return errorHandler.ReconcileError(err, Reconcile2MinutesLater)
		}

		// update api product status
		if err := r.updateAPIproductStatus(defaultContext, instance, logger); nil != err {
			instance.Status.Message = fmt.Sprintf("error add APIproduct subscription, err=%s", err.Error())
			//updateOrderStatus(instance, "reconcile order: fail to update order status")
			logger.Info("reconcile order: fail to update apiproduct status", "error", instance.Status.Message)
			return errorHandler.ReconcileError(err, Reconcile2MinutesLater)
		}
	}

	logger.Info("order reconcile success")

	return ctrl.Result{}, nil
}

func (r *OrderReconciler) createOrUpdateAPIProductService(order *ampv1.Order, logger logr.Logger, defaultContext context.Context) error {

	// change api product service name to api name + consumer name
	if nil == order {
		return nil
	}

	// get api product
	productKey := types.NamespacedName{Name: order.Spec.ProductName}
	product := &ampv1.ApiProduct{}
	if err := r.Get(defaultContext, productKey, product); nil != err {
		//logger.Info("get api product error", "productKey", productKey)
		if errors.IsNotFound(err) {
			return fmt.Errorf("fail: product %s not found ", productKey)
		}
		return err
	}

	// get consumer
	consumer := &ampv1.ApiConsumeApplication{}
	consumerKey := types.NamespacedName{Name: order.Spec.ConsumeApplicationName}
	if err := r.Get(defaultContext, consumerKey, consumer); nil != err {
		//logger.Info("get api consume application", consumerKey, consumerKey)
		if errors.IsNotFound(err) {
			return fmt.Errorf("fail: consumer %s not found", consumerKey)
		}
		return err
	}

	// create or update api product service
	var err error
	subscriptionPlan, err := ValidAndGetSubscriptionPlan(order.Spec, product)
	if nil != err {
		return err
	}

	createAPIProductService := func() error {
		apiProductService, err := NewApiProductService(order.Spec, product, subscriptionPlan)
		if nil != err {
			return err
		}
		if err := r.Create(defaultContext, apiProductService); nil != err {
			logger.Info("create apiproductservice fail", "error", err.Error())
			return err
		}
		logger.Info("create apiproductservice", "apiproductservice", apiProductService.GetName())
		return nil
	}

	serviceName := getAPIProductServiceName(order.Spec)
	apiProductService := &ampv1.ApiProductService{}
	serviceKey := types.NamespacedName{Name: serviceName}
	if err = r.Get(defaultContext, serviceKey, apiProductService); nil != err {
		// create api product service
		if errors.IsNotFound(err) {
			if err := createAPIProductService(); nil != err {
				return err
			}
			return nil
		}
		return err
	}

	// update api product service
	subscriptions := apiProductService.Spec.SubscriptionPlans
	set := sets.NewSubscription(subscriptions...)
	if set.Has(subscriptionPlan) {
		logger.Info("duplicate subscription", "service", apiProductService.GetName(), "subscription", subscriptionPlan.Id)
		return nil
	}
	apiProductService.Spec.SubscriptionPlans = append(apiProductService.Spec.SubscriptionPlans, subscriptionPlan)
	sort.Slice(subscriptions, func(i, j int) bool {
		return subscriptions[i].CreateTime.Before(&subscriptions[j].CreateTime)
	})
	return r.Update(defaultContext, apiProductService)
}

func (r *OrderReconciler) updateOrderAnnotation(order *ampv1.Order, logger logr.Logger, defaultContext context.Context) error {
	if nil == order {
		return nil
	}
	annotations := order.GetAnnotations()
	if nil == annotations {
		annotations = make(map[string]string, 1)
	}
	annotations[ampv1.PluginSynced] = ampv1.PluginSyncedTrue
	order.SetAnnotations(annotations)
	return r.Update(defaultContext, order)
}

func (r *OrderReconciler) updateOrderStatus(order *ampv1.Order, logger logr.Logger, defaultContext context.Context) error {
	return r.Status().Update(defaultContext, order)
}

func (r *OrderReconciler) updateAPIproductStatus(defaultContext context.Context, order *ampv1.Order, logger logr.Logger) error {
	obj := &unstructured.Unstructured{}
	obj.SetGroupVersionKind(schema.GroupVersionKind{
		Group:   ampv1.GroupVersion.Group,
		Version: ampv1.GroupVersion.Version,
		Kind:    "ApiProduct",
	})
	obj.SetName(order.Spec.ProductName)
	if err := r.Get(defaultContext, types.NamespacedName{Name: order.Spec.ProductName}, obj); nil != err {
		return err
	}
	subscriptionTimes, _, _ := unstructured.NestedInt64(obj.Object, "status", "subscription_times")
	//if !found || nil != err {
	//	return fmt.Errorf("unexcepted error: apiproduct.status.subscription_times, found=%v, err=%v", found, err)
	//}
	subscriptionTimes += 1
	_ = unstructured.SetNestedField(obj.Object, subscriptionTimes, "status", "subscription_times")
	return r.Status().Update(defaultContext, obj)
}

func getAPIProductServiceName(order ampv1.OrderSpec) string {
	return fmt.Sprintf("%s-%s", order.ProductName, order.ConsumeApplicationName)
}

func ValidAndGetSubscriptionPlan(order ampv1.OrderSpec, product *ampv1.ApiProduct) (ampv1.SubscriptionPlan, error) {
	subscription := product.Spec.SubscriptionPlans.Get(order.SubscriptionPlan)
	if nil == subscription {
		return ampv1.SubscriptionPlan{}, fmt.Errorf("subscription plan not in apiproduct, type=%s, name=%s, apiproduct=%s", order.SubscriptionPlan.Type, order.SubscriptionPlan.Name, product.GetName())
	}
	config := order.SubscriptionPlan.Config
	if ampv1.PricingPackage == subscription.Type {
		config = subscription.Config
	}
	result := ampv1.SubscriptionPlan{
		Id:         order.OrderName,
		Name:       subscription.Name,
		Type:       subscription.Type,
		Config:     config,
		Strategies: nil,
		CreateTime: metav1.NowMicro(),
	}

	validator := metering.NewValidator()
	if err := validator.ValidateSubScriptionPlan(result); nil != err {
		return ampv1.SubscriptionPlan{}, err
	}
	return result, nil
}

func NewApiProductService(order ampv1.OrderSpec, product *ampv1.ApiProduct, subscriptionPlan ampv1.SubscriptionPlan) (*ampv1.ApiProductService, error) {
	labels := map[string]string{
		"order":     order.OrderName,
		"product":   order.ProductName,
		"owner":     order.OwnerUserName,
		"developer": product.Spec.OwnerUserName,
		"consumer":  order.ConsumeApplicationName,
		"status":    string(ampv1.ServiceOpened),
	}
	SetCreateTimeLabels(labels)

	service := &ampv1.ApiProductService{
		ObjectMeta: metav1.ObjectMeta{
			Name:   getAPIProductServiceName(order),
			Labels: labels,
		},
		Spec: ampv1.ApiProductServiceSpec{
			ID:                     order.ID,
			OwnerUserName:          order.OwnerUserName,
			DeveloperName:          product.Spec.OwnerUserName,
			ProductName:            order.ProductName,
			ProductDisplayName:     product.Spec.DisplayName,
			ConsumeApplicationName: order.ConsumeApplicationName,
			SubscriptionPlans:      []ampv1.SubscriptionPlan{subscriptionPlan},
			SubscriptionStatus:     ampv1.ServiceOpened,
		},
	}

	service.TypeMeta.SetGroupVersionKind(
		schema.GroupVersionKind{
			Kind:    reflect.TypeOf(service).String(),
			Group:   ampv1.GroupVersion.Group,
			Version: ampv1.GroupVersion.Version,
		})
	return service, nil
}

const (
	LabelCreateTime  = "query_create_time"
	LabelCreateYear  = "query_create_year"
	LabelCreateMonth = "query_create_month"
)

func SetCreateTimeLabels(labels map[string]string) {
	now := time.Now()
	labels[LabelCreateTime] = now.Format("2006-01-02")
	labels[LabelCreateMonth] = now.Format("2006-01")
	labels[LabelCreateYear] = now.Format("2006")
}

var OrderConcurrence int

func init() {
	flag.IntVar(&OrderConcurrence, "order-concurrence", 1, "max concurrent reconciles for order")
	//flag.Parse()
}

func (r *OrderReconciler) SetupWithManager(mgr ctrl.Manager) error {
	return ctrl.NewControllerManagedBy(mgr).
		For(&ampv1.Order{}).
		WithOptions(controller.Options{MaxConcurrentReconciles: OrderConcurrence}).
		Complete(r)
}
