package controllers

import (
	"net/http"

	"github.com/go-logr/logr"
	apiErrors "k8s.io/apimachinery/pkg/api/errors"
	"sigs.k8s.io/controller-runtime/pkg/reconcile"
	logf "sigs.k8s.io/controller-runtime/pkg/runtime/log"

	"alauda.io/amp-controller/common"
)

var log = logf.Log.WithName("controller")

func IsAPIErorr(err error) bool {
	switch err.(type) {
	case apiErrors.APIStatus:
		return true
	}
	return false
}

func IsClientErorr(err error) bool {
	switch t := err.(type) {
	case apiErrors.APIStatus:
		return http.StatusInternalServerError > t.Status().Code && t.Status().Code >= http.StatusBadRequest
	}
	return false
}

func IsServerErorr(err error) bool {
	switch t := err.(type) {
	case apiErrors.APIStatus:
		return t.Status().Code >= http.StatusInternalServerError
	}
	return false
}

type ErrorHandler struct {
	Log logr.Logger
}

// ReconcileError is used to resolve situations where it is not possible
// to determine if an event should be coordinated later.
// reconcile strategy:
//	* handle error first, we use kube-rest for http requests which makes
//		the error scheme same with kubernetes client-go calls, so it's easy to
//		distinguish normal error and http errors, normal error will fail directly
//	* handle reconcile flags second, do what the flag demands
func (h *ErrorHandler) ReconcileError(err error, option *common.ReconcileOption) (reconcile.Result, error) {
	if nil == h.Log {
		h.Log = log
	}
	if nil == err {
		h.Log.Info("reconcile error: request a fail directly")
		return reconcile.Result{}, nil
	}

	retryAfter := common.ReconcileDefaultDelayTime
	flag := common.NoFlag

	if nil != option {
		retryAfter = option.RetryAfter
		flag = option.Flag
	}

	logger := h.Log.WithValues("error", err.Error(), "retryAfter", retryAfter, "flag", flag)

	if flag.Has(common.ReconcileFailDirectly) {
		logger.Info("reconcile error: request a fail")
		return reconcile.Result{}, nil
	}

	// Non-API error will fail directly
	if !IsAPIErorr(err) {
		logger.Info("a erorr happened, will fail")
		return reconcile.Result{}, nil
	}

	// handle API error
	if flag.Has(common.NotReconcileHttpErrorAnyMore) {
		logger.Info("reconcile error: request a fail")
		return reconcile.Result{}, nil

	}

	if apiErrors.IsNotFound(err) {
		// Object not found, return.  Created objects are automatically garbage collected.
		// For additional cleanup logic use finalizers.
		logger.Info("resource not found, will fail")
		return reconcile.Result{}, nil
	}

	if apiErrors.IsAlreadyExists(err) {
		logger.Info("resource already exists, will fail")
		return reconcile.Result{}, nil
	}

	if apiErrors.IsConflict(err) {
		// This normally means your resourceVersion is out of date
		// make a quick retry by return error, should avoid this condition
		if flag.Has(common.NotReconcileHttpConflictAnyMore) {
			logger.Info("resource conflict, will fail")
			return reconcile.Result{}, nil
		}
		if flag.Has(common.ReconcileHttpErrorNoDelay) {
			logger.Info("resource conflict, will enqueue")
			return reconcile.Result{Requeue: true}, nil
		}
		logger.Info("resource conflict, will retry")
		return reconcile.Result{RequeueAfter: retryAfter}, nil
	}

	if IsClientErorr(err) {
		logger.Info("client error, task will fail")
		return reconcile.Result{}, nil
	}

	// handle server error, update status in retryAfter minute
	log.Error(err, "a erorr happened, will retry later")
	return reconcile.Result{Requeue: true, RequeueAfter: retryAfter}, nil
}
